Setting up git-daemon2:
=======================

1) Install gpg:
---------------

Install gpg if you don't have it already. Git-daemon2 setup needs it.

2) Get gitolite tar
-------------------

If you have already working gitolite install, skip steps 2-4.

You need gitolite tar archive. Either download one directly or clone Gitolite
repository and do

'make master.tar'

(replace master with another branch if you want tar of different branch).

3) Make new user account
------------------------

Even if dedicated user account isn't strictly necressary, its recommended to
create one. If git-daemon2 is to be installed as system daemon, dedicating
account for it is REQUIRED.

4) Install gitolite
-------------------

Switch to new user account, cd to its home, unpack the gitolite tar
archive. Cd to top level where it is unpacked and do 'src/gl-install'
(don't cd to src directory and run gl-install from there, it won't work).

NOTE: Make sure that $HOME gets set correctly for the user. If using sudo,
use the -H option.

The first time 'src/gl-install' is run, it stops after creating main
configuration file. Do once-over on that file with text editor (the defaults
seem reasonable) and rerun 'src/gl-install'.

5) Compile and install git-daemon2:
-----------------------------------

Compile git-daemon2 and copy at least executables 'git-daemon2',
'gits-generate-hostkey' and 'gits-get-key-id' to under git user home
directory.

6) Generate hostkey:
--------------------

Run 'gits-gnerate-hostkey' and answer questions it gives. Hopefully after
giving first few answers, it invokes gpg to generate key. If you use
2048- or 3072-bit keys, gpg warns about some programs not supporting
these keysizs. Ignore those warnings, they aren't relevant. It may also
warn about options not applying to this run. That isn't serious either.

After generating key, it prompts for filename to save it. Something
like  .git-daemon2-hostkey in home directory is reasonable.

If using git-daemon2 in system daemon mode, somewhere under /etc is
also fairly reasoable.

7) Grab id of hostkey
---------------------

Do 'gits-get-key-id ~/.git-daemon2-hostkey' and write down the id
it produces. You'll need that later (its handier to have it written
down than to obtain it again).

Note that filename passed needs to include at least one '/' (prefix
with './' if in current directory) or it will look it from wrong
place. That's the reason for '~/' there.

Of course, if you used different name for hostkey, replace the filename with
the correct one there.

8) Edit the gitolite access rules
---------------------------------
If you just installed gitolite, 'cd ~/.gitolite' and then edit
'conf/gitolite.conf'. It contains example config. Delete stuff quite
liberally. You might put some access rules already.

Then after editing, run 'src/gl-compile-conf'. It probably throws
error about authorized_keys. However, it does everything that really
matters before that error, so that authorized_keys error can be ignored.

This isn't needed if you have existing gitolite install.

9) Run git-daemon2:
-------------------

Fairly reasonable command line is (if you don't need password "
"authentication, this is all on one line):

'git-daemon2 --listen-tcpv4 --verbose --host-key=~/.git-daemon2-hostkey \
--authorization-cmd=~/.gitolite/src/gl-auth-command'

Yes, that authorization cmd path has to be absolute (or actually, it can
be relative to home directory of git user). But it just plain ignores
current directory. Change the hostkey if needed.

If you need password authentication, create empty file with name something
like '.git-daemon2-srpfile' and add option:

'--srp-file=~/.git-daemon2-srpfile'

Check that daemon starts up (it does fairly extensive error checking on
startup and refuses to start if something goes wrong). If the daemon
starts, make it start as part of system startup somehow (also add
'--detach' so it becomes a daemon)

Note that if executing the daemon as root, you need to add option
'--user=git' (replace 'git' with git user if its something else). Note
that daemon reads hostkey and SRP files as root in that case. The
final line might look something like:

'git-daemon2 --listen-tcpv4 --verbose --host-key=~git/.git-daemon2-hostkey \
--authorization-cmd=~git/.gitolite/src/gl-auth-command --detach --user=git'

Note that if you use non-abstract Unix domain sockets, ensure that they
don't exist beforehand and chmod them afterwards (they are already created
when detaching execution exits).

10) Collect user keys and authorize them for apporiate actions:
---------------------------------------------------------------

Collect keypair IDs (gits-get-key-id) from users. For password
authentication, get SRP verifier lines (gits-generate-srp-verifier).

SRP verifier lines need to be added to SRP verifier database file
(text file specified by --srp-file=<file>). The lines are long so
'\' as last character of line does line continuation.

Also edit gitolite rules to give new users apporiate access.  The name
to use in Gitolite configuration for SRP user 'foo' is 'srp-foo'. The
keypair IDs already have correct prefix when generated.

If listening for Unix domain sockets, on some systems Unix authentication
is also available: If Unix user 'foo' pushes through unix domain socket
and doesn't specify other authentication, they appear as 'unix-foo'.

If you edit SRP verifier database, remember to SIGHUP git-daemon2
to make it reload that file (the parent process of the two is the
correct one, SIGHUP'ing the other does nothing). The parent process
also runs as root and the other as ordinary user if git-daemon2 was
invoked as root.

The Gitolite config changes can be done usual way (edit the config
and trigger 'src/gl-compile-conf'. These changes do not require
SIGHUP'ing git-daemon2.
