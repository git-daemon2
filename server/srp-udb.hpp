/*
 * Copyright (C) Ilari Liusvaara 2009
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */
#ifndef _srp_udb__hpp__included__
#define _srp_udb__hpp__included__

#include <gnutls/gnutls.h>
#include <vector>
#include <map>
#include <stdexcept>

class srp_udb
{
public:
	typedef std::vector<unsigned char> datum_t;
	srp_udb() throw();
	srp_udb(const std::string& filename) throw(std::bad_alloc, std::runtime_error);

	void add_user(const std::string& username, const std::string& salt, const std::string& verifier,
		const std::string& g, const std::string& n) throw(std::bad_alloc, std::out_of_range);

	void callback_fn(const std::string& username, gnutls_datum_t& salt, gnutls_datum_t& verifier,
		gnutls_datum_t& g, gnutls_datum_t& n) throw(std::bad_alloc, std::out_of_range);

	void session_add(gnutls_session_t session, const std::string& user) throw(std::bad_alloc);
	std::string session_user(gnutls_session_t session) throw(std::bad_alloc);
	void session_release(gnutls_session_t session) throw();
private:
	std::map<gnutls_session_t, std::string> sessions;
	std::map<std::string, datum_t> salts;
	std::map<std::string, datum_t> verifiers;
	std::map<std::string, datum_t> generators;
	std::map<std::string, datum_t> moduli;
	void copy_datum(gnutls_datum_t& dest, const datum_t& src) throw(std::bad_alloc);
	datum_t parse_base64(const std::string& spec) throw(std::bad_alloc, std::out_of_range);
};

extern srp_udb* active_udb;

int srp_callback_fn(gnutls_session_t session, const char* username, gnutls_datum_t* salt, gnutls_datum_t *verifier,
	gnutls_datum_t* g, gnutls_datum_t* n);

#endif
