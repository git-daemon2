/*
 * Copyright (C) Ilari Liusvaara 2009-2010
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */
#ifndef _unix__hpp__included__
#define _unix__hpp__included__

#include "listensock.hpp"
#include <stdexcept>
#include <string>

class unixlistener : public listener
{
public:
	unixlistener(const std::string& spec, bool tls_flag) throw(std::bad_alloc, os_exception,
		std::range_error);
	virtual ~unixlistener() throw();
	int get_fd() throw();
	std::pair<int, std::string> accept() throw(std::bad_alloc, os_exception);
	static bool class_supported(const std::string& spec) throw(std::runtime_error, std::bad_alloc);
	std::string special_authenticate(int fd) throw(std::bad_alloc);
private:
	int fd;
};

#endif
