/*
 * Copyright (C) Ilari Liusvaara 2009
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */
#include "pkt-decoder.hpp"
#include <alloca.h>
#include <algorithm>

flush_exception::flush_exception() : std::runtime_error("Flush packet seen") {}

static int decode(unsigned char hex)
{
	switch(hex) {
	case '0':			return 0;
	case '1':			return 1;
	case '2':			return 2;
	case '3':			return 3;
	case '4':			return 4;
	case '5':			return 5;
	case '6':			return 6;
	case '7':			return 7;
	case '8':			return 8;
	case '9':			return 9;
	case 'A':	case 'a':	return 10;
	case 'B':	case 'b':	return 11;
	case 'C':	case 'c':	return 12;
	case 'D':	case 'd':	return 13;
	case 'E':	case 'e':	return 14;
	case 'F':	case 'f':	return 15;
	default:			return -1;
	};
}

std::string decode_packet(circular_buffer& buffer) throw(std::bad_alloc, flush_exception, std::underflow_error,
	std::runtime_error)
{
	unsigned char buf[4];
	int decoded[4];
	unsigned char* tmpbuf;
	unsigned finallen;
	std::string packet;
	if(buffer.used() < 4)
		throw std::underflow_error("Packet header incomplete");
	buffer.peek(buf, 4);

	for(unsigned i = 0; i < 4; i++) {
		decoded[i] = decode(buf[i]);
		if(decoded[i] < 0)
			throw std::runtime_error("Invalid packet length character");
	}
	finallen = (decoded[0] << 12) + (decoded[1] << 8) + (decoded[2] << 4) + (decoded[3]);
	if(finallen == 0) {
		buffer.read(buf, 4);
		throw flush_exception();
	} else if(finallen < 4)
		throw std::runtime_error("Invalid packet length");

	if(buffer.used() < finallen)
		throw std::underflow_error("Packet body incomplete");
	packet.resize(finallen - 4);
	buffer.read(buf, 4);

	tmpbuf = (unsigned char*)alloca(finallen - 4);
	buffer.read(tmpbuf, finallen - 4);
	std::copy(tmpbuf, tmpbuf + (finallen - 4), packet.begin());

	return packet;
}
