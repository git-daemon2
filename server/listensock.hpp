/*
 * Copyright (C) Ilari Liusvaara 2009-2010
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */
#ifndef _listensock__hpp__included__
#define _listensock__hpp__included__

#include <map>
//Ugh. G++ 4.4 doesn't like <cstdint> without enabling C++0x mode.
#include <stdint.h>
#include "os_exception.hpp"

class listener
{
public:
//Destroy listener.
	virtual ~listener() throw();

//Get file descriptor for listnening socket
//
//Outputs:
//	Return value	The file descriptor.
	virtual int get_fd() throw() = 0;

//Accept connection from listening socket.
//
//Outputs:
//	Return value	First: Accepted socket.
//			Second: Address connection is from.
//
//Exceptions:
//	std::bad_alloc	Not enough memory.
//	os_exception	OS-level failure.
	virtual std::pair<int, std::string> accept() throw(std::bad_alloc, os_exception) = 0;

//Get listener for specified port specification.
//
//Inputs:
//	Spec		Listner specification.
//	tls_flag	Assume tls from start.
//
//Outputs:
//	Return value	Listner.
//
//Exceptions:
//	std::range_error
//			Specification is not valid for any known listener.
//	os_exception	OS-level error.
//	std::bad_alloc	Not enough memory.
	static listener& get_listener(const std::string& spec, bool tls_flag) throw(std::range_error,
		os_exception, std::bad_alloc);

//Perform special authentication.
//
//Inputs:
//	fd		The file descriptor
//
//Outputs:
//	Return value	Special authentication output. "" if none.
	virtual std::string special_authenticate(int fd) throw(std::bad_alloc);

//Set TLS-only flag.
//
//Inputs:
//	flag		New value for flag.
	void tlsonly_flag(bool flag) throw();

//Get TLS-only flag.
//
//Outputs:
//	Return value	Value for flag.
	bool tlsonly_flag() throw();
protected:
	//Create listner base class.
	listener() throw();
private:
	//Not copyable.
	listener(const listener& l);
	listener& operator=(const listener& l);
	//TLS-only flag.
	bool _tlsonly_flag;
};

//Transform string into port number.
//
//Inputs:
//	spec		Port number as string.
//
//Outputs:
//	Return value	The port.
//
//Exceptions:
//	std::range_error
//			Port number not valid.
unsigned short to_port_number(const std::string& spec) throw(std::range_error);

//Transform string into 32-bit unsigned quantity.
//
//Inputs:
//	spec		Port number as string.
//
//Outputs:
//	Return value	The number.
//
//Exceptions:
//	std::range_error
//			Number not valid.
uint32_t touint32_t(const std::string& spec) throw(std::range_error);

//Parse the host part out of host/port.
//
//Inputs:
//	spec		Host/port
//
//Outputs:
//	Return value	The host
//
//Exceptions:
//	std::range_error
//			Input not valid.
//	std::bad_alloc	Out of memory.
std::string extract_host(const std::string& hostport) throw(std::range_error, std::bad_alloc);

//Parse the port part out of host/port.
//
//Inputs:
//	spec		Host/port
//	tls_only	TLS-only connection.
//
//Outputs:
//	Return value	The port
//
//Exceptions:
//	std::range_error
//			Input not valid.
//	std::bad_alloc	Out of memory.
std::string extract_port(const std::string& hostport, bool tls_only) throw(std::range_error, std::bad_alloc);

#endif
