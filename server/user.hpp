/*
 * Copyright (C) Ilari Liusvaara 2009
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */
#ifndef _user__hpp__included__
#define _user__hpp__included__

#include <stdexcept>
#include <gnutls/gnutls.h>
#include <sys/types.h>
#include <map>
#include <string>
#include "cbuffer.hpp"

struct user;

class user_connection
{
public:
	static const int still_active;
	static const int connection_end;
	static const int layer4_error;
	static const int tls_error;
	static const int tls_hand_error;
	static const int ukill;
	static const int red_failure;
	static const int utimeout;

	user_connection(int black_fd, unsigned timeout_secs) throw(std::bad_alloc);
	~user_connection() throw();
	void configure_tls(gnutls_session_t session) throw();
	void add_to_sets(int& bound, fd_set& rfds, fd_set& wfds, struct timeval& deadline) throw();
	void service(fd_set& rfds, fd_set& wfds) throw();
	void service_nofd() throw();
	std::pair<int, std::string> get_failure() throw(std::bad_alloc);
	static std::string explain_failure(int code) throw(std::bad_alloc);
	bool tls_configured() throw();
	gnutls_session_t get_tls() throw();
	void set_red_io(int rin, int rout, int rerr) throw();
	void clear_red_io() throw();
	circular_buffer get_red_in() throw(std::bad_alloc, std::invalid_argument);
	circular_buffer get_red_out() throw(std::bad_alloc, std::invalid_argument);
	circular_buffer get_red_err() throw(std::bad_alloc, std::invalid_argument);
	void clear_deadline() throw();
	void send_red_in_eof() throw();
	void tls_send_alert(gnutls_alert_description_t alert) throw();
	bool red_out_eofd() throw();
	bool red_in_available() throw();
	bool red_out_available() throw();
	bool red_err_available() throw();
	std::string get_peer_identity() throw(std::bad_alloc);
private:
	user_connection(user_connection& c) throw();
	user_connection& operator=(user_connection& c) throw();
	struct user* underlying;
	gnutls_session_t set_session;
};

struct timeval to_timeout(const struct timeval& tv);

#endif
