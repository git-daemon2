/*
 * Copyright (C) Ilari Liusvaara 2009
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */
#include "misc.h"
#include "userchange.hpp"
#include "variables.hpp"
#include "log.hpp"
#include <pwd.h>
#include <sys/wait.h>
#include <grp.h>
#include <cstring>
#include <sstream>
#include <cerrno>

bool do_detach = false;
static bool link_closed = false;

userchanger::userchanger() throw(std::bad_alloc, std::runtime_error)
{
	const char* env_home;

	if(!getuid() || !geteuid())
		throw std::runtime_error("Execution without changing users only allowed for non-root");

	env_home = getenv("HOME");
	if(!env_home) {
		struct passwd* user;

		user = getpwuid(getuid());
		if(!user)
			throw std::runtime_error("Can't find entry for current user from system users database");

		home_directory = user->pw_dir;
	} else
		home_directory = env_home;

	main_uid = 0;
	main_gid = 0;
}

userchanger::userchanger(const std::string& username) throw(std::bad_alloc, std::runtime_error)
{
	struct passwd* user;
	struct group* group = NULL;
	long maxgroups;

	maxgroups = sysconf(_SC_NGROUPS_MAX);
	if(maxgroups < 0)
		maxgroups = 16;		//Guess.

	if(geteuid())
		throw std::runtime_error("Changing user needs superuser priviledges");

	user = getpwnam(username.c_str());
	if(!user)
		throw std::runtime_error("No such user in system users database");
	if(!user->pw_uid)
		throw std::runtime_error("Running as UID 0 not allowed");

	home_directory = user->pw_dir;
	main_uid = user->pw_uid;
	main_gid = user->pw_gid;

	try {
		setgrent();
		errno = 0;
		while((group = getgrent())) {
			char** userptr = group->gr_mem;
			while(*userptr && std::string(*userptr) != username)
				userptr++;
			if(*userptr) {
				//Found supplementary group.
				if(suplementary.size() < (unsigned long)maxgroups)
					suplementary.push_back(group->gr_gid);
				else
					gd2_error << "Can't add group " << group->gr_name << " because of "
						<< "OS group limit" << std::endl;
			}
			errno = 0;
		}
		if(errno != 0) {
			std::ostringstream err;
			err << "Error reading groups database: " << strerror(errno);
			throw std::runtime_error(err.str());
		}
		endgrent();
	} catch(std::exception& e) {
		endgrent();
		throw;
	}
}


void userchanger::apply_user_change() throw(std::bad_alloc, std::runtime_error)
{
	std::ostringstream err;
	if(!main_uid) {
		if(chdir(home_directory.c_str()) < 0) {
			err << "Error setting current directory: " << strerror(errno);
			throw std::runtime_error(err.str());
		}

		if(!getuid() || !geteuid())
			throw std::runtime_error("No user to change to and running as root");
		return;
	}

	if(setgroups(suplementary.size(), &suplementary[0]) < 0) {
		err << "Error setting supplementary groups: " << strerror(errno);
		throw std::runtime_error(err.str());
	}

	if(setregid(main_gid, main_gid) < 0) {
		err << "Error setting main gid: " << strerror(errno);
		throw std::runtime_error(err.str());
	}

	if(setreuid(main_uid, main_uid) < 0) {
		err << "Error setting main uid: " << strerror(errno);
		throw std::runtime_error(err.str());
	}

	if(chdir(home_directory.c_str()) < 0) {
		err << "Error setting current directory: " << strerror(errno);
		throw std::runtime_error(err.str());
	}

	if(setenv("HOME", home_directory.c_str(), 1) < 0) {
		err << "Error setting $HOME: " << strerror(errno);
		throw std::runtime_error(err.str());
	}

	if(!getuid() || !geteuid())
		throw std::runtime_error("Still running as root, didn't I drop that?");

}

void userchanger::debug() throw()
{
	gd2_notice << "Primary UID: " << main_uid << std::endl;
	gd2_notice << "Primary GID: " << main_gid << std::endl;
	for(std::vector<gid_t>::iterator i = suplementary.begin(); i != suplementary.end(); i++) {
		gd2_notice << "Additional GID:" << *i << std::endl;
	}
}

void close_link() throw()
{
	if(!do_detach || link_closed)
		return;
	force_close(2);
	link_closed = true;
	use_syslog = true;
}

int become_daemon() throw(std::bad_alloc, std::runtime_error)
{
	std::ostringstream err;
	int pipes[2];

	if(!do_detach)
		return -2;

	if(pipe(pipes) < 0) {
		err << "Setting up pipe failed: " << strerror(errno);
		throw std::runtime_error(err.str());
	}

	int p1 = fork();
	if(p1 < 0) {
		err << "Fork failed: " << strerror(errno);
		throw std::runtime_error(err.str());
	} else if(p1 == 0) {
		force_close(pipes[0]);
		if(dup2(pipes[1], 2) < 0) {
			gd2_error << "Duplicating file descrptor failed: " << strerror(errno) << std::endl;
			exit(1);
		}
		if(pipes[1] != 2)
			force_close(pipes[1]);
		if(setsid() < 0) {
			gd2_error << "Starting new session failed: " << strerror(errno) << std::endl;
			exit(1);
		}
		int p2 = fork();
		if(p2 < 0) {
			gd2_error << "Fork failed: %s" << strerror(errno) << std::endl;
			exit(1);
		} else if(p2 == 0) {
			return -1;
		} else
			exit(0);
	} else {
		int status;
		force_close(pipes[1]);
		while(waitpid(p1, &status, 0) < 0);
		if(!WIFEXITED(status) || WEXITSTATUS(status)) {
			throw std::runtime_error("Failed to daemonize");
		}
		return pipes[0];
	}
}
