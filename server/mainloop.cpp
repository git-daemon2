/*
 * Copyright (C) Ilari Liusvaara 2009-2010
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */
#include "misc.h"
#include "signals.hpp"
#include "certificate.hpp"
#include "listensock.hpp"
#include "ssh-keypair.hpp"
#ifndef DISABLE_SRP
#include "srp-udb.hpp"
#endif
#include "pkt-decoder.hpp"
#include "user.hpp"
#include "user.h"
#include "log.hpp"
#include <signal.h>
#include <stdexcept>
#include <string>
#include <vector>
#include <gnutls/openpgp.h>
#include <sys/time.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <iostream>
#include <sstream>
#include <gcrypt.h>

struct client
{
	user_connection* c_connection;
	unsigned c_session;
	std::string c_authenticated_as;
	gnutls_session_t c_tls_session;
	bool c_authenticated;
	bool c_tls;
	bool c_killed;
	pid_t c_child;
	std::string c_from;

	std::ostream& log_notice() throw();
	std::ostream& log_error() throw();
};

static std::vector<client> clients;

std::ostream& client::log_notice() throw()
{
	return (gd2_notice << "[Session " << c_session << "] ");
}

std::ostream& client::log_error() throw()
{
	return (gd2_error << "[Session " << c_session << "] ");
}

struct client* client_by_pid(pid_t pid) throw()
{
	for(std::vector<client>::iterator i = clients.begin(); i != clients.end(); i++)
		if(i->c_child == pid)
			return &*i;
	return NULL;
}

std::ostream& log_notice(pid_t pid) throw()
{
	struct client* c = client_by_pid(pid);
	if(c)
		return c->log_notice();
	else
		return (gd2_notice << "[Unknown PID " << pid << "] ");
}

std::ostream& log_error(pid_t pid) throw()
{
	struct client* c = client_by_pid(pid);
	if(c)
		return c->log_error();
	else
		return (gd2_error << "[Unknown PID " << pid << "] ");
}


static unsigned next_session = 0;
unsigned initial_timeout = 10;
unsigned max_users = 32;
bool do_vhosts = false;
std::string vhost_postfix;
std::string anonymous_id = "anonymous";
std::string authorization_cmd = "";

#ifndef DISABLE_SRP
gnutls_srp_server_credentials_t srp_scred = NULL;
#endif
gnutls_certificate_credentials_t serv_cert = NULL;
gnutls_dh_params_t dh_params = NULL;
certificate* cert = NULL;

//Set up TLS for connection.
static void setup_tls(struct client& client) throw(std::exception)
{
	user_connection& c = *client.c_connection;

	int s;
	gnutls_session_t session;

	s = gnutls_init(&session, GNUTLS_SERVER);
	if(s < 0)
		throw std::runtime_error(std::string("gnutls_init: ") + gnutls_strerror(s));

	client.c_tls_session = session;

#ifndef DISABLE_SRP
	//SRP isn't on by default, so enable it. Also, RSA key exchange is insecure (in
	//combination with SSH authentication), so disable it.
	s = gnutls_priority_set_direct(session, "NORMAL:-RSA:+SRP:+SRP-DSS:+SRP-RSA", NULL);
#else
	//The same thing as above about RSA goes here too.
	s = gnutls_priority_set_direct(session, "NORMAL:-RSA", NULL);
#endif
	if(s < 0)
		throw std::runtime_error(std::string("gnutls_priority_set_direct: ") + gnutls_strerror(s));

#ifndef DISABLE_SRP
	//SRP credentials so TLS-SRP can be used.
	s = gnutls_credentials_set(session, GNUTLS_CRD_SRP, srp_scred);
	if(s < 0)
		throw std::runtime_error(std::string("gnutls_credentials_set(SRP): ") + gnutls_strerror(s));
#endif

	//Certificate credentials so hostkey gets sent.
	s = gnutls_credentials_set(session, GNUTLS_CRD_CERTIFICATE, serv_cert);
	if(s < 0)
		throw std::runtime_error(std::string("gnutls_credentials_set(CERT): ") + gnutls_strerror(s));

	//Request client certificate so we can use it for authentication but don't
	//require it (for SRP-only and anonymous authentication).
	gnutls_certificate_server_set_request(session, GNUTLS_CERT_REQUEST);

	gnutls_dh_set_prime_bits(session, 1024);

	//Actually activate TLS on connection, now that it is set up.
	c.configure_tls(session);
	client.log_notice() << "TLS Configured" << std::endl;
	client.c_tls = true;
}

//Handle SIGCHLD by reaping zombie children.
void handle_sigchild() throw()
{
	int status;
	pid_t pid;
	sigchld_received = false;
	while((pid = waitpid(-1, &status, WNOHANG)) > 0) {
		struct client* c = client_by_pid(pid);

		try {
			//Print log message about it. Error severity is only used for abnormal
			//process termination (non-zero exit status or crash).
			if(c && !c->c_killed && c->c_connection)
				c->c_connection->clear_red_io();

			if(WIFEXITED(status) && WEXITSTATUS(status))
				log_error(pid) << "Child exited with status " << WEXITSTATUS(status) << std::endl;
			else if(WIFEXITED(status))
				log_notice(pid) << "Child exited normally" << std::endl;
			else if(WIFSIGNALED(status))
				log_error(pid) <<  "Child crashed with signal " << WTERMSIG(status) << std::endl;
			else
				log_error(pid) << "Child abend with unknown exit status " << status << std::endl;
		} catch(std::exception& e) {
			//That's just for report, so if it fails, not that disasterous.
		}
	}
}

//Call select. Notes that timeout is deadline, not offset into future.
//If SIGTERM or SIGCHLD is received during wait or have been received, function returns
//immediately.
static void do_select(int bound, fd_set& rfds, fd_set& wfds, struct timeval& timeout)
{
	struct timespec timeout2;
	int r;
	sigset_t old;
	sigset_t sigterm;

	//Block SIGTERM/SIGCHLD so they can be reliably waited on.
	sigemptyset(&sigterm);
	sigaddset(&sigterm, SIGTERM);
	sigaddset(&sigterm, SIGCHLD);
	sigprocmask(SIG_BLOCK, &sigterm, &old);

	//If SIGCHLD/SIGTERM has been received, return immediately. Due to signal block
	//they can't arrive before select starts. Clear the sets to avoid service
	//code from trying to do I/O with non-ready file descriptors.
	if(sigterm_received || sigchld_received) {
		FD_ZERO(&rfds);
		FD_ZERO(&wfds);
		sigprocmask(SIG_SETMASK, &old, NULL);
		return;
	}

	timeout = to_timeout(timeout);
	timeout2.tv_sec = timeout.tv_sec;
	timeout2.tv_nsec = timeout.tv_usec * 1000;
	r = pselect(bound, &rfds, &wfds, NULL, &timeout2, &old);

	//Restore signal mask.
	sigprocmask(SIG_SETMASK, &old, NULL);

	//If select failed, clear the file descriptor sets to avoid service code from
	//doing I/O with non-ready file descriptors. EINTR and ETIMEDOUT are normal
	//so don't report them.
	if(r < 0) {
		if(errno != EINTR && errno != ETIMEDOUT)
			gd2_error << "Select failed: " << strerror(errno) << std::endl;
		FD_ZERO(&rfds);
		FD_ZERO(&wfds);
	} else if(r == 0) {
		/* Timed out. */
		FD_ZERO(&rfds);
		FD_ZERO(&wfds);
	}
	return;
}

//Use vhost postfix to shorten hostname.
//The rules are:
//1) Dot at beginning of postfix matches empty string at start of name.
//2) If postfix matches end of string, that part is removed.
//3) If no match, complete hostname is retained.
static std::string shorten_host(const std::string& full, const std::string& postfix)
{
	//Dot at beginning of postfix matches empty string at start of name.
	if(postfix.length() > 0 && postfix.at(0) == '.' && postfix.substr(1) == full)
		return "";

	//If name is shorter than postfix, it can't match postfix.
	if(full.length() < postfix.length())
		return full;

	//If end of name matches postfix, clip the postfix away.
	if(full.substr(full.length() - postfix.length()) == postfix)
		return full.substr(0, full.length() - postfix.length());

	//No match. Retain full name.
	return full;
}

//Invoke authorization command with specified command.
static void invoke_command(struct client& c, const std::string& finalcmd)
{
	int p;
	int pipes[6];

	//Create three pairs of pipes, for stdin, stdout and stderr of
	//Authorization program.
	if(pipe(pipes + 0) < 0)
		throw os_exception(errno);
	if(pipe(pipes + 2) < 0)
		throw os_exception(errno);
	if(pipe(pipes + 4) < 0)
		throw os_exception(errno);

	p = fork();
	if(p < 0) {
		int err = errno;
		force_close(pipes[0]);
		force_close(pipes[1]);
		force_close(pipes[2]);
		force_close(pipes[3]);
		force_close(pipes[4]);
		force_close(pipes[5]);
		throw os_exception(err);
	} else if(p == 0) {
		int swap;
		//Close the ends of pipes not needed in child.
		force_close(pipes[1]);
		force_close(pipes[2]);
		force_close(pipes[4]);

		//Set SSH_ORIGINAL_COMMAND so authorization command gets the subcommand
		//to invoke.
		if(setenv("SSH_ORIGINAL_COMMAND", finalcmd.c_str(), 1) < 0) {
			c.log_error() << "Can't set SSH_ORIGINAL_COMMAND: " <<
				strerror(errno) << std::endl;
			exit(1);
		}

		// Save original stderr in case execution fails.
		if((swap = dup(2)) < 0) {
			c.log_error() << "Can't redirect fds for authorization command: " <<
				strerror(errno) << std::endl;
			exit(1);
		}
		//Redirect stdin/stdout/stderr.
		if((dup2(pipes[0], 0) < 0) || (dup2(pipes[3], 1) < 0) || (dup2(pipes[5], 2) < 0)) {
			c.log_error() << "Can't redirect fds for authorization command: " <<
				strerror(errno) << std::endl;
			exit(1);
		}
		//Who is the connection authenticated as? If nobody, substitute anonymous identity.
		std::string auth = anonymous_id;
		if(c.c_authenticated_as != "")
			auth = c.c_authenticated_as;

		//Finally, run the authorization program.
		execl(authorization_cmd.c_str(), authorization_cmd.c_str(), auth.c_str(), NULL);

		//Execution failed. Send some error to client.
		std::cerr << "Internal error: Failed to run authorization command" << std::endl;

		//Try to send message to log too (this is where the saved stderr is needed).
		if(dup2(swap, 2) >= 0)
			c.log_error() << "Failed to execute authorization command: " << strerror(errno) << std::endl;

		//Signal failure to parent process.
		exit(1);
	} else {
		//Mark the child to connection, close pipe ends not needed in parent, connect pipes to
		//dispatcher and clear the deadline (it only counts up to invoking authorization command).
		c.c_child = p;
		force_close(pipes[0]);
		force_close(pipes[3]);
		force_close(pipes[5]);
		c.c_connection->set_red_io(pipes[2], pipes[1], pipes[4]);
		c.c_connection->clear_deadline();
	}
}

//Process packet that attempts to invoke some command (except starttls).
static void process_connect_packet(struct client& c, const std::string& command, const std::string& host)
{
	//Split the command to primary command and argument to it.
	size_t split = command.find(' ');
	std::string pcmd, arg;
	if(split == std::string::npos)
		pcmd = command;
	else {
		pcmd = command.substr(0, split);
		arg = command.substr(split + 1);

		//The path is required to start by '/', so check that.
		if(arg.at(0) != '/')
			throw std::runtime_error("Invalid request, no '/' at start of arguemnt");

		//Munge path for vhosting.
		if(host != "")
			arg = "/" + host + arg;
	}

	if(pcmd == "invalid-command-test")
		throw std::runtime_error("Performing invalid command test as requested");

	//The argument needs to be quoted for "shell". But really its passed through environment, so
	//its not actually critical.
	std::string finalcmd;
	if(arg != "")
		finalcmd = pcmd + " '" + arg + "'";
	else
		finalcmd = pcmd;

	//Try to invoke authorization command.
	try {
		c.log_notice() << "Running authorziation command. " << std::endl;
		invoke_command(c, finalcmd);
	} catch(std::exception& e) {
		c.log_error() << "Can't run authorization command: " << e.what() << std::endl;
	}
}

static std::string extract_vhost(const std::string& packet, size_t hostpos)
{
	std::string host;
	size_t hostend = std::string::npos;
	size_t extra = 0;

	//Handle IPv6-related bracket notation.
	if(packet.at(hostpos + 5) == '[') {
		extra = 1;
		hostend = packet.find(']', hostpos);
	}

	//We also end at ',' even if that isn't legal.
	if(hostend == std::string::npos)
		hostend = packet.find_first_of(":,", hostpos);

	//It can be terminated by NUL as well. Find_first_of can't
	//search for that!
	if(hostend == std::string::npos)
		hostend = packet.find((char)0, hostpos);

	if(hostend == std::string::npos)
		//No host end. Use rest of string as host.
		host = packet.substr(hostpos + 5 + extra);
	else
		//Extract the host part.
		host = packet.substr(hostpos + 5 + extra, hostend - hostpos - 5 - extra);

	return host;
}

#define SIGNATURE_DATA_SIZE 32
#define HASH_ALGO GCRY_MD_SHA256
#define HASH_SIZE 32

//Process arbitrary command request.
static bool do_command_request(struct client& c) throw(std::exception)
{
	std::string packet;

	circular_buffer redout = c.c_connection->get_red_out();
	circular_buffer redin = c.c_connection->get_red_in();

	try {
		packet = decode_packet(redout);
	} catch(flush_exception& e) {
		c.log_notice() << "Ignored unexpected flush" << std::endl;
		return true;
	} catch(std::underflow_error& e) {
		if(!c.c_connection->red_out_eofd())
			return false;
		//If there's no more data, drain red input and mark red in as no more data, so
		//the connetion gets closed.
		redin.clear();
		c.c_connection->send_red_in_eof();
		return false;
	} //Throw other errors out so they terminate the connection.
	if(packet == "starttls") {
		//Activating TLS is not allowed if TLS is already active. Check that.
		if(c.c_tls)
			throw std::runtime_error("Attempt to start TLS with TLS already active");
		//Send signal back to client. Such signal is needed to avoid race conditions
		//(Client hello ending up in server red out buffer, resulting a deadlock).
		redin.write((unsigned char*)"proceed\n", 8);

		//Serve the connection so that that response winds up to send buffer ahead
		//of TLS traffic.
		c.c_connection->service_nofd();

		//Activate TLS.
		setup_tls(c);
		return false;	//Temporarily suspend processing for TLS handshake.
	} else if(packet.substr(0, 4) == "ssh ") {
		//Special SSH keypair command.
		if(!c.c_tls)
			throw std::runtime_error("Attempt to do SSH keypair without TLS active");

		try {
			int s;
			char hash[HASH_SIZE];
			const gnutls_datum_t* cert;
			char data_to_sign[SIGNATURE_DATA_SIZE];
			cert = gnutls_certificate_get_ours(c.c_connection->get_tls());
			if(!cert)
				throw std::runtime_error("We don't have certificate???");

			gcry_md_hash_buffer(HASH_ALGO, hash, cert->data, cert->size);
			s = gnutls_prf(c.c_connection->get_tls(), 14, "ssh-key-verify", 0, HASH_SIZE,
				(const char*)hash, SIGNATURE_DATA_SIZE, data_to_sign);
			if(s < 0)
				throw std::runtime_error(std::string("gnutls_prf: ") + gnutls_strerror(s));

			c.c_authenticated_as = handle_ssh_keypair(packet.substr(4), data_to_sign,
				SIGNATURE_DATA_SIZE);
			if(c.c_authenticated_as != "")
				c.log_notice() << "Authenticated as \"" << c.c_authenticated_as
					<< "\""<< std::endl;
		} catch(std::bad_alloc& e) {
			throw;		//Forward error
		} catch(std::exception & e) {
			c.log_error() << "SSH auth command error: " << e.what() << std::endl;
			c.c_connection->tls_send_alert(GNUTLS_A_BAD_CERTIFICATE);
			throw;
		}
		return true;	//Request comes next?
	} else {
		//Other command, we should check if it has extra attributes (host attribute).
		std::string command, extra, host;

		//Extended attributes begin at first NUL (yes, strings can contain it).
		size_t cmdlen = packet.find((char)0);
		if(cmdlen != std::string::npos) {
			//Found extended attributes. Split them.
			command = packet.substr(0, cmdlen);
			extra = packet.substr(cmdlen + 1);
			cmdlen = extra.find((char)0);
			if(cmdlen != std::string::npos)
				extra = extra.substr(0, cmdlen);
		} else
			//No extended attributes.
			command = packet;

		//Search for host header.
		size_t hostpos = extra.find("host=");
		if(hostpos != std::string::npos && do_vhosts) {
			//Shorten host by removing postfix.
			host = shorten_host(extract_vhost(extra, hostpos), vhost_postfix);
		}
		//Pass the command forwards towards execution.
		process_connect_packet(c, command, host);
		return false;	//Should get connection.
	}
	return true;
}

//The actual server main loop.
void server_mainloop(std::vector<listener*>& listeners)
{
	struct timeval timeout;

	//Ensure fds up to 2 exist to avoid trouble with opening something to those file descriptors.
	while(open("/dev/null", O_RDWR) < 3);

	//The exit condition from main loop is when there are no listeners and no connections (which
	//means there won't be any activity anymore).
	while(listeners.size() || clients.size()) {
		int bound = 0;
		fd_set rfds;
		fd_set wfds;
		FD_ZERO(&rfds);
		FD_ZERO(&wfds);
		gettimeofday(&timeout, NULL);
		//Strictly, this should be largest possible timeval value, but approximate by
		//at most 24h wait. Waking up spuriously once per day isn't that bad.
		timeout.tv_sec += 86400;

		//Add listeners to set of fds to probe.
		for(std::vector<listener*>::iterator i = listeners.begin(); i != listeners.end(); i++) {
			listener& l = **i;
			FD_SET(l.get_fd(), &rfds);
			if(bound <= l.get_fd())
				bound = l.get_fd() + 1;
		}

		//Add connections to set of fds to probe.
		for(std::vector<client>::iterator i = clients.begin(); i != clients.end(); i++) {
			client& c = *i;
			c.c_connection->add_to_sets(bound, rfds, wfds, timeout);
		}

		do_select(bound, rfds, wfds, timeout);
		if(sigterm_received) {
			// Accept no more connections. We quit when current connections go away.
			gd2_notice << "Received SIGTERM. Accepting no more connections.\n";
			listeners.clear();
		}
		if(sigchld_received)
			handle_sigchild();

		//Process new connections.
		for(std::vector<listener*>::iterator i = listeners.begin(); i != listeners.end(); i++) {
			listener& l = **i;
			//Any activity for this listener?
			if(!FD_ISSET(l.get_fd(), &rfds))
				continue;
			//If maximum number of users already exist, don't allow more.
			if(max_users > 0 && clients.size() >= max_users)
				continue;

			struct client newclient;
			newclient.c_connection = NULL;
			//We have new connection!
			try {
				std::pair<int, std::string> conn = l.accept();
				newclient.c_connection = new user_connection(conn.first, initial_timeout);
				newclient.c_session = next_session++;
				newclient.c_authenticated = false;
				newclient.c_child = 0;
				newclient.c_tls = false;
				newclient.c_killed = false;
				newclient.c_from = conn.second;

				//Try to grab peer's credentials.
				try {
					newclient.c_authenticated_as = l.special_authenticate(conn.first);
				} catch(std::exception& e) {
					newclient.log_error() << "Can't do special authentication: " << e.what()
						<< std::endl;
				}

				//If connection comes from TLS-only socket, set up TLS immediately so
				//no non-TLS data is seen.
				if(l.tlsonly_flag())
					setup_tls(newclient);

				clients.push_back(newclient);
				newclient.log_notice() << "Connection from " << conn.second << std::endl;
				if(newclient.c_authenticated_as != "")
					newclient.log_notice() << "Tentatively authenticated as \""
						<< newclient.c_authenticated_as << "\""<< std::endl;
			} catch(std::exception& e) {
				gd2_error << "Can't accept connection: " << e.what() << std::endl;
				//Clean up the connection object if any was allocated.
				if(newclient.c_connection)
					delete newclient.c_connection;
				continue;
			}
		}

		//Process the connections.
		for(std::vector<client>::iterator i = clients.begin(); i != clients.end(); i++) {
			client& c = *i;

			//Service the low-level connection.
			c.c_connection->service(rfds, wfds);

			//Has the connection ended?
			try {
				std::pair<int, std::string> err = c.c_connection->get_failure();
				if(err.second == "")
					err.second = "<N/A>";
#ifndef DISABLE_SRP
				std::string username = active_udb->session_user(c.c_tls_session);
				if(err.first == USER_TLS_HAND_ERROR && username != "")
					c.log_error() << "Login failure for \"" << username << "\" from <"
						<< c.c_from << ">, error (" << err.second << ")" << std::endl;
#endif
				if(err.first > 0)
					c.log_notice() << "Session ended normally." << std::endl;
				else if(err.first < 0)
					c.log_error() << "Connection lost: "
						<< user_connection::explain_failure(err.first) << " ("
						<< err.second << ")" << std::endl;

				if(err.first != 0) {
					c.c_killed = true;
					continue;
				}
			} catch(std::exception& e) {
				c.log_error() << "Unable to check termination status: " << e.what() << std::endl;
				c.c_connection->tls_send_alert(GNUTLS_A_INTERNAL_ERROR);
				continue;
			}

			//Authenticate if not yet authenticated and authentication is possible.
			if(!c.c_authenticated && c.c_connection->get_tls()) {
				c.c_authenticated = true;
				try {
					std::string tmpname = c.c_connection->get_peer_identity();
					if(tmpname != "")
						c.c_authenticated_as = tmpname;
					if(c.c_authenticated_as != "")
						c.log_notice() << "Authenticated as \"" << c.c_authenticated_as
							<< "\""<< std::endl;
				} catch(std::exception& e) {
					c.log_error() << "Unable to authenticate: " << e.what() << std::endl;
					c.c_connection->tls_send_alert(GNUTLS_A_INTERNAL_ERROR);
					continue;
				}

				//TLS handshake is now complete. Log that.
				c.log_notice() << "TLS handshake complete" << std::endl;
			}

			//If command interface is avaliable, read from it. Check both in and out because there
			//may be need of sending stuff back.
			if(c.c_connection->red_out_available() && c.c_connection->red_in_available()) {
				try {
					while(do_command_request(c));
				} catch(std::bad_alloc& e) {
					c.log_error() << "Request parsing failed due to out of memory." << std::endl;
					c.c_connection->tls_send_alert(GNUTLS_A_INTERNAL_ERROR);
				} catch(std::exception& e) {
					c.log_error() << "Killing connection: process command error: " << e.what()
						<< std::endl;
					c.c_connection->tls_send_alert(GNUTLS_A_DECODE_ERROR);
				}
			}
		}

		//Cleanup users that no longer exist.
		bool found_deleted = false;

		do {
			found_deleted = false;
			for(std::vector<client>::iterator i = clients.begin(); i != clients.end(); i++) {
				if(!i->c_killed)
					continue;
				delete i->c_connection;
				clients.erase(i);
				found_deleted = true;
				break;
			}
		} while(found_deleted);
	}
}
