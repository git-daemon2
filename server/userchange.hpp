/*
 * Copyright (C) Ilari Liusvaara 2009
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */
#ifndef _userchange__hpp__included__
#define _userchange__hpp__included__

#include <sys/types.h>
#include "os_exception.hpp"
#include <vector>

class userchanger
{
public:
	userchanger() throw(std::bad_alloc, std::runtime_error);
	userchanger(const std::string& username) throw(std::bad_alloc, std::runtime_error);
	void apply_user_change() throw(std::bad_alloc, std::runtime_error);
	void debug() throw();
private:
	std::string home_directory;
	uid_t main_uid;
	gid_t main_gid;
	std::vector<gid_t> suplementary;
};

void close_link() throw();
int become_daemon() throw(std::bad_alloc, std::runtime_error);

#endif
