/*
 * Copyright (C) Ilari Liusvaara 2009
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */
#ifndef _os_exception__hpp__included__
#define _os_exception__hpp__included__

#include <stdexcept>

class os_exception : public std::runtime_error
{
public:
	os_exception(int error) throw(std::bad_alloc);
	int get_error_code() throw();
private:
	int errorcode;
};

#endif
