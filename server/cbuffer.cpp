/*
 * Copyright (C) Ilari Liusvaara 2009
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */
#include <cstdlib>
#include <map>
#include <stdexcept>
#include <iostream>
#include <cerrno>
#include "os_exception.hpp"
#include "cbuffer.hpp"
#include "cbuffer.h"

class circular_buffer_i
{
public:
	//Create internal instance without filling fields.
	circular_buffer_i() throw();
	//Create circular buffer with specified size.
	circular_buffer_i(size_t size) throw(std::bad_alloc);
	//Destroy circular buffer (or wrapper for it).
	~circular_buffer_i() throw();
	//Reference count.
	unsigned ref_count;
	//Underlying circular buffer.
	struct cbuffer* underlying;
	//Underlying data buffer and its size.
	unsigned char* underlying_data;
	size_t underlying_size;
	//True if wrapping mode, false if owned mode.
	bool wrapping;
private:
	//This class is not copyable.
	circular_buffer_i(circular_buffer_i& x);
	circular_buffer_i& operator=(circular_buffer_i& x);
};

namespace {
	//Mapping from circular buffers to wrapping instances.
	std::map<cbuffer*, circular_buffer_i*> wraps;
}

size_t circular_buffer::used() throw()
{
	return cbuffer_used(imp->underlying);
}

size_t circular_buffer::free() throw()
{
	return cbuffer_free(imp->underlying);
}

void circular_buffer::peek(unsigned char* dest, size_t toread) throw(std::length_error)
{
	if(cbuffer_peek(imp->underlying, dest, toread) < 0)
		throw std::length_error("Peek toread too large");
}

void circular_buffer::read(unsigned char* dest, size_t toread) throw(std::length_error)
{
	if(cbuffer_read(imp->underlying, dest, toread) < 0)
		throw std::length_error("Read toread too large");
}

void circular_buffer::write(unsigned char* src, size_t towrite) throw(std::length_error)
{
	if(cbuffer_write(imp->underlying, src, towrite) < 0)
		throw std::length_error("Write towrite too large");
}

void circular_buffer::move(circular_buffer& to, size_t tomove) throw(std::length_error)
{
	if(cbuffer_move(to.imp->underlying, imp->underlying, tomove) < 0)
		throw std::length_error("Move tomove too large");
}

void circular_buffer::clear() throw()
{
	cbuffer_clear(imp->underlying);
}

size_t circular_buffer::read_max(unsigned char* dest, size_t limit) throw()
{
	return cbuffer_read_max(imp->underlying, dest, limit);
}

size_t circular_buffer::write_max(unsigned char* src, size_t limit) throw()
{
	return cbuffer_write_max(imp->underlying, src, limit);
}
size_t circular_buffer::move_max(circular_buffer& to, size_t limit) throw()
{
	return cbuffer_move_max(to.imp->underlying, imp->underlying, limit);
}

size_t circular_buffer::move_nolimit(circular_buffer& to) throw()
{
	return cbuffer_move_nolimit(to.imp->underlying, imp->underlying);
}

size_t circular_buffer::read_fd(int fd) throw(os_exception)
{
	ssize_t r;
	r = cbuffer_read_fd(imp->underlying, fd);
	if(r < 0)
		throw os_exception(errno);
	return (size_t)r;
}

size_t circular_buffer::write_fd(int fd) throw(os_exception)
{
	ssize_t r;
	r = cbuffer_write_fd(imp->underlying, fd);
	if(r < 0)
		throw os_exception(errno);
	return (size_t)r;
}

std::pair<unsigned char*, size_t> circular_buffer::fill_r_segment() throw()
{
	unsigned char* base;
	size_t size;
	cbuffer_fill_r_segment(imp->underlying, &base, &size);
	return std::make_pair(base, size);
}

std::pair<unsigned char*, size_t> circular_buffer::fill_w_segment() throw()
{
	unsigned char* base;
	size_t size;
	cbuffer_fill_w_segment(imp->underlying, &base, &size);
	return std::make_pair(base, size);
}

void circular_buffer::commit_r_segment(size_t size) throw(std::length_error)
{
	if(size > cbuffer_used(imp->underlying))
		throw std::length_error("Read commit exceeds used space");
	cbuffer_commit_r_segment(imp->underlying, size);
}

void circular_buffer::commit_w_segment(size_t size) throw(std::length_error)
{
	if(size > cbuffer_free(imp->underlying))
		throw std::length_error("Write commit exceeds free space");
	cbuffer_commit_w_segment(imp->underlying, size);
}

circular_buffer::circular_buffer(size_t buffersize) throw(std::bad_alloc)
{
	circular_buffer_i* x = new circular_buffer_i(buffersize);
	x->ref_count = 1;
	try {
		wraps[x->underlying] = x;
	} catch(std::bad_alloc& e) {
		delete x;
		throw;
	}
	imp = x;
}

circular_buffer::~circular_buffer() throw()
{
	imp->ref_count--;
	if(!imp->ref_count) {
		if(imp->underlying)
			wraps.erase(imp->underlying);
		delete imp;
	}
}

circular_buffer::circular_buffer(cbuffer* cbuf) throw(std::bad_alloc)
{
	if(wraps.count(cbuf)) {
		imp = wraps[cbuf];
		if(imp->ref_count >= 0)
			imp->ref_count++;
	} else {
		circular_buffer_i* x = new circular_buffer_i();
		x->underlying = cbuf;
		x->underlying_data = NULL;
		x->underlying_size = 0;
		// The new refrence + backreference from cbuffer.
		x->ref_count = 2;
		x->wrapping = true;
		try {
			wraps[cbuf] = x;
		} catch(std::exception& e) {
			delete x;
			throw;
		}
		imp = x;
	}
}

circular_buffer::circular_buffer(const circular_buffer& cbuf)
{
	cbuf.imp->ref_count++;
	imp = cbuf.imp;
}

circular_buffer& circular_buffer::operator=(const circular_buffer& cbuf)
{
	if(this == &cbuf)
		return *this;

	cbuf.imp->ref_count++;
	imp->ref_count--;
	if(!imp->ref_count) {
		wraps.erase(imp->underlying);
		delete imp;
	}
	imp = cbuf.imp;
	return *this;
}

circular_buffer_i::circular_buffer_i() throw()
{
}

circular_buffer_i::circular_buffer_i(size_t size) throw(std::bad_alloc)
{
	underlying_data = new unsigned char[size];
	underlying_size = size;
	underlying = cbuffer_create(underlying_data, underlying_size);
	wrapping = false;
	if(!underlying) {
		//If creation failed, free the underlying data.
		delete[] underlying_data;
		throw std::bad_alloc();
	}
}

circular_buffer_i::~circular_buffer_i() throw()
{
	//If wrapping is true, this cbuffer instance is just wrapping cbuffer and shouldn't
	//destroy it because somebody else owns it.
	if(!wrapping && underlying) {
		cbuffer_destroy(underlying);
		underlying = NULL;
	}
	if(underlying_data)
		delete[] underlying_data;
}

void circular_buffer::disappearing(struct cbuffer* cbuf) throw()
{
	if(wraps.count(cbuf)) {
		circular_buffer_i* buf = wraps[cbuf];
		buf->ref_count--;
		if(!buf->ref_count)
			delete buf;
		else
			buf->underlying = NULL;
		wraps.erase(cbuf);
	}
}
