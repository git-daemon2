#include "ssh-keypair.hpp"
#include <gcrypt.h>
#include <vector>
#include <stdexcept>
#include <cstdio>
#include <iostream>

//RSA key format:
//string	"ssh-rsa"
//mpint		RSA public exponent e
//mpint		RSA modulus n
//mpint		RSA signature s (RSASSA-PKCS1-v1.5)

//DSA key format:
//string	"ssh-dss"
//mpint		DSA group modulus p
//mpint		DSA group size q
//mpint		DSA group generator g
//mpint		DSA public key y
//mpint		DSA signature value r
//mpint		DSA signature value s

struct mpint
{
	gcry_mpi_t value;
	mpint(gcry_mpi_t v);
	mpint(const unsigned char* buffer, size_t buflen) throw(std::exception);
	mpint(const unsigned char* buffer, size_t buflen, size_t& offset) throw(std::exception);
	void do_init(const unsigned char* buffer, size_t buflen) throw(std::exception);
	~mpint() throw();
};

struct sexpr
{
	gcry_sexp_t expr;
	sexpr(const char* format, void** args) throw(std::exception);
	sexpr(gcry_sexp_t e);
	~sexpr() throw();
};

struct digest
{
	gcry_md_hd_t hd;
	digest(int algo) throw(std::exception);
	~digest() throw();
};

void throw_error(std::string prefix, gcry_error_t err)
{
	if(gcry_err_code(err) == GPG_ERR_ENOMEM)
		throw std::bad_alloc();
	else if(gcry_err_code(err))
		throw std::runtime_error(prefix + ": " + gcry_strsource(err) + " / " + gcry_strerror(err));
}

digest::digest(int algo) throw(std::exception)
{
	throw_error("gcry_md_open", gcry_md_open(&hd, algo, 0));
}

digest::~digest() throw()
{
	 gcry_md_close(hd);
}

sexpr::sexpr(gcry_sexp_t e)
{
	expr = e;
}

sexpr::sexpr(const char* format, void** args) throw(std::exception)
{
	size_t erroff;
	throw_error("Unable to build S-expression", gcry_sexp_build_array(&expr, &erroff, format, args));
}

sexpr::~sexpr() throw()
{
	gcry_sexp_release(expr);
}

mpint::mpint(gcry_mpi_t v)
{
	value = v;
}

mpint::mpint(const unsigned char* buffer, size_t buflen) throw(std::exception)
{
	do_init(buffer, buflen);
}


void mpint::do_init(const unsigned char* buffer, size_t buflen) throw(std::exception)
{
	throw_error("Unable to parse MPI component", gcry_mpi_scan(&value, GCRYMPI_FMT_STD, buffer, buflen, NULL));
}

mpint::mpint(const unsigned char* buffer, size_t buflen, size_t& offset) throw(std::exception)
{
	size_t numlen = 0;
	if(offset + 4 > buflen || offset + 4 < offset)
		throw std::runtime_error("MPI component overflows string");
	numlen |= ((size_t)buffer[offset + 0] << 24);
	numlen |= ((size_t)buffer[offset + 1] << 16);
	numlen |= ((size_t)buffer[offset + 2] << 8);
	numlen |= ((size_t)buffer[offset + 3]);
	if(offset + 4 + numlen > buflen || offset + 4 + numlen < offset + 4)
		throw std::runtime_error("MPI component overflows string");
	do_init(buffer + offset + 4, numlen);
	offset = offset + 4 + numlen;
}

mpint::~mpint() throw()
{
	gcry_mpi_release(value);
}

bool is_rsa_key(const std::string& data)
{
	const char* key = data.c_str();
	const char rsa_key_header[11] = {0, 0, 0, 7, 0x73, 0x73, 0x68, 0x2d, 0x72, 0x73, 0x61};
	return !memcmp(key, rsa_key_header, 11);
}

bool is_dsa_key(const std::string& data)
{
	const char* key = data.c_str();
	const char dsa_key_header[11] = {0, 0, 0, 7, 0x73, 0x73, 0x68, 0x2d, 0x64, 0x73, 0x73};
	return !memcmp(key, dsa_key_header, 11);
}

size_t handle_ssh_rsa_keypair(const std::string& data, const char* challenge, size_t size)
{
	const unsigned char* key = (const unsigned char*)data.c_str();
	size_t keysize = data.length();
	void* tmp[4];
	size_t ukeysize;
	size_t offset = 11;
	int hashlen;
	int hashalgo;
	const char* algo;

	mpint e(key, keysize, offset);
	mpint n(key, keysize, offset);
	ukeysize = offset;
	mpint s(key, keysize, offset);

	tmp[0] = &n.value;
	tmp[1] = &e.value;
	tmp[2] = &s.value;
	tmp[3] = NULL;
	sexpr pkey("(public-key (rsa (n %m) (e %m)))", tmp);
	sexpr sign("(data (flags raw) (value %m))", tmp + 2);

	gcry_sexp_t result;
	throw_error("gcry_pk_encrypt: ", gcry_pk_encrypt(&result, sign.expr, pkey.expr));
	sexpr sdata(result);

	sexpr sdata1(gcry_sexp_nth(sdata.expr, 1));
	if(!sdata1.expr)
		throw std::bad_alloc();

	sexpr sdata2(gcry_sexp_nth(sdata1.expr, 1));
	if(!sdata2.expr)
		throw std::bad_alloc();

	size_t decrypted_len;
	const unsigned char* decrypted = (const unsigned char*)gcry_sexp_nth_data(sdata2.expr, 1, &decrypted_len);

/*
	fprintf(stderr, "Decrypted RSA value (%u bytes): ", decrypted_len);
	for(size_t i = 0; i < decrypted_len; i++)
		fprintf(stderr, "%02X", decrypted[i]);
	fprintf(stderr, "\n");
*/
	offset = 1;

	if(decrypted_len < 1)
		throw std::runtime_error("RSA Modulus way too short");
	if(decrypted_len != (gcry_mpi_get_nbits(n.value) + 7) / 8 - 1)
		throw std::runtime_error("Bad RSA signature padding (unexpected length)");
	if(decrypted[0] != 1)
		throw std::runtime_error("Bad RSA signature padding (expected PKCS type 1)");
	while(offset < decrypted_len && decrypted[offset] == 0xFF)
		offset++;
	if(offset == decrypted_len || decrypted[offset] != 0)
		throw std::runtime_error("Bad RSA signature padding (expected 0 after 0xFFs)");
	if(offset < 9)
		throw std::runtime_error("Bad RSA signature padding (required at least 8 0xFFs)");
	offset++;

	if(decrypted_len - offset == 35 && !memcmp(decrypted + offset,
		"\x30\x21\x30\x09\x06\x05\x2b\x0e\x03\x02\x1a\x05\x00\x04\x14", 15)) {
		algo = "sha1";
		hashalgo = GCRY_MD_SHA1;
		hashlen = 20;
		offset += 15;
	} else if(decrypted_len - offset == 47 && !memcmp(decrypted + offset,
		"\x30\x2d\x30\x0d\x06\x09\x60\x86\x48\x01\x65\x03\x04\x02\x04\x05\x00\x04\x1c", 19)) {
		algo = "sha224";
		hashalgo = GCRY_MD_SHA224;
		hashlen = 28;
		offset += 19;
	} else if(decrypted_len - offset == 51 && !memcmp(decrypted + offset,
		"\x30\x31\x30\x0d\x06\x09\x60\x86\x48\x01\x65\x03\x04\x02\x01\x05\x00\x04\x20", 19)) {
		algo = "sha256";
		hashalgo = GCRY_MD_SHA256;
		hashlen = 32;
		offset += 19;
	} else if(decrypted_len - offset == 67 && !memcmp(decrypted + offset,
		"\x30\x41\x30\x0d\x06\x09\x60\x86\x48\x01\x65\x03\x04\x02\x02\x05\x00\x04\x30", 19)) {
		algo = "sha384";
		hashalgo = GCRY_MD_SHA384;
		hashlen = 48;
		offset += 19;
	} else if(decrypted_len - offset == 83 && !memcmp(decrypted + offset,
		"\x30\x51\x30\x0d\x06\x09\x60\x86\x48\x01\x65\x03\x04\x02\x03\x05\x00\x04\x40", 19)) {
		algo = "sha512";
		hashalgo = GCRY_MD_SHA512;
		hashlen = 64;
		offset += 19;
	} else
		throw std::runtime_error("Unknown hash function used");

	digest d(hashalgo);
	gcry_md_write(d.hd, challenge, size);
	unsigned char* block = gcry_md_read(d.hd, 0);

	if(memcmp(decrypted + offset, block, hashlen))
		throw std::runtime_error("Signature is for different data");

	return ukeysize;
}

typedef void* voidptr;

size_t handle_ssh_dsa_keypair(const std::string& data, const char* challenge, size_t size)
{
	const unsigned char* key = (const unsigned char*)data.c_str();
	size_t keysize = data.length();
	voidptr tmp[8];
	int hashlen;
	int hashalgo;
	const char* algo;
	size_t offset = 11;
	size_t ukeysize;

	mpint p(key, keysize, offset);
	mpint q(key, keysize, offset);
	mpint g(key, keysize, offset);
	mpint y(key, keysize, offset);
	ukeysize = offset;
	mpint r(key, keysize, offset);
	mpint s(key, keysize, offset);

	tmp[0] = &p.value;
	tmp[1] = &q.value;
	tmp[2] = &g.value;
	tmp[3] = &y.value;
	tmp[4] = NULL;
	tmp[5] = &r.value;
	tmp[6] = &s.value;
	tmp[7] = NULL;
	sexpr pkey("(public-key (dsa (p %m) (q %m) (g %m) (y %m)))", tmp);
	sexpr psig("(sig-val (dsa (r %m) (s %m)))", tmp + 5);

	unsigned int sbits = gcry_mpi_get_nbits(q.value);
	if(sbits <= 160) {
		algo = "sha1";
		hashalgo = GCRY_MD_SHA1;
		hashlen = 20;
	} else if(sbits <= 224) {
		algo = "sha224";
		hashalgo = GCRY_MD_SHA224;
		hashlen = 28;
	} else if(sbits <= 256) {
		algo = "sha256";
		hashalgo = GCRY_MD_SHA256;
		hashlen = 32;
	} else if(sbits <= 384) {
		algo = "sha384";
		hashalgo = GCRY_MD_SHA384;
		hashlen = 48;
	} else if(sbits <= 512) {
		algo = "sha512";
		hashalgo = GCRY_MD_SHA512;
		hashlen = 64;
	} else
		throw std::runtime_error("Unknown DSA key hash");

	digest d(hashalgo);
	gcry_md_write(d.hd, challenge, size);
	unsigned char* block = gcry_md_read(d.hd, 0);

	gcry_mpi_t _datahash;
	throw_error("scanning data hash", gcry_mpi_scan(&_datahash, GCRYMPI_FMT_USG, block, hashlen, NULL));
	mpint datahash(_datahash);

	//tmp[0] = (void*)&algo;
	//tmp[1] = &datahash.value;
	tmp[0] = &datahash;
	tmp[1] = NULL;
	sexpr verified_data("%m", tmp);

	throw_error("gcry_pk_verify", gcry_pk_verify(psig.expr, verified_data.expr, pkey.expr));

	return ukeysize;
}

/*
static std::vector<char> hash_input(int algo, const char* challenge, size_t size)
{
	std::vector<char> r;

	r.resize(gcry_md_get_algo_dlen(algo));
	gcry_md_hash_buffer(algo, &r[0], challenge, size);
	return r;
}
*/
const char* hextostring(const unsigned char* in, size_t len)
{
	static char buffer[512];
	for(size_t i = 0; i < len; i++)
		sprintf(buffer + 2 * i, "%02x", in[i]);
	buffer[2 * len] = 0;
	return buffer;
}

std::string handle_ssh_keypair(const std::string& data, const char* challenge, size_t size)
{
	const char* key = data.c_str();
	size_t keysize;
	if(is_rsa_key(data)) {
		keysize = handle_ssh_rsa_keypair(data, challenge, size);
	} else if(is_dsa_key(data)) {
		keysize = handle_ssh_dsa_keypair(data, challenge, size);
	} else
		throw std::runtime_error("Unknown SSH key format");

	digest d(GCRY_MD_SHA224);
	gcry_md_write(d.hd, key, keysize);
	unsigned char* hash = gcry_md_read(d.hd, 0);
	return std::string("ssh-") + hextostring(hash, 28);
}
