/*
 * Copyright (C) Ilari Liusvaara 2009
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */
#include "srp-udb.hpp"
#include <cstring>
#include <fstream>
#include <stdexcept>
#include <sstream>
#include <algorithm>
#include <log.hpp>

//Characters that are valid in SRP line plus list of characters not valid for Base-64 (: is counted
//as valid for Base-64 even if it is really not.
const char* valid_chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz./:_-";
const char* not_valid_base64 = "_-";

srp_udb* active_udb = NULL;

srp_udb::srp_udb() throw()
{
}

srp_udb::srp_udb(const std::string& filename) throw(std::bad_alloc, std::runtime_error)
{
	std::ifstream input(filename.c_str());

	std::string logicalline;

	bool will_continue = false;
	unsigned linecounter = 0;
	while(input) {
		std::string physicalline;
		std::getline(input, physicalline);
		linecounter++;
		//Skip comment lines. Note that continuation can't be comment.
		if(physicalline.length() > 0 && physicalline.at(0) == '#' && !will_continue)
			continue;
		//Check for invalid characters.
		size_t first_inval = physicalline.find_first_not_of(valid_chars);
		size_t len = physicalline.length();
		if(first_inval < len && (first_inval < len - 1 || physicalline.at(first_inval) != '\\')) {
			std::ostringstream err;
			err << "Illegal character in physical line " << linecounter;
			throw std::runtime_error(err.str());
		}

		//'\' at end of line makes next line to be continuation of current. The '\' is not considered
		//to be part of line.
		if(len > 0 && physicalline.at(len - 1) == '\\') {
			if(will_continue)
				logicalline = logicalline + physicalline.substr(0, len - 1);
			else
				logicalline = physicalline.substr(0, len - 1);
			will_continue = true;
		} else {
			if(will_continue)
				logicalline = logicalline + physicalline;
			else
				logicalline = physicalline;
			will_continue = false;
		}

		//Require non-empty complete line to proceed. Skip empty lines.
		if(will_continue)
			continue;
		if(logicalline == "")
			continue;

		//Split the line into components. Also check components 2-5 for non-legal Base-64 characters.
		//':' is the component separator.
		size_t split1 = std::string::npos;
		size_t split2 = std::string::npos;
		size_t split3 = std::string::npos;
		size_t split4 = std::string::npos;
		size_t split5 = std::string::npos;
		size_t inval = std::string::npos;
		split1 = logicalline.find_first_of(":", 0);
		if(split1 != std::string::npos)
			split2 = logicalline.find_first_of(":", split1 + 1);
		if(split2 != std::string::npos)
			split3 = logicalline.find_first_of(":", split2 + 1);
		if(split3 != std::string::npos)
			split4 = logicalline.find_first_of(":", split3 + 1);
		if(split4 != std::string::npos)
			split5 = logicalline.find_first_of(":", split4 + 1);
		if(split1 != std::string::npos)
			inval = logicalline.find_first_of(not_valid_base64, split1 + 1);
		if(split4 == std::string::npos || split5 != std::string::npos || inval != std::string::npos) {
			std::ostringstream err;
			err << "Syntax error in logcal line ending at line " << linecounter;
			throw std::runtime_error(err.str());
		}

		//Extract the components.
		std::string username = logicalline.substr(0, split1);
		std::string salt = logicalline.substr(split1 + 1, split2 - split1 - 1);
		std::string verifier = logicalline.substr(split2 + 1, split3 - split2 - 1);
		std::string g = logicalline.substr(split3 + 1, split4 - split3 - 1);
		std::string n = logicalline.substr(split4 + 1);

		//Actually add it to database.
		try {
			add_user(username, salt, verifier, g, n);
		} catch(std::exception& e) {
			std::ostringstream err;
			err << "Invalid data on logcal line ending at line " << linecounter
				<< ": " << e.what();
			throw std::runtime_error(err.str());
		}
		//Next logical line.
		logicalline = "";
	}
	//Check that reading ended in good order.
	if(!input && !input.eof())
		throw std::runtime_error("OS-level error reading SRP user database file");
	if(will_continue)
		throw std::runtime_error("Expected next line after \\, got EOF");
}

srp_udb::datum_t srp_udb::parse_base64(const std::string& spec) throw(std::bad_alloc, std::out_of_range)
{
	datum_t r;
	int s;
	gnutls_datum_t _base64;
	size_t base64len, reslen, reslen2;

	base64len = spec.length();
	//+1 is hack to get SRP decoding sizes right. The result sizes mod 3 with input size mod 4
	//are: 0 -> 0, 1 -> 1, 2 -> 1, 3 -> 2.
	reslen2 = reslen = (3 * base64len + 1) / 4;

	_base64.data = (unsigned char*)spec.c_str();
	_base64.size = base64len;

	r.resize(reslen);
	s = gnutls_srp_base64_decode(&_base64, (char*)&r[0], &reslen2);
	if(s < 0)
		throw std::out_of_range("Base64 decoding error");
	else if(reslen != reslen2)
		throw std::out_of_range("Internal error: Incorrect decoded data length");
	return r;
}

void srp_udb::add_user(const std::string& username, const std::string& salt, const std::string& verifier,
	const std::string& g, const std::string& n) throw(std::bad_alloc, std::out_of_range)
{
	datum_t _salt = parse_base64(salt);
	datum_t _verifier = parse_base64(verifier);
	datum_t _g = parse_base64(g);
	datum_t _n = parse_base64(n);

	salts[username] = _salt;
	verifiers[username] = _verifier;
	generators[username] = _g;
	moduli[username] = _n;
}


void srp_udb::callback_fn(const std::string& username, gnutls_datum_t& salt, gnutls_datum_t& verifier,
	gnutls_datum_t& g, gnutls_datum_t& n) throw(std::bad_alloc, std::out_of_range)
{
	//Does this user even exist?
	if(!salts.count(username))
		throw std::out_of_range("No such user");
	if(!verifiers.count(username))
		throw std::out_of_range("No such user");
	if(!generators.count(username))
		throw std::out_of_range("No such user");
	if(!moduli.count(username))
		throw std::out_of_range("No such user");

	//Yes, get parameters for it.
	const datum_t& _salt = salts[username];
	const datum_t& _verifier = verifiers[username];
	const datum_t& _generator = generators[username];
	const datum_t& _modulus = moduli[username];

	//Copy the parameters to output variables.
	try {
		salt.data = NULL;
		verifier.data = NULL;
		g.data = NULL;
		n.data = NULL;
		copy_datum(salt, _salt);
		copy_datum(verifier, _verifier);
		copy_datum(g, _generator);
		copy_datum(n, _modulus);
	} catch(std::exception& e) {
		if(salt.data)
			gnutls_free(salt.data);
		if(verifier.data)
			gnutls_free(verifier.data);
		if(g.data)
			gnutls_free(g.data);
		if(n.data)
			gnutls_free(n.data);
		salt.data = NULL;
		verifier.data = NULL;
		g.data = NULL;
		n.data = NULL;
		throw;
	}
}

void srp_udb::copy_datum(gnutls_datum_t& dest, const datum_t& src) throw(std::bad_alloc)
{
	dest.data = (unsigned char*)gnutls_malloc(src.size());
	if(!dest.data)
		throw std::bad_alloc();
	dest.size = src.size();
	std::copy(src.begin(), src.end(), dest.data);
}

void srp_udb::session_add(gnutls_session_t session, const std::string& user) throw(std::bad_alloc)
{
	sessions[session] = user;
}

std::string srp_udb::session_user(gnutls_session_t session) throw(std::bad_alloc)
{
	if(sessions.count(session))
		return sessions[session];
	else
		return std::string("");
}

void srp_udb::session_release(gnutls_session_t session) throw()
{
	if(sessions.count(session))
		sessions.erase(session);
}

int srp_callback_fn(gnutls_session_t session, const char* username, gnutls_datum_t* salt, gnutls_datum_t *verifier,
	gnutls_datum_t* g, gnutls_datum_t* n)
{
	try {
		std::string user(username);
		if(!active_udb)
			return -1;
		active_udb->callback_fn(user, *salt, *verifier, *g, *n);
		active_udb->session_add(session, user);
		return 0;
	} catch(std::exception& e) {
		return -1;
	}
}
