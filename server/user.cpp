/*
 * Copyright (C) Ilari Liusvaara 2009
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */
#include "user.hpp"
#include "user.h"
#include <sstream>
#include <sys/time.h>
#include <gnutls/openpgp.h>
#ifndef DISABLE_SRP
#include "srp-udb.hpp"
#endif

const int user_connection::still_active = USER_STILL_ACTIVE;
const int user_connection::connection_end = USER_CONNECTION_END;
const int user_connection::layer4_error = USER_LAYER4_ERROR;
const int user_connection::tls_error = USER_TLS_ERROR;
const int user_connection::tls_hand_error = USER_TLS_HAND_ERROR;
const int user_connection::ukill = USER_KILL;
const int user_connection::red_failure = USER_RED_FAILURE;
const int user_connection::utimeout = USER_TIMEOUT;

void user_connection::configure_tls(gnutls_session_t session) throw()
{
	user_configure_tls(underlying, session);
	set_session = session;
}

void user_connection::add_to_sets(int& bound, fd_set& rfds, fd_set& wfds, struct timeval& deadline) throw()
{
	user_add_to_sets(underlying, &bound, &rfds, &wfds, &deadline);
}

void user_connection::service(fd_set& rfds, fd_set& wfds) throw()
{
	user_service(underlying, &rfds, &wfds);
}

void user_connection::service_nofd() throw()
{
	user_service_nofd(underlying);
}

std::string user_connection::explain_failure(int code) throw(std::bad_alloc)
{
	return std::string(user_explain_failure(code));
}

bool user_connection::tls_configured() throw()
{
	return (user_tls_configured(underlying) != 0);
}

gnutls_session_t user_connection::get_tls() throw()
{
	return user_get_tls(underlying);
}

void user_connection::set_red_io(int rin, int rout, int rerr) throw()
{
	user_set_red_io(underlying, rin, rout, rerr);
}

void user_connection::clear_red_io() throw()
{
	user_clear_red_io(underlying);
}

void user_connection::clear_deadline() throw()
{
	user_clear_deadline(underlying);
}

void user_connection::send_red_in_eof() throw()
{
	user_send_red_in_eof(underlying);
}

user_connection::~user_connection() throw()
{
#ifndef DISABLE_SRP
	if(set_session)
		active_udb->session_release(set_session);
#endif
	circular_buffer::disappearing(user_get_red_in_force(underlying));
	circular_buffer::disappearing(user_get_red_out_force(underlying));
	circular_buffer::disappearing(user_get_red_err_force(underlying));
	user_release(underlying);
}

std::pair<int, std::string> user_connection::get_failure() throw(std::bad_alloc)
{
	int code = user_get_failure(underlying);
	const char* msg = user_get_error(underlying);
	if(msg)
		return std::make_pair(code, std::string(msg));
	else
		return std::make_pair(code, std::string(""));
}

circular_buffer user_connection::get_red_in() throw(std::bad_alloc, std::invalid_argument)
{
	cbuffer* cbuf = user_get_red_in(underlying);
	if(!cbuf)
		throw std::invalid_argument("Red in fd attached in get_red_in");
	circular_buffer circ(cbuf);
	return circ;
}

circular_buffer user_connection::get_red_out() throw(std::bad_alloc, std::invalid_argument)
{
	cbuffer* cbuf = user_get_red_out(underlying);
	if(!cbuf)
		throw std::invalid_argument("Red out fd attached in get_red_out");
	circular_buffer circ(cbuf);
	return circ;
}

circular_buffer user_connection::get_red_err() throw(std::bad_alloc, std::invalid_argument)
{
	cbuffer* cbuf = user_get_red_err(underlying);
	if(!cbuf)
		throw std::invalid_argument("Red err fd attached in get_red_err");
	circular_buffer circ(cbuf);
	return circ;
}

user_connection::user_connection(int black_fd, unsigned timeout_secs) throw(std::bad_alloc)
{
	underlying = user_create(black_fd, timeout_secs);
	if(!underlying)
		throw std::bad_alloc();
}

struct timeval to_timeout(const struct timeval& tv)
{
	struct timeval res;
	struct timeval tn;
	gettimeofday(&tn, NULL);
	if(tn.tv_sec > tv.tv_sec) {
		res.tv_sec = 0;
		res.tv_usec = 0;
	} else if(tn.tv_sec <= tv.tv_sec && tn.tv_usec <= tv.tv_usec) {
		res.tv_sec = tv.tv_sec - tn.tv_sec;
		res.tv_usec = tv.tv_usec - tn.tv_usec;
	} else if(tn.tv_sec < tv.tv_sec && tn.tv_usec > tv.tv_usec) {
		res.tv_sec = tv.tv_sec - tn.tv_sec - 1;
		res.tv_usec = 1000000 + tv.tv_usec - tn.tv_usec;
	} else {
		res.tv_sec = 0;
		res.tv_usec = 0;
	}
	return res;
}

void user_connection::tls_send_alert(gnutls_alert_description_t alert) throw()
{
	user_tls_send_alert(underlying, alert);
}

bool user_connection::red_out_eofd() throw()
{
	return (user_red_out_eofd(underlying) != 0);
}

bool user_connection::red_in_available() throw()
{
	return (user_get_red_in(underlying) != NULL);
}

bool user_connection::red_out_available() throw()
{
	return (user_get_red_out(underlying) != NULL);
}

bool user_connection::red_err_available() throw()
{
	return (user_get_red_err(underlying) != NULL);
}

/* Be ready in case some joker decides to use 1024 bit hash as fingerprint. */
#define KEYBUF 128

const char* hexes = "0123456789abcdef";

static std::string hextostring(const unsigned char* buf, size_t bufsize)
{
	std::ostringstream out;
	for(size_t i = 0; i < bufsize; i++) {
		out << hexes[(buf[i] >> 4) & 15];
		out << hexes[buf[i] & 15];
	}
	return out.str();
}

std::string user_connection::get_peer_identity() throw(std::bad_alloc)
{
	const gnutls_datum_t* certificate = NULL;
	unsigned int size = 0;

	gnutls_session_t session = this->get_tls();
	if(!session)
		//In case some 1D10T calls without TLS being read.
		return std::string("<UNINITIALIZED>");

	//The only certificate type we can handle is OpenPGP cert.
	if(gnutls_certificate_type_get(session) == GNUTLS_CRT_OPENPGP) {
		unsigned int vout;
		size_t vout2;
		unsigned char buffer[KEYBUF];
		gnutls_openpgp_crt_t cert;

		//Get the fingerprint of peer's certificate. For this, we need to load the certificate
		//first. Also verify certificate internal consistency (to detect tampering).
		certificate = gnutls_certificate_get_peers(session, &size);
		if(!certificate)
			goto no_cert;

		if(gnutls_openpgp_crt_init(&cert) < 0)
			throw std::bad_alloc();

		if(gnutls_openpgp_crt_import(cert, certificate, GNUTLS_OPENPGP_FMT_RAW) < 0)
			goto bad_cert_allocated;

		//Defend against subkey attack against GnuTLS.
                if(gnutls_openpgp_crt_get_subkey_count(cert))
			goto bad_cert_allocated;

		if(gnutls_openpgp_crt_verify_self(cert, 0, &vout) < 0 || vout)
			goto bad_cert_allocated;

		vout2 = KEYBUF;
		if(gnutls_openpgp_crt_get_fingerprint(cert, buffer, &vout2) < 0)
			goto bad_cert_allocated;

		gnutls_openpgp_crt_deinit(cert);

		return std::string("openpgp-") + hextostring(buffer, vout2);

bad_cert_allocated:
		//Can't load certificate or corrupt certificate.
		this->tls_send_alert(GNUTLS_A_BAD_CERTIFICATE);
		gnutls_openpgp_crt_deinit(cert);
		return std::string("<ERROR>");
	}
no_cert:
	//No luck with certificate. Try to obtain SRP userid next.
#ifndef DISABLE_SRP
	const char* srpuser = gnutls_srp_server_get_username(session);
	if(srpuser)
		return std::string("srp-") + srpuser;
#endif
	//No luck. Anonymous access.
	return std::string("");
}
