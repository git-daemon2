/*
 * Copyright (C) Ilari Liusvaara 2009
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */
#ifndef _certificate__hpp__included__
#define _certificate__hpp__included__

#include <gnutls/gnutls.h>
#include <string>
#include <stdexcept>

//Certificate class (for hostkeys).
class certificate
{
public:
//Read the specified hostkey file.
//
//Inputs:
//	filename	Filename to read.
//
//Exceptions:
//	std::bad_alloc	Not enough memory.
//	std::runtime_error
//			Things gone wrong (can't read hostkey, invalid hostkey, etc).
	certificate(const std::string& filename) throw(std::bad_alloc, std::runtime_error);
//Release hostkey.
	~certificate() throw();
//The public and private key.
	gnutls_datum_t private_key;
	gnutls_datum_t public_key;
private:
	//This class is not copyable.
	certificate(const certificate& c);
	certificate& operator=(const certificate& c);
};

#endif
