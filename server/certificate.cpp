/*
 * Copyright (C) Ilari Liusvaara 2009
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */
#include "misc.h"
#include "certificate.hpp"
#include "cbuffer.hpp"
#include "os_exception.hpp"
#include <unistd.h>
#include <fcntl.h>
#include <cstring>
#include <errno.h>

#define CERT_MAX 65536

certificate::certificate(const std::string& filename) throw(std::bad_alloc, std::runtime_error)
{
	circular_buffer content(CERT_MAX);
	int fd;
	unsigned char head[10];

	//Read the hostkey from file.
	fd = open(filename.c_str(), O_RDONLY);
	if(fd < 0)
		throw os_exception(errno);
	while(1) {
		try {
			size_t r = content.read_fd(fd);
			if(r == 0)
				break;
		} catch(os_exception& e) {
			if(e.get_error_code() == EAGAIN) {
				if(!content.free())
					throw std::runtime_error("Hostkey too big");
			} else if(e.get_error_code() != EINTR)
				throw;
		}
	}
	force_close(fd);

	//Try to parse the header.
	try {
		content.read(head, 9);
		head[9] = 0;
		if(strcmp((char*)head, "GITSSCERT") && strcmp((char*)head, "GITSUCERT"))
			throw std::runtime_error("");
	} catch(std::exception& e) {
		throw std::runtime_error("File is not a hostkey");
	}
	if(!strcmp((char*)head, "GITSSCERT"))
		throw std::runtime_error("Sealed hostkeys are not supported");

	//Read the actual contents (private and public keys).
	try {
		private_key.data = NULL;
		public_key.data = NULL;

		//Private part...
		content.read(head, 2);
		size_t privsize = ((size_t)head[0] << 8) | ((size_t)head[1]);
		private_key.data = (unsigned char*)gnutls_malloc(privsize);
		if(!private_key.data)
			throw std::bad_alloc();
		private_key.size = privsize;
		content.read(private_key.data, private_key.size);

		//And public part...
		content.read(head, 2);
		size_t pubsize = ((size_t)head[0] << 8) | ((size_t)head[1]);
		public_key.data = (unsigned char*)gnutls_malloc(pubsize);
		if(!public_key.data)
			throw std::bad_alloc();
		public_key.size = pubsize;
		content.read(public_key.data, public_key.size);

		//There should be nothing left over.
		if(content.used())
			throw std::runtime_error("");
	} catch(std::exception& e) {
		if(private_key.data)
			gnutls_free(private_key.data);
		if(public_key.data)
			gnutls_free(public_key.data);
		throw std::runtime_error("Error parsing hostkey");
	}
}

certificate::~certificate() throw()
{
	gnutls_free(private_key.data);
	gnutls_free(public_key.data);
}
