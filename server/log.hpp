/*
 * Copyright (C) Ilari Liusvaara 2009
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */
#ifndef _log__hpp__included__
#define _log__hpp__included__

#include <cstdarg>
#include <iosfwd>
#include <string>
#include <boost/iostreams/categories.hpp>
#include <boost/iostreams/stream.hpp>

//Stream sink class.
struct gd2_log_sink
{
public:
	typedef char char_type;
	typedef boost::iostreams::sink_tag category;

	//Constructor. Input severity for messages.
	gd2_log_sink(bool _notice) throw();
	//Write data to output.
	std::streamsize write(const char* s, std::streamsize n);
private:
	//Buffer to store incomplete lines.
	std::string linebuffer;
	//Severity for messages.
	bool notice;
};

//The actual error streams.
extern boost::iostreams::stream<gd2_log_sink> gd2_notice;
extern boost::iostreams::stream<gd2_log_sink> gd2_error;

#endif
