/*
 * Copyright (C) Ilari Liusvaara 2009-2010
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */

#ifdef WIN32

unixlistener::unixlistener(const std::string& _spec) throw(std::bad_alloc, os_exception, std::range_error)
{
	throw std::range_error("Unix sockets not supported");
}

unixlistener::~unixlistener() throw()
{
}

int unixlistener::get_fd() throw()
{
	return -1;
}

std::pair<int, std::string> unixlistener::accept() throw(std::bad_alloc, os_exception)
{
	throw os_exception(EAFNOSUPPORT);
}

bool unixlistener::class_supported(const std::string& spec) throw(std::runtime_exception, std::bad_alloc)
{
	return false;
}

#else


#ifdef USE_SO_PEERCRED
#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif
#include <sys/socket.h>
#include <sys/un.h>
#include <pwd.h>
#endif
#include "misc.h"
#include "unix.hpp"
#include "log.hpp"
#include "variables.hpp"
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <sys/un.h>
#include <cctype>
#include <netdb.h>
#include <string>
#include <errno.h>

unixlistener::unixlistener(const std::string& _spec, bool tls_flag) throw(std::bad_alloc, os_exception,
	std::range_error)
{
	struct sockaddr_un saddr;
	std::string spec = _spec;
	int plen;

	std::string aspec = spec.substr(0, 5);
	if(aspec != "unix:")
		throw std::range_error("Wrong listen spec type");
	spec = spec.substr(5);

	saddr.sun_family = AF_UNIX;
	strcpy(saddr.sun_path, spec.c_str());
	if(saddr.sun_path[0] == '@')
		saddr.sun_path[0] = '\0';

	int sfd = socket(AF_UNIX, SOCK_STREAM, 0);
	if(sfd < 0)
		throw os_exception(errno);

	if(reuseaddr) {
		int yes = 1;
		if(setsockopt(sfd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int)) < 0)
			throw os_exception(errno);
	}

	if(spec.length() > 0 && spec.at(0) == '@')
		plen = (int)((char*)saddr.sun_path - (char*)&saddr) + spec.length();
	else
		plen = (int)sizeof(saddr);
	if(bind(sfd, (struct sockaddr*)&saddr, plen) < 0)
		throw os_exception(errno);
	if(listen(sfd, 10) < 0)
		throw os_exception(errno);

	tlsonly_flag(tls_flag);

	fd = sfd;
}

unixlistener::~unixlistener() throw()
{
	force_close(fd);
}

int unixlistener::get_fd() throw()
{
	return fd;
}

std::pair<int, std::string> unixlistener::accept() throw(std::bad_alloc, os_exception)
{
	int afd;
	struct sockaddr_un saddr;
	socklen_t saddrlen = sizeof(saddr);

	if(!getuid() || !geteuid())
		throw os_exception(EPERM);	//Fuck, no!

	afd = ::accept(fd, (struct sockaddr*)&saddr, &saddrlen);
	if(afd < 0)
		throw os_exception(errno);

	return std::make_pair(afd, std::string("<unix domain socket>"));
}

bool unixlistener::class_supported(const std::string& spec) throw(std::runtime_error, std::bad_alloc)
{
	return (spec == "unix");
}

std::string unixlistener::special_authenticate(int fd) throw(std::bad_alloc)
{
#ifdef USE_SO_PEERCRED
	struct ucred cred;
	socklen_t credlen = sizeof(cred);
	struct passwd* userentry = NULL;
	if(getsockopt(fd, SOL_SOCKET, SO_PEERCRED, &cred, &credlen) >= 0)
		userentry = getpwuid(cred.uid);
	if(userentry)
		return std::string("unix-") + userentry->pw_name;
#endif
	return std::string("");
}


#endif
