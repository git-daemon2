/*
 * Copyright (C) Ilari Liusvaara 2009
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */
#include "os_exception.hpp"
#include <cerrno>
#include <cstring>

os_exception::os_exception(int error) throw(std::bad_alloc)
	: std::runtime_error(strerror(error)), errorcode(error)
{
}


int os_exception::get_error_code() throw()
{
	return errorcode;
}
