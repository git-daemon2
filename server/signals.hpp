/*
 * Copyright (C) Ilari Liusvaara 2009
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */
#ifndef _signals__hpp__included__
#define _signals__hpp__included__

#include <signal.h>

volatile extern sig_atomic_t sighup_received;
volatile extern sig_atomic_t sigterm_received;
volatile extern sig_atomic_t sigchld_received;

void initialize_signals();

#endif
