/*
 * Copyright (C) Ilari Liusvaara 2009
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */
#ifndef _variables__hpp__included__
#define _variables__hpp__included__

#include <gnutls/gnutls.h>
#include "certificate.hpp"

extern unsigned initial_timeout;
extern unsigned max_users;
extern bool use_syslog;
extern bool be_verbose;
extern bool do_detach;
#ifndef DISABLE_SRP
extern gnutls_srp_server_credentials_t srp_scred;
#endif
extern gnutls_certificate_credentials_t serv_cert;
extern gnutls_dh_params_t dh_params;
extern certificate* cert;
extern bool do_vhosts;
extern std::string vhost_postfix;
extern std::string anonymous_id;
extern std::string authorization_cmd;
extern bool reuseaddr;

#endif
