/*
 * Copyright (C) Ilari Liusvaara 2009-2010
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */
#include "misc.h"
#include "inet.hpp"
#include "log.hpp"
#include "variables.hpp"
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <cctype>
#include <netdb.h>
#include <string>
#include <errno.h>

namespace {
	uint32_t toipaddr(const std::string& spec) throw(std::range_error)
	{
		struct hostent* host;
		host = gethostbyname(spec.c_str());
		if(!host || !host->h_addr || host->h_addrtype != AF_INET)
			throw std::range_error("Bad listen address '" + spec + "'");
		return *(uint32_t*)host->h_addr;
	}

	int toprotocol(const std::string& spec) throw(std::range_error)
	{
		struct protoent* proto;
		proto = getprotobyname(spec.c_str());
		if(!proto)
			throw("Bad protocol name '" + spec + "'");
		return proto->p_proto;
	}

	uint16_t toservice(const std::string& sspec, const std::string& pspec) throw(std::range_error)
	{
		struct servent* serv;
		try {
			return to_port_number(sspec);
		} catch(...) {
		}
		serv = getservbyname(sspec.c_str(), pspec.c_str());
		if(!serv)
			throw("Bad port number '" + sspec + "/" + pspec + "'.");
		return (uint16_t)serv->s_port;
	}
}

bool inetlistener::class_supported(const std::string& _spec) throw(std::runtime_error, std::bad_alloc)
{
	int i, proto;
	std::string spec = _spec;
	size_t slash = spec.find_first_of("/");
	std::string protocol = spec.substr(0, slash);
	std::string addrspace = spec.substr(slash + 1);

	if(addrspace != "ipv4")
		return false;

	try {
		proto = toprotocol(protocol);
	} catch(...) {
		return false;
	}

	i = socket(AF_INET, SOCK_STREAM, proto);
	if(i < 0)
		return false;
	close(i);

	return true;
}

inetlistener::inetlistener(const std::string& _spec, bool tls_flag)
	throw(std::bad_alloc, os_exception, std::range_error)
{
	uint32_t ip = INADDR_ANY;
	struct sockaddr_in saddr;
	std::string spec = _spec;

	af = AF_INET;

	size_t col = spec.find_first_of(":");
	if(col == std::string::npos)
		throw("Bad address specification format");
	std::string clazz = spec.substr(0, col);
	spec = spec.substr(col + 1);

	size_t slash = clazz.find_first_of("/");
	std::string protocol = clazz.substr(0, slash);
	std::string addrspace = clazz.substr(slash + 1);

	if(addrspace != "ipv4")
		throw("Invalid address space '" + addrspace + "'");

	std::string host = extract_host(spec);
	std::string port = extract_port(spec, tls_flag);
	if(host != "")
		ip = toipaddr(host);

	saddr.sin_family = AF_INET;
	saddr.sin_port = htons(toservice(port, protocol));
	saddr.sin_addr.s_addr = ip;

	int sfd = socket(AF_INET, SOCK_STREAM, toprotocol(protocol));
	if(sfd < 0)
		throw os_exception(errno);

	if(reuseaddr) {
		int yes = 1;
		if(setsockopt(sfd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int)) < 0)
			throw os_exception(errno);
	}

	if(bind(sfd, (struct sockaddr*)&saddr, sizeof(saddr)) < 0)
		throw os_exception(errno);
	if(listen(sfd, 10) < 0)
		throw os_exception(errno);

	tlsonly_flag(tls_flag);

	fd = sfd;
}

inetlistener::~inetlistener() throw()
{
	force_close(fd);
}

int inetlistener::get_fd() throw()
{
	return fd;
}

std::pair<int, std::string> inetlistener::accept() throw(std::bad_alloc, os_exception)
{
	int afd;
	struct sockaddr_in saddr;
	socklen_t saddrlen = sizeof(saddr);
	char buffer[32];

	if(!getuid() || !geteuid())
		throw os_exception(EPERM);	//Fuck, no!

	afd = ::accept(fd, (struct sockaddr*)&saddr, &saddrlen);
	if(afd < 0)
		throw os_exception(errno);

	if(saddr.sin_family != AF_INET)
		return std::make_pair(afd, std::string("<UNKNOWN address family>"));

	sprintf(buffer, "%s:%u", inet_ntoa(saddr.sin_addr), ntohs(saddr.sin_port));
	return std::make_pair(afd, std::string(buffer));
}
