/*
 * Copyright (C) Ilari Liusvaara 2009-2010
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */
#include "misc.h"
#include "userchange.hpp"
#include "variables.hpp"
#include "log.hpp"
#include "listensock.hpp"
#include "userchange.hpp"
#ifndef DISABLE_SRP
#include "srp-udb.hpp"
#endif
#include "signals.hpp"
#include <sys/types.h>
#include <sys/wait.h>
#include <signal.h>
#include <cerrno>
#include <gnutls/gnutls.h>
#include <gnutls/openpgp.h>
#include <list>
#include <iostream>

static pid_t current_child = 0;
static pid_t new_child = 0;
static bool gnutls_global_init_done = false;

bool reuseaddr = false;

void server_mainloop(std::vector<listener*>& listeners);

#ifndef DISABLE_SRP
#define OPT_SRPFILE "--srp-file="
#endif
#define OPT_HOSTKEY "--host-key="
#define OPT_USER "--user="
#define OPT_DHBITS "--dhbits="
#define OPT_TIMEOUT "--timeout="
#define OPT_MAXUSERS "--max-users="
#define OPT_VHOST "--vhost="
//OPT_VHOST2 must OPT_VHOST minus trailing '='.
#define OPT_VHOST2 "--vhost"
#define OPT_ANONYMOUS "--anonymous-user="
#define OPT_AUTHORIZATION "--authorization-cmd="

#define OPT_LISTEN_TCPV4 "--listen-tcpv4"
#define OPT_LISTEN_TCPV6 "--listen-tcpv6"
#define OPT_LISTEN "--listen="
#define OPT_TLS_LISTEN "--tls-listen="

#define OPT_DETACH "--detach"
#define OPT_VERBOSE "--verbose"
#define OPT_REUSEADDR "--reuseaddr"
#define OPT_HELP "--help"

#define LENOF(X) strlen(X)

//Make integer out of string, with specified bounds.
static unsigned int_interp(const std::string& value, unsigned lowbound, unsigned highbound)
{
	const char* str = value.c_str();
	char* end;
	unsigned long v;

	v = strtoul(str, &end, 10);
	if(*end)
		throw std::runtime_error("Invalid number");
	if(v < lowbound)
		throw std::runtime_error("Value too small");
	if(v > highbound)
		throw std::runtime_error("Value too large");
	return v;
}

//Do the actual listener startup sequence.
static int startup_sequence(std::vector<std::string>& arguments, std::vector<listener*>& listeners)
{
	int p, s;
	int sigpipe[2];
	struct userchanger* changer = NULL;
#ifndef DISABLE_SRP
	std::string srp_file = "";
#endif
	std::string certfile = "";
	int dh_bits = 1024;

	//Parse the remaining arguments.
	for(std::vector<std::string>::iterator i = arguments.begin(); i != arguments.end(); i++) {
		try {
#ifndef DISABLE_SRP
			if(i->substr(0, LENOF(OPT_SRPFILE)) == OPT_SRPFILE)
				srp_file = i->substr(LENOF(OPT_SRPFILE));
#endif
			if(i->substr(0, LENOF(OPT_HOSTKEY)) == OPT_HOSTKEY)
				certfile = i->substr(LENOF(OPT_HOSTKEY));
			if(i->substr(0, LENOF(OPT_USER)) == OPT_USER) {
				if(changer)
					delete changer;
				changer = NULL;
				changer = new userchanger(i->substr(LENOF(OPT_USER)));
			}
			if(i->substr(0, LENOF(OPT_DHBITS)) == OPT_DHBITS)
				dh_bits = int_interp(i->substr(LENOF(OPT_DHBITS)), 1024, 3072);
			if(i->substr(0, LENOF(OPT_TIMEOUT)) == OPT_TIMEOUT)
				initial_timeout = int_interp(i->substr(LENOF(OPT_TIMEOUT)), 1, 900);
			if(i->substr(0, LENOF(OPT_MAXUSERS)) == OPT_MAXUSERS)
				max_users = int_interp(i->substr(LENOF(OPT_MAXUSERS)), 0, 10000);
			if(i->substr(0, LENOF(OPT_VHOST)) == OPT_VHOST)
				vhost_postfix = i->substr(LENOF(OPT_VHOST));
			if(i->substr(0, LENOF(OPT_VHOST2)) == OPT_VHOST2)
				do_vhosts = true;
			if(i->substr(0, LENOF(OPT_ANONYMOUS)) == OPT_ANONYMOUS)
				anonymous_id = i->substr(LENOF(OPT_ANONYMOUS));
			if(i->substr(0, LENOF(OPT_AUTHORIZATION)) == OPT_AUTHORIZATION)
				authorization_cmd = i->substr(LENOF(OPT_AUTHORIZATION));
		} catch(std::exception& e) {
			gd2_error << "Failed to process argument " << *i << ": " << e.what() << std::endl;
			exit(1);
		}
	}

	//If no userchanger created by parameter processing, create one here.
	if(!changer) {
		try {
			if(!getuid() || !geteuid()) {
				gd2_error << "--user must be specified if running as root." << std::endl;
				return -1;
			}
			changer = new userchanger();
		} catch(std::exception& e) {
			gd2_error << "Can't create dummy userchanger: " << e.what() << std::endl;
			return -1;
		}
	}

	//Check that authorization command is given. We can't check access to it yet, because that
	//requires executing as same account as listener process.
	if(authorization_cmd == "") {
		gd2_error << "Authorization command must be specified (--authorization-cmd)" << std::endl;
		return -1;
	}

	//Check that hostkey is specified.
	if(certfile == "") {
		gd2_error << "Host key must be specified (--hostkey)" << std::endl;
		delete changer;
		return -1;
	}

	//Initialize GnuTLS if not done yet.
	if(!gnutls_global_init_done) {
		if(gnutls_global_init() < 0) {
			gd2_error << "Can't initialize GnuTLS." << std::endl;
			delete changer;
			return -1;
		}
		gnutls_global_init_done = true;
	}


#ifndef DISABLE_SRP
	//Load SRP users database.
	if(active_udb) {
		delete active_udb;
		active_udb = NULL;
	}
	try {
		if(srp_file != "")
			active_udb = new srp_udb(srp_file);
		else
			active_udb = new srp_udb();
	} catch(std::exception& e) {
		gd2_error << "Can't read SRP users database." << std::endl;
		delete changer;
		return -1;
	}

	//Initialize SRP credentials so connections can accept SRP.
	if(!srp_scred) {
		s = gnutls_srp_allocate_server_credentials(&srp_scred);
		if(s < 0) {
			gd2_error << "Can't allocate SRP creds: " << gnutls_strerror(s) << std::endl;
			delete changer;
			return -1;
		}
		gnutls_srp_set_server_credentials_function(srp_scred, srp_callback_fn);
	}
#endif

	//Initialize Diffie-Hellman parameters for non-SRP connections.
	if(dh_params) {
		gnutls_dh_params_deinit(dh_params);
		dh_params = NULL;
	}
	s = gnutls_dh_params_init(&dh_params);
	if(s < 0) {
		gd2_error << "Can't allocate DH parameters: " << gnutls_strerror(s) << std::endl;
		delete changer;
		return -1;
	}
	s = gnutls_dh_params_generate2(dh_params, dh_bits);
	if(s < 0) {
		gd2_error << "Can't generate DH parameters: " << gnutls_strerror(s) << std::endl;
		delete changer;
		return -1;
	}

	//Load the hostkey.
	try {
		int s = 0;

		if(cert) {
			delete cert;
			cert = NULL;
		}
		cert = new certificate(certfile);
		s = gnutls_certificate_allocate_credentials(&serv_cert);
		if(s < 0)
			throw std::runtime_error(gnutls_strerror(s));
		s = gnutls_certificate_set_openpgp_keyring_mem(serv_cert, cert->public_key.data,
			cert->public_key.size, GNUTLS_OPENPGP_FMT_RAW);
		if(s < 0)
			throw std::runtime_error(gnutls_strerror(s));
		s = gnutls_certificate_set_openpgp_key_mem(serv_cert, &cert->public_key, &cert->private_key,
			GNUTLS_OPENPGP_FMT_RAW);
		if(s < 0)
			throw std::runtime_error(gnutls_strerror(s));
	} catch(std::exception& e) {
		gd2_error << "Can't load hostkey: " << e.what() << std::endl;
		delete changer;
		return -1;
	}

	//Assign Diffie-Hellman parameters to server hostkey.
	gnutls_certificate_set_dh_params(serv_cert, dh_params);

	//Create signaling pipe to signal exec failure down.
	if(pipe(sigpipe) < 0) {
		gd2_error << "Failed to create signaling pipe: " << strerror(errno) << std::endl;
		delete changer;
		return -1;
	}

	p = fork();
	if(p < 0) {
		//Failed. Close the pipes and return.
		force_close(sigpipe[0]);
		force_close(sigpipe[1]);
		gd2_error << "Failed to fork new listener: " << strerror(errno) << std::endl;
		delete changer;
		return -1;
	} else if(p == 0) {
		//Clear SIGTERM to avoid process immediately quitting on inherited SIGTERM.
		sigterm_received = false;

		//Clear any masked signals so signal reporting works.
		sigset_t emptyset;
		sigemptyset(&emptyset);
		sigprocmask(SIG_SETMASK, &emptyset, NULL);

		//Close read end of signal pipe. Not needed.
		force_close(sigpipe[0]);

		//Do the user change.
		try {
			changer->apply_user_change();
		} catch(std::exception& e) {
			gd2_error << "Failed to change user: " << e.what() << std::endl;
			exit(1);
		}

		//Check that authorization command is runnable.
		if(access(authorization_cmd.c_str(), X_OK) < 0) {
			gd2_error << "No execute permissions for specified authorization command." << std::endl;
			exit(1);
		}

		//Signal success and enter main loop.
		char buf = 'Y';
		while(write(sigpipe[1], &buf, 1) < 1);
		close_link();
		server_mainloop(listeners);
		exit(0);
	} else if(p > 0) {
		//Mark the newly created process as new child process.
		new_child = p;
		delete changer;
		//Close write end of signal pipe to catch EOFs properly.
		force_close(sigpipe[1]);

		//Wait for success or failure notice to come from signal pipe. EOF signals failure, reading 'Y'
		//signals success.
		while(1) {
			char buf;
			size_t r = read(sigpipe[0], &buf, 1);
			if(r == 0) {
				force_close(sigpipe[0]);
				new_child = 0;
				return -1;
			} else if(r > 0) {
				force_close(sigpipe[0]);
				return 0;
			}
		}
	}
	// Can't really come here.
	return -1;
}

//Main management process loop.
static void manage_loop(std::vector<std::string>& arguments)
{
	std::vector<listener*> l;

	//Create the listeners.
	for(std::vector<std::string>::iterator i = arguments.begin(); i != arguments.end(); i++) {
		try {
			if(*i == OPT_LISTEN_TCPV4)
				l.push_back(&listener::get_listener("tcp/ipv4::9418", false));
			if(*i == OPT_LISTEN_TCPV6)
				l.push_back(&listener::get_listener("tcp/ipv6::9418", false));
			if(i->substr(0, LENOF(OPT_LISTEN)) == OPT_LISTEN) {
				std::string spec = i->substr(LENOF(OPT_LISTEN));
				l.push_back(&listener::get_listener(spec, false));
			}
			if(i->substr(0, LENOF(OPT_TLS_LISTEN)) == OPT_TLS_LISTEN) {
				std::string spec = i->substr(LENOF(OPT_TLS_LISTEN));
				l.push_back(&listener::get_listener(spec, true));
			}
		} catch(std::exception& e) {
			gd2_error << "Failed to process argument " << *i << ": " << e.what() << std::endl;
			exit(1);
		}
	}

	//We need to listen at least one port.
	if(l.size() == 0) {
		gd2_error << "Need at least one --listen or --tls-listen" << std::endl;
		exit(1);
	}

	//Start the listner.
	if(startup_sequence(arguments, l) < 0) {
		gd2_error << "Failed to start listener process" << std::endl;
		exit(1);
	}
	//Let the possible starter process to exit in order to return the terminal to invoker.
	close_link();

	//Make new child our primary listener process.
	current_child = new_child;
	new_child = 0;

	//This is the main daemon loop for manager process. Listen for signals and react to them.
	while(true) {
		//Signal sets so we can block signals in order to relably wait for them.
		sigset_t emptyset;
		sigset_t important;
		sigemptyset(&emptyset);
		sigemptyset(&important);
		sigaddset(&important, SIGHUP);
		sigaddset(&important, SIGTERM);
		sigaddset(&important, SIGCHLD);

		//SIGTERM causes the whole daemon to exit. Kill listeners and exit.
		if(sigterm_received) {
			gd2_notice << "SIGTERM received, exiting." << std::endl;
			if(current_child)
				kill(current_child, SIGTERM);
			if(new_child)
				kill(new_child, SIGTERM);
			exit(0);
		}
		//SIGHUP restarts listener. Rerun listner startup sequence and if successful, kill the
		//old listener (so that it exits when current users disconnect).
		if(sighup_received) {
			sighup_received = false;
			gd2_notice << "Restarting listener process..." << std::endl;
			if(new_child) {
				gd2_error << "Previous child restart still in progress." << std::endl;
				continue;	//Only start one new process at time.
			}
			if(startup_sequence(arguments, l) < 0) {
				gd2_error << "Failed to start new listener process" << std::endl;
				new_child = 0;
				continue;
			}
			if(current_child)
				kill(current_child, SIGTERM);
			gd2_notice << "Listener process restarted." << std::endl;
			current_child = new_child;
			new_child = 0;
		}
		//SIGCHLD. Reap the exited listner process. Restart it if it crashed.
		if(sigchld_received) {
			pid_t pid;
			int status;
			sigchld_received = false;
			gd2_notice << "SIGCHLD received, reaping zombies..." << std::endl;
			while((pid = waitpid(-1, &status, WNOHANG)) > 0) {
				if(WIFEXITED(status) && WEXITSTATUS(status)) {
					gd2_error << "Listener process exited with code " << WEXITSTATUS(status)
						<< std::endl;
				} else if(WIFSIGNALED(status)) {
					gd2_error << "Listener process crashed with signal " << WTERMSIG(status)
						<< std::endl;
				}
				if(pid == current_child) {
					gd2_error << "Listener process died, restarting..." << std::endl;
					while(startup_sequence(arguments, l) < 0) {
						gd2_error << "Failed to start listener process" << std::endl;
						sleep(30);
					}
					current_child = new_child;
					new_child = 0;
				}
			}
		}

		//Wait for SIGCHLD, SIGHUP  or SIGTERM, those are all the interesting events here.
		sigprocmask(SIG_SETMASK, &important, NULL);
		if(sighup_received || sigterm_received || sigchld_received)
			continue;
		sigsuspend(&emptyset);
	}
}

void do_help()
{
	std::cout << "git-daemon2: Server for git:// and gits://." << std::endl;
	std::cout << std::endl;
	std::cout << "Command line options:" << std::endl;
	std::cout << OPT_VHOST2 << std::endl;
	std::cout << "\tOptional, default no vhosting" << std::endl;
	std::cout << "\tEnable virtual hosting. If client sends hostname, prepend" << std::endl;
	std::cout << "\t'/<hostname>/' to requested path." << std::endl;
	std::cout << OPT_VHOST << "<postfix>" << std::endl;
	std::cout << "\tOptional, default no vhosting" << std::endl;
	std::cout << "\tEnable virtual hosting. If client sends hostname, prepend" << std::endl;
	std::cout << "\t'<hostname>/' to requested path, but strip postfix matching" << std::endl;
	std::cout << "\t<postfix> if found. '.' in start of <postfix> matches empty" << std::endl;
	std::cout << "\tstring at start of sent hostname. If hostname becomes empty," << std::endl;
	std::cout << "\tdon't append anything." << std::endl;
	std::cout << OPT_LISTEN_TCPV4 << std::endl;
	std::cout << "\tOptional, default no" << std::endl;
	std::cout << "\tSame as '--listen=tcp4:9418'." << std::endl;
	std::cout << OPT_LISTEN_TCPV6 << std::endl;
	std::cout << "\tOptional, default no" << std::endl;
	std::cout << "\tSame as '--listen=tcp6:9418'." << std::endl;
	std::cout << OPT_DETACH << std::endl;
	std::cout << "\tOptional, default don't detach" << std::endl;
	std::cout << "\tDetach from console and become daemon after startup." << std::endl;
	std::cout << OPT_VERBOSE << std::endl;
	std::cout << "\tOptional, default disable verbose mode" << std::endl;
	std::cout << "\tEnable verbose mode." << std::endl;
	std::cout << OPT_REUSEADDR << std::endl;
	std::cout << "\tOptional, default don't use SO_REUSADDR" << std::endl;
	std::cout << "\tSet SO_REUSEADDR when binding to sockets." << std::endl;
	std::cout << OPT_HELP << std::endl;
	std::cout << "\tThis help." << std::endl;
#ifndef DISABLE_SRP
	std::cout << OPT_SRPFILE << "<file>" << std::endl;
	std::cout << "\tOptional, default empty database" << std::endl;
	std::cout << "\tUse <file> as SRP password database (note: format is not" << std::endl;
	std::cout << "\tcompatible with SRP tpasswd files, but SRP tpasswd and" << std::endl;
	std::cout << "\ttpasswd.conf files are convertable with some copy and" << std::endl;
	std::cout << "\tpaste). The file is read before switching users and reread" << std::endl;
	std::cout << "\ton SIGHUP. Username 'foo' authenticated using SRP appears" << std::endl;
	std::cout << "\tto authorization as 'srp-foo'." << std::endl;
#endif
	std::cout << OPT_HOSTKEY << "<file>" << std::endl;
	std::cout << "\tMandatory" << std::endl;
	std::cout << "\tUse <file> as hostkey. This file is read before switching" << std::endl;
	std::cout << "\tusers and reread on SIGHUP." << std::endl;
	std::cout << OPT_USER << "<user>" << std::endl;
	std::cout << "\tMandatory if running as root, otherwise not allowed" << std::endl;
	std::cout << "\tSwitch to <user> before accepting connections. The group and" << std::endl;
	std::cout << "\tsupplementary groups to use are taken from system users and" << std::endl;
	std::cout << "\tgroups databases." << std::endl;
	std::cout << OPT_DHBITS << "<bits>" << std::endl;
	std::cout << "\tOptional, default 1024" << std::endl;
	std::cout << "\tSet number of bits for Diffie-Hellman session keys. Allowed" << std::endl;
	std::cout << "\trange is 1024-3072. The Diffie-Hellman group for session keys" << std::endl;
	std::cout << "\tis regenerated on every startup and on every SIGHUP." << std::endl;
	std::cout << "\tNote that longer keys are slower." << std::endl;
	std::cout << OPT_TIMEOUT << "<timeout>" << std::endl;
	std::cout << "\tOptional, default 10" << std::endl;
	std::cout << "\tSet number seconds allowed from connection being established" << std::endl;
	std::cout << "\tto service request. If client doesn't do it fast enough, time" << std::endl;
	std::cout << "\tit out. Allowed range is 1-900. Note that this doesn't affect" << std::endl;
	std::cout << "\tpost service request operation in any way." << std::endl;
	std::cout << OPT_MAXUSERS << "<maxusers>" << std::endl;
	std::cout << "\tOptional, default 32" << std::endl;
	std::cout << "\tSet number user allowed simultaneously. 0 disables limiting," << std::endl;
	std::cout << "\totherwise range is 1-10000." << std::endl;
	std::cout << OPT_ANONYMOUS << "<identity>" << std::endl;
	std::cout << "\tOptional, default 'anonymous'" << std::endl;
	std::cout << "\tSet name to use for anonymous user. " << std::endl;
	std::cout << OPT_AUTHORIZATION << "<program>" << std::endl;
	std::cout << "\tMandatory" << std::endl;
	std::cout << "\tSet name of authorization program. <program> should be" << std::endl;
	std::cout << "\tabsolute path (its relative to home directory of user" << std::endl;
	std::cout << "\trunning listener). If using git-daemon2 with Gitolite, should" << std::endl;
	std::cout << "\tbe set to point to gl-auth-command executable." << std::endl;
	std::cout << OPT_LISTEN << "<spec>" << std::endl;
	std::cout << "\tAt least one " << OPT_LISTEN " or " << OPT_TLS_LISTEN << "is mandatory." << std::endl;
	std::cout << "\tListen for port <spec> for git:// and gits:// connections." << std::endl;
	std::cout << OPT_TLS_LISTEN << "<spec>" << std::endl;
	std::cout << "\tAt least one " << OPT_LISTEN " or " << OPT_TLS_LISTEN << "is mandatory." << std::endl;
	std::cout << "\tListen for port <spec> for gits::tls:// connections." << std::endl;
	std::cout << std::endl;
	std::cout << "Listen spec formats:" << std::endl;
	std::cout << "tcp/ipv4::<port>" << std::endl;
	std::cout << "\tListen to TCP/IP port <port>" << std::endl;
	std::cout << "tcp/ipv4:<ipaddr>:<port>" << std::endl;
	std::cout << "\tListen to TCP/IP port <port> on address <ipaddr>" << std::endl;
	std::cout << "tcp/ipv6::<port>" << std::endl;
	std::cout << "\tListen to TCP/IPv6 port <port>. On some systems this also" << std::endl;
	std::cout << "\tcovers TCP/IP port <port>." << std::endl;
	std::cout << "tcp/ipv6:[<ipaddr>]:<port>" << std::endl;
	std::cout << "\tListen to TCP/IPv6 port <port> on address <ipaddr>" << std::endl;
	std::cout << "tcp/ipv6:[<ipaddr>%<ifidx>]:<port>" << std::endl;
	std::cout << "\tListen to TCP/IPv6 port <port> on address <ipaddr> on" << std::endl;
	std::cout << "\tinterface index <ifidx>. Lets to bind to link-local" << std::endl;
	std::cout << "\taddresses." << std::endl;
	std::cout << "unix:<absolute-path>" << std::endl;
	std::cout << "\tListen to Unix domain socket at <absolute-path>." << std::endl;
	std::cout << "\tThe path must begin with '/'." << std::endl;
	std::cout << "unix:@/<socket-name>" << std::endl;
	std::cout << "\tListen to Unix domain socket with abstract name /<scoket_name>." << std::endl;
	std::cout << "\tNot all systems support this (mainly Linux)." << std::endl;
	exit(0);
}

int main(int argc, char** argv)
{
	int fd;
	bool errors = false;
	std::vector<std::string> arguments;
	std::list<std::string> full_options;
	std::list<std::string> prefix_options;

	//Assemble list of options.
	full_options.push_back(OPT_VHOST2);
	full_options.push_back(OPT_LISTEN_TCPV4);
	full_options.push_back(OPT_LISTEN_TCPV6);
	full_options.push_back(OPT_DETACH);
	full_options.push_back(OPT_VERBOSE);
	full_options.push_back(OPT_REUSEADDR);
	full_options.push_back(OPT_HELP);
#ifndef DISABLE_SRP
	prefix_options.push_back(OPT_SRPFILE);
#endif
	prefix_options.push_back(OPT_HOSTKEY);
	prefix_options.push_back(OPT_USER);
	prefix_options.push_back(OPT_DHBITS);
	prefix_options.push_back(OPT_TIMEOUT);
	prefix_options.push_back(OPT_MAXUSERS);
	prefix_options.push_back(OPT_VHOST);
	prefix_options.push_back(OPT_ANONYMOUS);
	prefix_options.push_back(OPT_AUTHORIZATION);
	prefix_options.push_back(OPT_LISTEN);
	prefix_options.push_back(OPT_TLS_LISTEN);

	//Initialize signal handlers.
	initialize_signals();

	//Parse some very imporant options and check options.
	for(int i = 1; i < argc; i++) {
		if(!strcmp(argv[i], OPT_DETACH))
			do_detach = true;
		if(!strcmp(argv[i], OPT_VERBOSE))
			be_verbose = true;
		if(!strcmp(argv[i], OPT_REUSEADDR))
			reuseaddr = true;
		if(!strcmp(argv[i], OPT_HELP))
			do_help();

		std::string astr = argv[i];
		for(std::list<std::string>::iterator i = full_options.begin(); i != full_options.end(); i++)
			if(*i == astr)
				goto ok;
		for(std::list<std::string>::iterator i = prefix_options.begin(); i != prefix_options.end(); i++)
			if(*i == astr.substr(0, i->length()))
				goto ok;
		gd2_error << "Unknown argument: " << astr << std::endl;
		return 1;
ok:
		;
	}

	//Become daemon if daemon is requested.
	try {
		fd = become_daemon();
	} catch(std::exception& e) {
		gd2_error << "Unable to become daemon: " << e.what() << std::endl;
		return 1;
	}

	//Wait for daemon to really start up.
	while(fd >= 0) {
		char buffer[257];
		memset(buffer, 0, 257);
		ssize_t r = read(fd, buffer, 256);
		if(r == 0)
			return (errors ? 1 : 0);
		else if(r > 0) {
			errors = true;
			gd2_error << buffer;
		}
	}

	//Convert arguments to form suitable for higher functions.
	for(int i = 1; i < argc; i++)
		arguments.push_back(argv[i]);

	//Now, we are in management process.
	manage_loop(arguments);

	return 0;
}
