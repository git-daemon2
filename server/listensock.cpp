/*
 * Copyright (C) Ilari Liusvaara 2009-2010
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */
#include "listensock.hpp"
#include "inet.hpp"
#include "unix.hpp"

listener::~listener() throw()
{
}

listener::listener() throw()
{
	_tlsonly_flag = false;
}

listener& listener::get_listener(const std::string& spec, bool tls_flag) throw(std::range_error, os_exception,
	std::bad_alloc)
{
	std::string _spec = spec;
	size_t col = _spec.find_first_of(":");
	std::string clazz;
	if(col != std::string::npos)
		clazz = _spec.substr(0, col);
	else
		throw std::range_error("Invalid specification syntax");
	//Try listeners one by one until one accepts or all reject.
	try {
		if(inetlistener::class_supported(clazz))
			return *new inetlistener(spec, tls_flag);
	} catch(std::exception& e) {
		throw std::range_error("Invalid specification: " + std::string(e.what()));
	}
	try {
		if(unixlistener::class_supported(clazz))
			return *new unixlistener(spec, tls_flag);
	} catch(std::range_error& e) {
	}
	throw std::range_error("No handler knows about class '" + clazz + "'");
}

std::string listener::special_authenticate(int fd) throw(std::bad_alloc)
{
	return std::string("");
}


void listener::tlsonly_flag(bool flag) throw()
{
	_tlsonly_flag = flag;
}

bool listener::tlsonly_flag() throw()
{
	return _tlsonly_flag;
}

//Parse character as base-10 digit.
static int char_to_int(char ch)
{
	switch(ch) {
	case '0':
		return 0;
	case '1':
		return 1;
	case '2':
		return 2;
	case '3':
		return 3;
	case '4':
		return 4;
	case '5':
		return 5;
	case '6':
		return 6;
	case '7':
		return 7;
	case '8':
		return 8;
	case '9':
		return 9;
	default:
		return -1;
	}
}

unsigned short to_port_number(const std::string& spec) throw(std::range_error)
{
	unsigned short port = 0;

	//Blank string is not valid number.
	if(spec.length() == 0)
		throw  std::range_error("Invalid port number");

	//Valid port numbers are 1-65535
	for(unsigned i = 0; i < spec.length(); i++) {
		char ch = char_to_int(spec.at(i));
		if(ch < 0)
			throw std::range_error("Invalid port number");
		if(ch == 0 && port == 0)
			throw std::range_error("Invalid port number");
		if(port > 6553 || (port > 6552 && ch > 5))
			throw std::range_error("Invalid port number");
		port = port * 10 + ch;
	}
	return port;
}

uint32_t touint32_t(const std::string& spec) throw(std::range_error)
{
	uint32_t x = 0;

	//Blank string is not valid number.
	if(spec.length() == 0)
		throw  std::range_error("Invalid number");

	//0 is special case, makes it easier to deal with zeros.
	if(spec == "0")
		return 0;

	//Valid uints numbers are 0-2^32-1, but because we handled 0 as special case,
	//the valid range can be 1-2^32-1 here.
	for(unsigned i = 0; i < spec.length(); i++) {
		char ch = char_to_int(spec.at(i));
		if(ch < 0)
			throw std::range_error("Invalid number");
		if(ch == 0 && x == 0)
			throw std::range_error("Invalid number");
		if(x > 429496729 || (x > 429496728 && ch > 5))
			throw std::range_error("Invalid number");
		x = x * 10 + ch;
	}
	return x;
}

std::string extract_host(const std::string& _hostport) throw(std::range_error, std::bad_alloc)
{
	std::string hostport = _hostport;
	if(hostport.length() == 0)
		throw std::range_error("Bad host/port specification '" + hostport + "'");
	if(hostport.at(0) == '[') {
		size_t end = hostport.find_first_of("]");
		if(end != std::string::npos)
			return hostport.substr(1, end - 1);
		throw std::range_error("Bad host/port specification '" + hostport + "'");
	}
	size_t end = hostport.find_first_of(":");
	if(end != std::string::npos)
		return hostport.substr(0, end);
	else
		return hostport;
}

std::string extract_port(const std::string& _hostport, bool tls_flag) throw(std::range_error, std::bad_alloc)
{
	std::string hostport = _hostport;
	if(hostport.length() == 0)
		throw std::range_error("Bad host/port specification '" + hostport + "'");
	if(hostport.at(0) == '[') {
		size_t end = hostport.find_first_of("]");
		if(end == hostport.length() - 1)
			throw std::range_error("Bad host/port specification '" + hostport + "'");
		else if(end == std::string::npos)
			if(tls_flag)
				return std::string("gits");
			else
				return std::string("git");
		else if(hostport.length() > end + 2 && hostport.at(end + 1) == ':')
			return hostport.substr(end + 2);
		else
			throw std::range_error("Bad host/port specification '" + hostport + "'");
	}
	size_t end = hostport.find_first_of(":");
	if(end == hostport.length() - 1)
		throw std::range_error("Bad host/port specification '" + hostport + "'");
	else if(end == std::string::npos)
		if(tls_flag)
			return "gits";
		else
			return "git";
	else if(hostport.length() > end + 1)
		return hostport.substr(end + 1);
	else
		throw std::range_error("Bad host/port specification '" + hostport + "'");
}
