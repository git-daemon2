#ifndef _ssh_keypair__hpp__included__
#define _ssh_keypair__hpp__included__

#include <string>

std::string handle_ssh_keypair(const std::string& data, const char* challenge, size_t size);

#endif
