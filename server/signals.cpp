/*
 * Copyright (C) Ilari Liusvaara 2009
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */
#include "signals.hpp"

volatile sig_atomic_t sighup_received = 0;
volatile sig_atomic_t sigterm_received = 0;
volatile sig_atomic_t sigchld_received = 0;

void sighup(int x)
{
	sighup_received = 1;
}

void sigterm(int x)
{
	sigterm_received = 1;
}

void sigchld(int x)
{
	sigchld_received = 1;
}

void initialize_signals()
{
	signal(SIGHUP, sighup);
	signal(SIGTERM, sigterm);
	signal(SIGCHLD, sigchld);
	signal(SIGPIPE, SIG_IGN);
	signal(SIGSEGV, SIG_DFL);
}
