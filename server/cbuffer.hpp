/*
 * Copyright (C) Ilari Liusvaara 2009
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */
#ifndef _cbuffer__hpp__included__
#define _cbuffer__hpp__included__

#include <cstdlib>
#include <map>
#include <stdexcept>
#include "os_exception.hpp"

//We don't know the internal representation of cbuffer struct so we need to forward-declare it.
struct cbuffer;

//Internal instance classs.
class circular_buffer_i;

//Circular buffer class. Most methods here just call the corresponding cbuffer_* function and
//turns error codes received into excpetions. The destination/only circular buffer in such
//operation will be circular buffer this instance is handle for.
class circular_buffer
{
public:
//Create new circular buffer with specified size. The newly created circular buffer lives until
//last reference to it is disposed.
//
//Inputs:
//	buffersize	Size of circular buffer.
//
//Exceptions:
//	std::bad_alloc	Not enough memory for operation.
	circular_buffer(size_t buffersize) throw(std::bad_alloc);

//Wrap existing circular buffer. The newly created handle is valid until underlying circular
//buffer is disposed. Wrapped buffer won't be freed when last handle goes away.
//
//Inputs:
//	cbuf		The underlying buffer.
//
//Exceptions:
//	std::bad_alloc	Not enough memory for operation.
	circular_buffer(cbuffer* cbuf) throw(std::bad_alloc);

//Dispose handle to circular buffer.
	~circular_buffer() throw();

//Call cbuffer_used().
	size_t used() throw();

//Call cbuffer_free().
	size_t free() throw();

//Call cbuffer_peek().
//
//Exceptions:
//	std::length_error
//			Not enough data in buffer.
	void peek(unsigned char* dest, size_t toread) throw(std::length_error);

//Call cbuffer_read().
//
//Exceptions:
//	std::length_error
//			Not enough data in buffer.
	void read(unsigned char* dest, size_t toread) throw(std::length_error);

//Call cbuffer_write().
//
//Exceptions:
//	std::length_error
//			Not enough free space in buffer.
	void write(unsigned char* src, size_t towrite) throw(std::length_error);

//Call cbuffer_move().
//
//Exceptions:
//	std::length_error
//			Move too large.
	void move(circular_buffer& to, size_t tomove) throw(std::length_error);

//Call cbuffer_read_fd().
//
//Exceptions:
//	os_exception	OS Error occured.
	size_t read_fd(int fd) throw(os_exception);

//Call cbuffer_write_fd().
//
//Exceptions:
//	os_exception	OS Error occured.
	size_t write_fd(int fd) throw(os_exception);

//Call cbuffer_fill_r_segment().
	std::pair<unsigned char*, size_t> fill_r_segment() throw();

//Call cbuffer_fill_w_segment().
	std::pair<unsigned char*, size_t> fill_w_segment() throw();

//Call cbuffer_commit_r_segment().
	void commit_r_segment(size_t size) throw(std::length_error);

//Call cbuffer_commit_w_segment().
	void commit_w_segment(size_t size) throw(std::length_error);

//Call cbuffer_clear().
	void clear() throw();

//Call cbuffer_read_max().
	size_t read_max(unsigned char* dest, size_t limit) throw();

//Call cbuffer_write_max().
	size_t write_max(unsigned char* src, size_t limit) throw();

//Call cbuffer_move_max().
	size_t move_max(circular_buffer& to, size_t limit) throw();

//Call cbuffer_move_nolimit().
	size_t move_nolimit(circular_buffer& to) throw();

//Create copy of handle.
	circular_buffer(const circular_buffer& cbuf);

//Assign handles.
	circular_buffer& operator=(const circular_buffer& cbuf);

//Signal that this circular buffer is going away and any wraps for
//it should be invalidated.
	static void disappearing(struct cbuffer* cbuf) throw();
private:
	//Internal representation of circular buffer.
	circular_buffer_i* imp;
};

#endif
