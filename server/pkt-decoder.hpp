/*
 * Copyright (C) Ilari Liusvaara 2009
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */
#ifndef _pkt_decoder__hpp__included__
#define _pkt_decoder__hpp__included__

#include <string>
#include "cbuffer.hpp"
#include <stdexcept>

class flush_exception : public std::runtime_error
{
public:
	flush_exception();
};

std::string decode_packet(circular_buffer& buffer) throw(std::bad_alloc, flush_exception, std::underflow_error,
	std::runtime_error);

#endif
