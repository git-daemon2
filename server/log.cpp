/*
 * Copyright (C) Ilari Liusvaara 2009
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */
#include "log.hpp"
#include <syslog.h>
#include <iostream>
#include <cerrno>
#include <unistd.h>
#include <sstream>

bool use_syslog = false;
bool be_verbose = false;
static bool syslog_open = false;

gd2_log_sink::gd2_log_sink(bool _notice) throw()
{
	notice = _notice;
}

static void splat_message(const std::string& msg, bool notice)
{
	//Only print notices in verbose mode.
	if(notice && !be_verbose)
		return;

again_with_syslog:
	if(use_syslog && !syslog_open) {
		openlog("git-daemon2", LOG_PID, LOG_DAEMON);
		syslog_open = true;
	}

	if(use_syslog)
		if(notice)
			syslog(LOG_NOTICE, "%s", msg.c_str());
		else
			syslog(LOG_ERR, "%s", msg.c_str());
	else {
		//Format the message.
		std::string msg2;
		std::ostringstream str;
		if(notice)
			str << "[PID " << getpid() << "] Notice: " << msg << std::endl;
		else
			str << "[PID " << getpid() << "] Error: " << msg << std::endl;
		msg2 = str.str();

		//Write it to stderr. If stderr gets closed, switch to syslog.
		size_t i = 0;
		while(i < msg2.length()) {
			ssize_t r = write(2, &msg2.at(i), msg2.length() - i);
			if(r < 0 && (errno == EPIPE || errno == EBADF)) {
				use_syslog = true;
				goto again_with_syslog;
			}
			if(r > 0)
				i += (size_t)r;
		}
	}
}

std::streamsize gd2_log_sink::write(const char* s, std::streamsize n)
{
	std::streamsize p = 0;
	const char* old_s = s;

	while(n > 0) {
		//Find first linefeed.
		while(n > 0 && *s != '\n') {
			n--;
			s++;
			p++;
		};
		//Copy until first linefeed (if any). If linefeed exists, splat the
		//message to terminal/syslog and clear the line buffer.
		linebuffer = linebuffer + std::string(old_s, s);
		old_s = s;
		if(n > 0) {
			splat_message(linebuffer, notice);
			linebuffer = std::string("");
			p++;
			n--;
			s++;
		}
	}
	return p;
}

//Our streams.
static gd2_log_sink notice_sink(true);
static gd2_log_sink error_sink(false);
boost::iostreams::stream<gd2_log_sink> gd2_notice(notice_sink, 0, 0);
boost::iostreams::stream<gd2_log_sink> gd2_error(error_sink, 0, 0);
