/*
 * Copyright (C) Ilari Liusvaara 2009-2010
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */
#include "misc.h"
#include "inet.hpp"
#include "log.hpp"
#include "variables.hpp"
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <cctype>
#include <netdb.h>
#include <string>
#include <errno.h>

namespace {
	uint32_t toipaddr(const std::string& spec) throw(std::range_error)
	{
		struct hostent* host;
		host = gethostbyname(spec.c_str());
		if(!host || !host->h_addr || host->h_addrtype != AF_INET)
			throw std::range_error("Bad listen address '" + spec + "'");
		return *(uint32_t*)host->h_addr;
	}

	int toprotocol(const std::string& spec) throw(std::range_error)
	{
		struct protoent* proto;
		proto = getprotobyname(spec.c_str());
		if(!proto)
			throw("Bad protocol name '" + spec + "'");
		return proto->p_proto;
	}
}

bool inetlistener::class_supported(const std::string& _spec) throw(std::runtime_error, std::bad_alloc)
{
	int i, af, proto;
	std::string spec = _spec;
	size_t slash = spec.find_first_of("/");
	std::string protocol = spec.substr(0, slash);
	std::string addrspace = spec.substr(slash + 1);

	if(addrspace == "ipv4")
		af = AF_INET;
	else if(addrspace == "ipv6")
		af = AF_INET6;
	else
		return false;

	try {
		proto = toprotocol(protocol);
	} catch(...) {
		return false;
	}

	i = socket(af, SOCK_STREAM, proto);
	if(i < 0)
		return false;
	close(i);

	return true;
}

inetlistener::inetlistener(const std::string& _spec, bool tls_flag)
	throw(std::bad_alloc, os_exception, std::range_error)
{
	std::string spec = _spec;
	std::string addr;
	struct addrinfo hints;
	struct addrinfo* naddr;

	size_t col = spec.find_first_of(":");
	if(col == std::string::npos)
		throw std::range_error("Bad address specification format");
	std::string clazz = spec.substr(0, col);
	spec = spec.substr(col + 1);

	size_t slash = clazz.find_first_of("/");
	std::string protocol = clazz.substr(0, slash);
	std::string addrspace = clazz.substr(slash + 1);

	if(addrspace == "ipv4") {
		af = AF_INET;
		afsize = sizeof(struct sockaddr_in);
	} else if(addrspace == "ipv6") {
		af = AF_INET6;
		afsize = sizeof(struct sockaddr_in6);
	} else
		throw std::range_error("Invalid address space '" + addrspace + "'");

	int proto = toprotocol(protocol);
	std::string host = extract_host(spec);
	std::string port = extract_port(spec, tls_flag);

	memset(&hints, 0, sizeof(struct addrinfo));
	hints.ai_flags = AI_PASSIVE;
	hints.ai_family = af;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = toprotocol(protocol);
	int r;
	if(host == "")
		r = getaddrinfo(NULL, port.c_str(), &hints, &naddr);
	else
		r = getaddrinfo(host.c_str(), port.c_str(), &hints, &naddr);
	if(r) {
		freeaddrinfo(naddr);
		throw std::range_error(std::string("getaddrinfo failed: ") + gai_strerror(r));
	}

	int sfd = socket(af, SOCK_STREAM, proto);
	if(sfd < 0)
		throw os_exception(errno);

	if(reuseaddr) {
		int yes = 1;
		if(setsockopt(sfd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int)) < 0)
			throw os_exception(errno);
	}

	if(bind(sfd, naddr->ai_addr, naddr->ai_addrlen) < 0)
		throw os_exception(errno);
	if(listen(sfd, 10) < 0)
		throw os_exception(errno);

	freeaddrinfo(naddr);

	tlsonly_flag(tls_flag);

	fd = sfd;
}

inetlistener::~inetlistener() throw()
{
	force_close(fd);
}

int inetlistener::get_fd() throw()
{
	return fd;
}

#define BUFFERSIZE 256
#define MAXAFSIZE 512

std::pair<int, std::string> inetlistener::accept() throw(std::bad_alloc, os_exception)
{
	int afd;
	char buffer[2 * BUFFERSIZE + 4];
	char hbuffer[BUFFERSIZE];
	char pbuffer[BUFFERSIZE];
	char saddr[MAXAFSIZE];
	socklen_t saddrlen = afsize;

	if(!getuid() || !geteuid())
		throw os_exception(EPERM);	//Fuck, no!

	afd = ::accept(fd, (struct sockaddr*)saddr, &saddrlen);
	if(afd < 0)
		throw os_exception(errno);

	int r = getnameinfo((struct sockaddr*)saddr, saddrlen,
		hbuffer, BUFFERSIZE, pbuffer, BUFFERSIZE,
		NI_NUMERICHOST | NI_NUMERICSERV);
	if(r)
		return std::make_pair(afd, std::string("<UNKNOWN>"));

	if(strchr(hbuffer, ':'))
		sprintf(buffer, "[%s]:%s", hbuffer, pbuffer);
	else
		sprintf(buffer, "%s:%s", hbuffer, pbuffer);

	return std::make_pair(afd, std::string(buffer));
}
