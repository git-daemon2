/*
 * Copyright (C) Ilari Liusvaara 2009
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void write_cert(int server_mode);

static void do_help()
{
	printf("gits-generate-hostkey: Host key generator.\n");
	printf("Command line options:\n");
	printf("--help\n");
	printf("\tThis help\n");
	printf("\n");
	printf("Note: Keep generated host key files private!\n");
	printf("Use gits-get-key-name to get public short\n");
	printf("representation of host key for user hostkey\n");
	printf("databases.\n");
	exit(0);
}

int main(int argc, char **argv)
{
	if (argc > 1 && !strcmp(argv[1], "--help"))
		do_help();
	write_cert(1);
	return 0;
}
