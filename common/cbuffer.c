/*
 * Copyright (C) Ilari Liusvaara 2009
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */
#include "cbuffer.h"
#include <stdlib.h>
#include <assert.h>
#include <unistd.h>
#ifdef USE_UNIX_SCATTER_GATHER_IO
#include <sys/uio.h>
#endif
#include <errno.h>
#include <string.h>

/*
 * NOTE: This code may not crash, call exit or anything similar unless
 * program state is corrupt or call parameters are completely invalid.
 * It also may not call Git APIs.
 */

struct cbuffer
{
	/* Base address for data area. */
	unsigned char *cb_base;
	/* Amount of free space. */
	size_t cb_free;
	/* Amount of used space. */
	size_t cb_used;
	/* Get pointer (relative to base) */
	size_t cb_get;
	/* Put pointer (relative to base) */
	size_t cb_put;
	/* Total size */
	size_t cb_size;
};

/* Assert that invariants of circular buffer hold. */
static void assert_invariants(struct cbuffer *cbuf)
{
	assert(cbuf->cb_free >= 0 && cbuf->cb_free <= cbuf->cb_size);
	assert(cbuf->cb_used >= 0 && cbuf->cb_used <= cbuf->cb_size);
	assert(cbuf->cb_free + cbuf->cb_used == cbuf->cb_size);
	assert(cbuf->cb_get >= 0 && cbuf->cb_get <= cbuf->cb_size);
	assert(cbuf->cb_put >= 0 && cbuf->cb_put <= cbuf->cb_size);
	if (cbuf->cb_get == cbuf->cb_put)
		assert(cbuf->cb_free == 0 || cbuf->cb_used == 0);
	else if (cbuf->cb_get < cbuf->cb_put)
		assert(cbuf->cb_get + cbuf->cb_used == cbuf->cb_put);
	else
		assert(cbuf->cb_put + cbuf->cb_free == cbuf->cb_get);
}

/* Ack specified amount of data written. */
static void ack_write(struct cbuffer *cbuf, size_t wsize)
{
	assert_invariants(cbuf);
	assert(wsize <= cbuf->cb_free);
	/*
	 * Writing data decreses free space, increases used space,
	 * increases put pointer, but it will wrap around if it
	 * goes past end of buffer.
	 */
	cbuf->cb_free -= wsize;
	cbuf->cb_used += wsize;
	cbuf->cb_put += wsize;
	if (cbuf->cb_put >= cbuf->cb_size)
		cbuf->cb_put -= cbuf->cb_size;
	assert_invariants(cbuf);
}

/* Ack specified amount of data read (removing it). */
static void ack_read(struct cbuffer *cbuf, size_t rsize)
{
	assert_invariants(cbuf);
	assert(rsize <= cbuf->cb_used);
	/*
	 * Reading data decreses used space, increases free space,
	 * increases get pointer, but it will wrap around if it
	 * goes past end of buffer.
	 */
	cbuf->cb_free += rsize;
	cbuf->cb_used -= rsize;
	cbuf->cb_get += rsize;
	if (cbuf->cb_get >= cbuf->cb_size)
		cbuf->cb_get -= cbuf->cb_size;
	assert_invariants(cbuf);
}

struct cbuffer *cbuffer_create(unsigned char *data, size_t datasize)
{
	struct cbuffer *cbuf = (struct cbuffer*)malloc(sizeof(
		struct cbuffer));
	if (cbuf) {
		cbuf->cb_base = data;
		cbuf->cb_size = cbuf->cb_free = datasize;
		cbuf->cb_used = cbuf->cb_get = cbuf->cb_put = 0;
		assert_invariants(cbuf);
	}
	return cbuf;
}

void cbuffer_destroy(struct cbuffer *cbuf)
{
	if (cbuf) {
		assert_invariants(cbuf);
		free(cbuf);
	}
}

size_t cbuffer_used(struct cbuffer *cbuf)
{
	assert_invariants(cbuf);
	return cbuf->cb_used;
}

size_t cbuffer_free(struct cbuffer *cbuf)
{
	assert_invariants(cbuf);
	return cbuf->cb_free;
}

int cbuffer_read(struct cbuffer *cbuf, unsigned char *dest, size_t toread)
{
	assert_invariants(cbuf);

	/* Check that user doesn't try to read too much. */
	if (toread > cbuf->cb_used)
		return -1;

	size_t tocopy = toread;

	/*
	 * Limit the amount of data read to amount fits inside single
	 * segment. If get pointer is not greater or equal to put pointer,
	 * then entiere used space is only single segment.
	 */
	if (cbuf->cb_get >= cbuf->cb_put)
		if (tocopy > cbuf->cb_size - cbuf->cb_get)
			tocopy = cbuf->cb_size - cbuf->cb_get;

	if (tocopy > 0) {
		/* Copy the segment and mark it read. */
		memcpy(dest, cbuf->cb_base + cbuf->cb_get, tocopy);
		ack_read(cbuf, tocopy);
		/* Adjust the request for subsequent segments. */
		toread -= tocopy;
		dest += tocopy;
	}

	/*
	 * If the read was incomplete, repeat the read request for the
	 * non-read tail.
	 */
	if (toread > 0)
		return cbuffer_read(cbuf, dest, toread);

	assert_invariants(cbuf);

	return 0;
}

int cbuffer_peek(struct cbuffer *cbuf, unsigned char *dest, size_t toread)
{
	assert_invariants(cbuf);

	/* Check that user doesn't try to peek too much. */
	if (toread > cbuf->cb_used)
		return -1;

	/* Is the used space as single segment or in two segments? */
	if (cbuf->cb_get + toread > cbuf->cb_size) {
		/* Two. We have to compute where segment boundary is and
		   copy the data as two copies. */
		size_t firstseg = cbuf->cb_size - cbuf->cb_get;
		memcpy(dest, cbuf->cb_base + cbuf->cb_get, firstseg);
		memcpy(dest + firstseg, cbuf->cb_base, toread - firstseg);
	} else {
		/* One, data can be read as single copy. */
		memcpy(dest, cbuf->cb_base + cbuf->cb_get, toread);
	}

	assert_invariants(cbuf);

	return 0;
}

int cbuffer_write(struct cbuffer *cbuf, const unsigned char *src,
	size_t towrite)
{
	assert_invariants(cbuf);

	/* Check that user doesn't try to write too much. */
	if (towrite > cbuf->cb_free)
		return -1;

	size_t tocopy = towrite;

	/*
	 * Limit the amount of data written to amount fits inside single
	 * segment. If put pointer is not greater or equal to get pointer,
	 * then entiere free space is only single segment.
	 */
	if (cbuf->cb_put >= cbuf->cb_get)
		if (tocopy > cbuf->cb_size - cbuf->cb_put)
			tocopy = cbuf->cb_size - cbuf->cb_put;

	if (tocopy > 0) {
		/* Copy the segment and mark it written. */
		memcpy(cbuf->cb_base + cbuf->cb_put, src, tocopy);
		ack_write(cbuf, tocopy);
		/* Adjust the request for subsequent segments. */
		towrite -= tocopy;
		src += tocopy;
	}

	/*
	 * If the write was incomplete, repeat the write request for the
	 * non-written tail.
	 */
	if (towrite > 0)
		return cbuffer_write(cbuf, src, towrite);

	assert_invariants(cbuf);

	return 0;
}

int cbuffer_move(struct cbuffer *dest, struct cbuffer *src, size_t tomove)
{
	assert_invariants(dest);
	assert_invariants(src);

	/* Check that amount to move isn't too great. */
	if (tomove > dest->cb_free)
		return -1;
	if (tomove > src->cb_used)
		return -1;

	size_t tocopy = tomove;
	/*
	 * Compute maximum number of bytes that is less than amount to
	 * move and amount of used/free space in current segments in
	 * both buffers.
	 */
	if (dest->cb_put >= dest->cb_get)
		if (tocopy > dest->cb_size - dest->cb_put)
			tocopy = dest->cb_size - dest->cb_put;
	if (src->cb_get >= src->cb_put)
		if (tocopy > src->cb_size - src->cb_get)
			tocopy = src->cb_size - src->cb_get;

	if (tocopy > 0) {
		/* Move the segment. and mark it moved. */
		memcpy(dest->cb_base + dest->cb_put,
			src->cb_base +src->cb_get, tocopy);
		ack_read(src, tocopy);
		ack_write(dest, tocopy);
		/* Adjust request for subsequent segments. */
		tomove -= tocopy;
	}

	/* If request was incomplete, move the yet unmoved tail. */
	if (tomove > 0)
		return cbuffer_move(dest, src, tomove);

	assert_invariants(dest);
	assert_invariants(src);

	return 0;
}

ssize_t cbuffer_read_fd(struct cbuffer *cbuf, int fd)
{
	ssize_t r;

	assert_invariants(cbuf);

	/* Generate EAGAIN if needed (on no free space). */
	if (cbuf->cb_free == 0) {
		errno = EAGAIN;
		return -1;
	}

	/*
	 * If scatter-gather I/O is available, entiere buffer may be read at
	 * once. Otherwise only single segment can be read at time.
	 */
#ifdef USE_UNIX_SCATTER_GATHER_IO
	struct iovec areas[2];
	int touse;

	/* One or two segments? */
	if (cbuf->cb_put >= cbuf->cb_get) {
		/* Two. */
		areas[0].iov_base = cbuf->cb_base + cbuf->cb_put;
		areas[0].iov_len = cbuf->cb_size - cbuf->cb_put;
		areas[1].iov_base = cbuf->cb_base;
		areas[1].iov_len = cbuf->cb_get;
		touse = 2;
	} else {
		/* One. */
		areas[0].iov_base = cbuf->cb_base + cbuf->cb_put;
		areas[0].iov_len = cbuf->cb_get - cbuf->cb_put;
		touse = 1;
	}
	r = readv(fd, areas, touse);
#else
	/* Read into current segment. */
	if (cbuf->cb_put >= cbuf->cb_get)
		r = read(fd, cbuf->cb_base + cbuf->cb_put,
			cbuf->cb_size - cbuf->cb_put);
	else
		r = read(fd, cbuf->cb_base + cbuf->cb_put,
			cbuf->cb_get - cbuf->cb_put);
#endif
	/* Ack any successfully read data as written. */
	if (r > 0)
		ack_write(cbuf, (size_t)r);

	assert_invariants(cbuf);

	return r;
}

ssize_t cbuffer_write_fd(struct cbuffer *cbuf, int fd)
{
	ssize_t r;

	assert_invariants(cbuf);

	/* Generate EAGAIN if needed (on no used space). */
	if (cbuf->cb_used == 0) {
		errno = EAGAIN;
		return -1;
	}

	/*
	 * If scatter-gather I/O is available, entiere buffer may be written
	 * at once. Otherwise only single segment can be written at time.
	 */
#ifdef USE_UNIX_SCATTER_GATHER_IO
	struct iovec areas[2];
	int touse;

	/* One or two segments? */
	if (cbuf->cb_get >= cbuf->cb_put) {
		/* Two. */
		areas[0].iov_base = cbuf->cb_base + cbuf->cb_get;
		areas[0].iov_len = cbuf->cb_size - cbuf->cb_get;
		areas[1].iov_base = cbuf->cb_base;
		areas[1].iov_len = cbuf->cb_put;
		touse = 2;
	} else {
		/* One. */
		areas[0].iov_base = cbuf->cb_base + cbuf->cb_get;
		areas[0].iov_len = cbuf->cb_put - cbuf->cb_get;
		touse = 1;
	}
	r = writev(fd, areas, touse);
#else
	/* Write current segment. */
	if (cbuf->cb_get >= cbuf->cb_put)
		r = write(fd, cbuf->cb_base + cbuf->cb_get,
			cbuf->cb_size - cbuf->cb_get);
	else
		r = write(fd, cbuf->cb_base + cbuf->cb_get,
			cbuf->cb_put - cbuf->cb_get);
#endif
	/* Ack any successfully written data as read. */
	if (r > 0)
		ack_read(cbuf, (size_t)r);

	assert_invariants(cbuf);

	return r;
}

void cbuffer_fill_r_segment(struct cbuffer *cbuf, unsigned char **base,
	size_t *length)
{
	assert_invariants(cbuf);

	/* Compute segment base. */
	*base = cbuf->cb_base + cbuf->cb_get;

	if (!cbuf->cb_used) {
		/* No used space -> empty segment. */
		*length = 0;
	} else if (cbuf->cb_get >= cbuf->cb_put) {
		/* High segment is current. */
		*length = cbuf->cb_size - cbuf->cb_get;
	} else {
		/* Low segment is current. */
		*length = cbuf->cb_put - cbuf->cb_get;
	}

	assert_invariants(cbuf);
}

void cbuffer_commit_r_segment(struct cbuffer *cbuf, size_t length)
{
	assert_invariants(cbuf);
	/*
	 * This doesn't handle read being longer than single segment right,
	 * but that's undefined anyway.
	 */
	ack_read(cbuf, length);
	assert_invariants(cbuf);
}

void cbuffer_fill_w_segment(struct cbuffer *cbuf, unsigned char **base,
	size_t *length)
{
	assert_invariants(cbuf);

	/* Compute segment base. */
	*base = cbuf->cb_base + cbuf->cb_put;

	if (!cbuf->cb_free) {
		/* No free space -> empty segment. */
		*length = 0;
	} else if (cbuf->cb_put >= cbuf->cb_get) {
		/* High segment is current. */
		*length = cbuf->cb_size - cbuf->cb_put;
	} else {
		/* Low segment is current. */
		*length = cbuf->cb_get - cbuf->cb_put;
	}

	assert_invariants(cbuf);
}

void cbuffer_commit_w_segment(struct cbuffer *cbuf, size_t length)
{
	assert_invariants(cbuf);
	/*
	 * This doesn't handle write being longer than single segment right,
	 * but that's undefined anyway.
	 */
	ack_write(cbuf, length);
	assert_invariants(cbuf);
}


void cbuffer_clear(struct cbuffer *cbuf)
{
	/*
	 * Just resetting pointers and values to initial defaults clears
	 * all data.
	 */
	cbuf->cb_used = cbuf->cb_put = cbuf->cb_get = 0;
	cbuf->cb_free = cbuf->cb_size;
	assert_invariants(cbuf);
}

size_t cbuffer_read_max(struct cbuffer *cbuf, unsigned char *dest,
	size_t limit)
{
	/* Limit the request to maximum possible and do read request. */
	if (limit > cbuffer_used(cbuf))
		limit = cbuffer_used(cbuf);
	cbuffer_read(cbuf, dest, limit);
	return limit;
}

size_t cbuffer_write_max(struct cbuffer *cbuf, const unsigned char *src,
	size_t limit)
{
	/* Limit the request to maximum possible and do write request. */
	if (limit > cbuffer_free(cbuf))
		limit = cbuffer_free(cbuf);
	cbuffer_write(cbuf, src, limit);
	return limit;
}

size_t cbuffer_move_max(struct cbuffer *dest, struct cbuffer *src,
	size_t limit)
{
	/* Limit the request to maximum possible and do move request. */
	if (limit > cbuffer_free(dest))
		limit = cbuffer_free(dest);
	if (limit > cbuffer_used(src))
		limit = cbuffer_used(src);
	cbuffer_move(dest, src, limit);
	return limit;
}

size_t cbuffer_move_nolimit(struct cbuffer *dest, struct cbuffer *src)
{
	size_t limit;
	/*
	 * Limit the request to maximum possible and do move request.
	 * The move lacks limit so use free space in destination as
	 * first limit.
	 */
	limit = cbuffer_free(dest);
	if (limit > cbuffer_used(src))
		limit = cbuffer_used(src);
	cbuffer_move(dest, src, limit);
	return limit;
}
