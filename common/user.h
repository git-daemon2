/*
 * Copyright (C) Ilari Liusvaara 2009
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */
#ifndef _user__h__included__
#define _user__h__included__

#include <gnutls/gnutls.h>
#include "cbuffer.h"
#include <stdint.h>
#include <stdio.h>
#include <sys/time.h>

#ifdef __cplusplus
extern "C" {
#endif

/*
 * Terminoloyy:
 *	black:
 *		Side of user session connected to socket. Transports
 *		unencrypted or TLS data between peers.
 *	red:
 *		Side of user session connected to server loop or to
 *		helper program.
 *	red input:
 *		Input buffer unencrypted data is read from. May be connected
 *		to file descriptor. In that case that file descriptor is
 *		read for data.
 *	red output:
 *		Output buffer decrypted data is written to. May be connected
 *		to file descriptor. In that cse that file descriptor is
 *		written with the data.
 *	red error:
 *		Input buffer error input is read from. May be connecte to
 *		file descriptor, which is read for input data. If red input
 *		file descriptor has data succesfully read, the red error buffer
 *		is cleared and any further error input is redirected to bit
 *		bucket. If red input closes with red error having data, then
 *		that data is sent as ERR packet when red error has associated
 *		file descriptor closed.
 *	red_in:
 *		File descriptor associated with red input.
 *	red_out:
 *		File descriptor associated with red output.
 *	red_err:
 *		File descriptor associated with red error.
 *	deadline:
 *		Time to disconnect (some) client on or perform some other
 *		service.
 */

/* Main session structure. Opaque type. */
struct user;

/* Failure codes. */
/* User still active. */
#define USER_STILL_ACTIVE	0
/* Connection normal end. */
#define USER_CONNECTION_END	1
/* Transport error. */
#define USER_LAYER4_ERROR	-1
/* TLS error while not handshaking. */
#define USER_TLS_ERROR		-2
/* TLS handshake error. */
#define USER_TLS_HAND_ERROR	-3
/* User killed. */
#define USER_KILL		-4
/* Red file descriptor I/O operation failure. */
#define USER_RED_FAILURE	-5
/* User timed out. */
#define USER_TIMEOUT		-6

/*
 * Create new user session.
 *
 * Input:
 *	black_fd	Black fd. Must be socket.
 *	timeout_secs	Initial timeout in seconds.
 *
 * Output:
 *	Return value	Newly created session, or NULL on out of
 *			memory.
 */
struct user *user_create(int black_fd, unsigned timeout_secs);

/*
 * Configure session to use TLS. The TLS session must be preconfigured
 * (credentials set, etc), but not handshaked.
 *
 * Input:
 *	user		The user session to manipulate.
 *	session		TLS session to assign.
 */
void user_configure_tls(struct user *user, gnutls_session_t session);

/*
 * Add current user session file descriptors that are ready to read or
 * write to file descriptor sets for read or write. Also update dead-
 * line if needed.
 *
 * Input:
 *	user		The user session to handle.
 *	bound		Current file descriptor bound. 0 if there are
 *			no file descriptors in either set.
 *	rfds		Read file descriptor set.
 *	wfds		Write file descriptor set.
 *	deadline	Current deadline for select.
 *
 * Output:
 *	bound		Updated file descriptor bound.
 *	rfds		Updated read file descriptor set.
 *	wfds		Updated write file descriptor set.
 *	deadline	Updated deadline for select.
 */
void user_add_to_sets(struct user *user, int *bound, fd_set *rfds,
	fd_set *wfds, struct timeval *deadline);

/*
 * Service this user.
 *
 * Input:
 *	user		The user session to handle.
 *	rfds		Ready to read file descriptors.
 *	wfds		Ready to write file descriptors.
 */
void user_service(struct user *user, fd_set *rfds, fd_set *wfds);

/*
 * Service this user without doing any I/O
 *
 * Input:
 *	user		The user session to handle.
 */
void user_service_nofd(struct user *user);

/*
 * Get failure class.
 *
 * Input:
 *	user		The user session to interrogate.
 *
 * Output:
 *	Return value	0 if connection is active, 1 if end
 *			of connection, negative code if connection
 *			ended with error. See USER_* defintions.
 */
int user_get_failure(struct user *user);

/*
 * Get explanation of failure code.
 *
 * Input:
 *	code		The failure code.
 *
 * Output:
 *	Return value	String explaining the code. Do not
 *			free this.
 */
const char *user_explain_failure(int code);

/*
 * Get more detailed error message to explain the failure.
 *
 * Input:
 *	user		The user session.
 *
 * Output:
 *	Return value	Error message or NULL if there is
 *			no more detailed error message. Do
 *			not free this.
 *
 * Notes:
 *	- Error message can be NULL or not independently of
 *	  main failure status.
 */
const char *user_get_error(struct user *user);

/*
 * Has TLS been configured?
 *
 * Input:
 *	user		The user session to interrogate.
 *
 * Output:
 *	Return value	Nonzero if configured, zero if not.
 */
/* Returns 1 if TLS has been configured, 0 otherwise. */
int user_tls_configured(struct user *user);

/*
 * Return TLS session associated with user session.
 *
 * Input:
 *	user		The user session to interrogate.
 *
 * Output:
 *	Return value	If TLS is configured and has handshaked, the TLS
 *			session. Otherwise NULL.
 */
gnutls_session_t user_get_tls(struct user *user);

/*
 * Free user structure. If user is still active, disconnect user hard.
 *
 * Input:
 *	user		The user session to release.
 */
void user_release(struct user *user);

/*
 * Set red I/O file descriptors.
 *
 * Input:
 *	user		The user session to manipulate.
 *	red_in		Red input file descriptor, -1 for none.
 *	red_out		Red output file descritpor, -1 for none.
 *	red_err		Red error file descriptor, -1 for none.
 *
 * Notes:
 *	- red_in and red_err must be read ends if present.
 *	- red_out must be write end if present.
 */
void user_set_red_io(struct user *user, int red_in, int red_out, int red_err);

/*
 * Clear red I/O by closing red output and marking no red output file
 * descriptor. This may be neeeded, since red out can't close without
 * input to transfer.
 *
 * Input:
 *	user		The user session to manipulate.
 */
void user_clear_red_io(struct user *user);

/*
 * Get red input buffer.
 *
 * Input:
 *	user		The user session to interrogate.
 *
 * Output:
 *	Return value	If red_in is set to none, the buffer, otherwise
 *			NULL.
 */
struct cbuffer *user_get_red_in(struct user *user);

/*
 * Get red output buffer.
 *
 * Input:
 *	user		The user session to interrogate.
 *
 * Output:
 *	Return value	If red_out is set to none, the buffer, otherwise
 *			NULL.
 */
struct cbuffer *user_get_red_out(struct user *user);

/*
 * Get red error buffer.
 *
 * Input:
 *	user		The user session to interrogate.
 *
 * Output:
 *	Return value	If red_err is set to none and no data has been
 *			received through red_in, the buffer, otherwise
 *			NULL.
 */
struct cbuffer *user_get_red_err(struct user *user);

/*
 * Clear deadline (don't generate timeout anymore).
 *
 * Input:
 *	user		The user session to manipulate.
 */
void user_clear_deadline(struct user *user);

/*
 * Send EOF to red input.
 *
 * Input:
 *	user		The user session to manipulate.
 *
 * Notes:
 *	- Only works if red input has no file descriptor associated.
 *	- EOF is automatically sent if red_in encounters EOF.
 */
void user_send_red_in_eof(struct user *user);

/*
 * Force get red input buffer (even if it shouldn't be available).
 *
 * Input:
 *	user		The user session to interrogate.
 *
 * Output:
 *	Return value	Red input buffer.
 */
struct cbuffer *user_get_red_in_force(struct user *user);

/*
 * Force get red output buffer (even if it shouldn't be available).
 *
 * Input:
 *	user		The user session to interrogate.
 *
 * Output:
 *	Return value	Red output buffer.
 */
struct cbuffer *user_get_red_out_force(struct user *user);

/*
 * Force get red error buffer (even if it shouldn't be available).
 *
 * Input:
 *	user		The user session to interrogate.
 *
 * Output:
 *	Return value	Red error buffer.
 */
struct cbuffer *user_get_red_err_force(struct user *user);

/*
 * Send fatal TLS alert.
 *
 * Input:
 *	user		The user session to manipulate.
 *	alert		The alert to send.
 *
 * Notes:
 *	- Ignored if no TLS has been configured.
 */
void user_tls_send_alert(struct user *user, gnutls_alert_description_t alert);

/*
 * Has red output been EOF'd?
 *
 * Input:
 *	user		The user session to interrogate.
 *
 * Output:
 *	Return value	Nonzero if red out can receive no more data and
 *			not connected to file descriptor, otherwise zero
 *			(more data possible).
 */
int user_red_out_eofd(struct user *user);

/*
 * Print debugging output.
 *
 * Input:
 *	user		The user session to interrogate.
 *	filp		Where to write the output.
 */
void user_debug(struct user *user, FILE *filp);

#ifdef __cplusplus
}
#endif

#endif
