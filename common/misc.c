/*
 * Copyright (C) Ilari Liusvaara 2009
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */
#include "misc.h"
#include <unistd.h>
#include <errno.h>

void force_close(int fd)
{
	while (close(fd) < 0 && errno != EBADF);
}
