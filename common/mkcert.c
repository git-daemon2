/*
 * Copyright (C) Ilari Liusvaara 2009-2010
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */
#include "cbuffer.h"
#include "home.h"
#include "prompt.h"
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include <string.h>
#include <errno.h>
#include <signal.h>
#include <sys/stat.h>
#include "git-compat-util.h"
#ifndef BUILD_SELFSTANDING
#include "run-command.h"
#endif

#define CERT_MAX 65536
#define BUFSIZE 8192

static int seal_cert(struct cbuffer *sealed, struct cbuffer *unsealed,
	const char *sealer)
{
	struct child_process child;
	char **argv;
	char *sealer_copy;
	int splits = 0;
	int escape = 0;
	int ridx, widx, tidx;
	int cleanup = 0;
	const char *i;

	signal(SIGPIPE, SIG_IGN);

	for (i = sealer; *i; i++) {
		if (escape)
			escape = 0;
		else if (*i == '\\')
			escape = 1;
		else if (*i == ' ')
			splits++;
	}

	argv = xmalloc((splits + 2) * sizeof(char*));
	argv[splits + 1] = NULL;

	sealer_copy = xstrdup(sealer);
	argv[0] = sealer_copy;

	ridx = 0;
	widx = 0;
	tidx = 1;
	escape = 0;
	while (sealer_copy[ridx]) {
		if (escape) {
			escape = 0;
			sealer_copy[widx++] = sealer_copy[ridx++];
		} else if (sealer_copy[ridx] == '\\') {
			ridx++;
			escape = 1;
		} else if (sealer_copy[ridx] == ' ') {
			sealer_copy[widx++] = '\0';
			argv[tidx++] = sealer_copy + widx;
			ridx++;
		} else
			sealer_copy[widx++] = sealer_copy[ridx++];
	}
	sealer_copy[widx] = '\0';

	memset(&child, 0, sizeof(child));
	child.argv = (const char**)argv;
	child.in = -1;
	child.out = -1;
	child.err = 0;
	if (start_command(&child))
		return -1;
	cleanup = 1;

	while (1) {
		int bound;
		fd_set rf;
		fd_set wf;
		int r;

		FD_ZERO(&rf);
		FD_ZERO(&wf);
		FD_SET(child.out, &rf);
		if (cbuffer_used(unsealed))
			FD_SET(child.in, &wf);
		else
			close(child.in);

		if (cbuffer_used(sealed))
			bound = ((child.out > child.in) ? child.out :
				child.in) + 1;
		else
			bound = child.out + 1;

		r = select(bound, &rf, &wf, NULL, NULL);
		if (r < 0 && r != EINTR) {
			perror("Select");
			goto exit_error;
		}
		if (r < 0) {
			FD_ZERO(&rf);
			FD_ZERO(&wf);
			perror("select");
		}

		if (FD_ISSET(child.out, &rf)) {
			r = cbuffer_read_fd(sealed, child.out);
			if (r < 0 && errno != EINTR && errno != EAGAIN) {
				fprintf(stderr, "Read from sealer "
					"failed: %s", strerror(errno));
				goto exit_error;
			}
			if (r < 0 && errno == EAGAIN)
				if (!cbuffer_free(sealed)) {
					fprintf(stderr, "Keypair too big\n");
					goto exit_error;
			}
			if (r < 0)
				perror("read");
			if (r == 0)
				break;
			}

			if (FD_ISSET(child.in, &wf)) {
			r = cbuffer_write_fd(unsealed, child.in);
			if (r < 0 && errno == EPIPE) {
				fprintf(stderr, "Sealer exited "
					"unexpectedly\n");
				goto exit_error;
			}
			if (r < 0 && errno != EINTR && errno != EAGAIN) {
				fprintf(stderr, "Write to sealer "
					"failed: %s", strerror(errno));
				goto exit_error;
			}
			if (r < 0)
				perror("write");
		}
	}

	if (finish_command(&child)) {
		cleanup = 0;
		goto exit_error;
	}

	close(child.in);
	close(child.out);

	return 0;
exit_error:
	close(child.in);
	close(child.out);
	return -1;
}

static void append_member(struct cbuffer *cbuf, const char *filename)
{
	unsigned char backing[CERT_MAX];
	struct cbuffer *content;
	int fd;
	size_t size = 0;
	unsigned char buf[2];

	content = cbuffer_create(backing, CERT_MAX);
	fd = open(filename, O_RDONLY);
	if (fd < 0) {
		perror("open");
		exit(1);
	}
	while (1) {
		ssize_t r = cbuffer_read_fd(content, fd);
		if (r < 0) {
			if (errno == EAGAIN) {
				if (!cbuffer_free(content)) {
					fprintf(stderr, "Member too big.\n");
					unlink("key.private.tmp");
					unlink("key.public.tmp");
					exit(1);
				}
			} else if (errno != EINTR) {
				perror("read");
				exit(1);
			}
		} else if (r == 0) {
			break;
		} else
			size += r;
	}
	close(fd);

	buf[0] = (unsigned char)((size >> 8) & 0xFF);
	buf[1] = (unsigned char)((size) & 0xFF);
	if (cbuffer_write(cbuf, buf, 2) < 0) {
		fprintf(stderr, "Certificate too big (can't write member header).\n");
		unlink("key.private.tmp");
		unlink("key.public.tmp");
		exit(1);
	}
	if (cbuffer_move(cbuf, content, size) < 0) {
		fprintf(stderr, "Certificate too big (can't write member of %lu "
			"bytes).\n", (unsigned long)size);
		unlink("key.private.tmp");
		unlink("key.public.tmp");
		exit(1);
	}

	cbuffer_destroy(content);
}


static char *escape(char *s)
{
	char *ans;
	int ridx = 0, widx = 0;

	ans = xmalloc(2 * strlen(s) + 1);
	while (s[ridx]) {
		if (s[ridx] == '\\') {
			ans[widx++] = '\\';
			ans[widx++] = '\\';
			ridx++;
		} else if (s[ridx] == ' ') {
			ans[widx++] = '\\';
			ans[widx++] = ' ';
			ridx++;
		} else {
			ans[widx++] = s[ridx++];
		}
	}
	ans[widx] = '\0';
	free(s);
	return ans;
}

static int check_name(char *s)
{
	size_t x;
	x = strspn(s, " 0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"
		"abcdefghijklmmnopqrstuvwxyz!#$%&'*+-/=?^_`{|}~");
	if (!x || s[x]) {
		return -1;
	}
	return 0;
}

static int check_comment(char *s)
{
	char *at;
	at = strchr(s, '(');
	if (at)
		return -1;
	at = strchr(s, '\\');
	if (at)
		return -1;
	at = strchr(s, ')');
	if (at)
		return -1;
	at = s;
	while (*at >= 32 && *at <= 126)
		at++;
	if (*at)
		return -1;
	return 0;
}

static int check_email(char *s)
{
	size_t x;
	char *at;
	at = strchr(s, '@');
	if (!at)
		return -1;
	x = strspn(s, ".0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"
		"abcdefghijklmmnopqrstuvwxyz!#$%&'*+-/=?^_`{|}~");
	if (s[x] != '@')
		return -1;
	if (!at[1])
		return -1;
	x = strspn(at + 1, ".0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"
		"abcdefghijklmmnopqrstuvwxyz!#$%&'*+-/=?^_`{|}~");
	if (at[x + 1])
		return -1;
	return 0;
}

#define BUFSIZE 8192

void write_cert(int server_mode)
{
	unsigned char backing1[CERT_MAX];
	unsigned char backing2[CERT_MAX];
	unsigned char keytemp[CERT_MAX];
	struct cbuffer *unsealed;
	struct cbuffer *sealed;
	unsigned char *buf = (unsigned char*)"GITSUCERT";
	unsigned char *buf2 = (unsigned char*)"GITSSCERT\x00\x03gpg";
	char *name;
	char fbuffer[BUFSIZE];
	int fd;
	int do_seal = 0;
	int keylen = 1024;
	char *realname;
	char *comment;
	char *email;
	FILE *script;

	if (!ensure_directory("$HOME") < 0)
		die("Hey, you don't have a home directory!");

	unsealed = cbuffer_create(backing1, CERT_MAX);
	sealed = cbuffer_create(backing2, CERT_MAX);

reask_length:
	printf("1) 1024 bit key\n");
	printf("2) 2048 bit key\n");
	printf("3) 3072 bit key\n");
	name = prompt_string("Pick key length", 0);
	if (!strcmp(name, "1"))
		keylen = 1024;
	else if (!strcmp(name, "2"))
		keylen = 2048;
	else if (!strcmp(name, "3"))
		keylen = 3072;
	else {
		fprintf(stderr, "Bad choice\n");
		goto reask_length;
	}

ask_name:
	realname = prompt_string("Enter name to put into key", 0);
	if (check_name(realname) < 0) {
		fprintf(stderr, "Bad name\n");
		goto ask_name;
	}
ask_comment:
	comment = prompt_string("Enter comment to put into key", 0);
	if (check_comment(comment) < 0) {
		fprintf(stderr, "Bad comment\n");
		goto ask_comment;
	}
ask_email:
	email = prompt_string("Enter E-mail address to put into key", 0);
	if (check_email(email) < 0) {
		fprintf(stderr, "Bad E-mail address\n");
		goto ask_email;
	}

	script = fopen("key.script.tmp", "w");
	if (!script)
		die("Can't create key script");
	fprintf(script, "Key-Type: DSA\n");
	fprintf(script, "Key-Length: %i\n", keylen);
	fprintf(script, "Name-Real: %s\n", realname);
	if (*comment)
		fprintf(script, "Name-Comment: %s\n", comment);
	fprintf(script, "Name-Email: %s\n", email);
	fprintf(script, "Expire-Date: 0\n");
	fprintf(script, "%%pubring key.public.tmp\n");
	fprintf(script, "%%secring key.private.tmp\n");
	fprintf(script, "%%commit\n");
	fprintf(script, "%%echo done\n");
	fclose(script);

	if (system("gpg --batch --gen-key key.script.tmp")) {
		unlink("key.private.tmp");
		unlink("key.public.tmp");
		unlink("key.script.tmp");
		die("Can't generate key");
	}
	unlink("key.script.tmp");

	append_member(unsealed, "key.private.tmp");
	unlink("key.private.tmp");
	append_member(unsealed, "key.public.tmp");
	unlink("key.public.tmp");

reask_seal:
	if (server_mode)
		goto no_seal;
	printf("1) Don't seal key\n");
	printf("2) Seal using password (gpg)\n");
	printf("3) Seal using keypair (gpg)\n");
	name = prompt_string("Pick sealing method", 0);
	if (!strcmp(name, "1"))
		do_seal = 0;
	else if (!strcmp(name, "2"))
		do_seal = 1;
	else if (!strcmp(name, "3"))
		do_seal = 2;
	else {
		fprintf(stderr, "Bad choice");
		goto reask_seal;
	}
no_seal:
	keylen = cbuffer_read_max(unsealed, keytemp, CERT_MAX);
	cbuffer_write(unsealed, keytemp, keylen);

	if (do_seal == 1) {
		cbuffer_write(sealed, (unsigned char*)buf2, 14);
		if (seal_cert(sealed, unsealed, "gpg --symmetric "
			"--force-mdc") < 0) {
			cbuffer_clear(sealed);
			cbuffer_clear(unsealed);
			cbuffer_write(unsealed, keytemp, keylen);
			fprintf(stderr, "Sealing failed.\n");
			goto reask_seal;
		}
	} else if (do_seal == 2) {
		char *hint;
		cbuffer_write(sealed, (unsigned char*)buf2, 14);

		hint = prompt_string("Seal using whose key", 0);
		hint = escape(hint);
		sprintf(fbuffer, "gpg --encrypt --recipient %s "
			"--force-mdc", hint);
		free(hint);

		if (seal_cert(sealed, unsealed, fbuffer) < 0) {
			cbuffer_clear(sealed);
			cbuffer_clear(unsealed);
			cbuffer_write(unsealed, keytemp, keylen);
			fprintf(stderr, "Sealing failed.\n");
			goto reask_seal;
		}
	} else {
		cbuffer_write(sealed, (unsigned char*)buf, 9);
		cbuffer_move_nolimit(sealed, unsealed);
		if (!cbuffer_free(sealed))
			die("Key too large");
	}

retry_name:
	if (server_mode) {
		name = prompt_string("Enter filename to save key as", 0);
		strcpy(fbuffer, name);
	} else {
		name = prompt_string("Enter name for key", 0);
		if (strcspn(name, "@:/[") < strlen(name)) {
			fprintf(stderr, "Bad name\n");
			goto retry_name;
		}
		sprintf(fbuffer, "$XDG_CONFIG_HOME/gits/keys/%s", name);
	}
	if (!strcmp(name, "")) {
		fprintf(stderr, "Bad name\n");
		goto retry_name;
	}

	fd = open_create_dirs(fbuffer, O_WRONLY | O_CREAT | O_EXCL, 0600);
	if (fd < 0) {
		error("Can't open '%s': %s", fbuffer, strerror(errno));
		goto retry_name;
	}
	while (cbuffer_used(sealed)) {
		ssize_t r = cbuffer_write_fd(sealed, fd);
		if (r < 0 && errno != EINTR) {
			perror("write");
			exit(1);
		}
	}
	close(fd);
}
