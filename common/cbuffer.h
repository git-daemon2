/*
 * Copyright (C) Ilari Liusvaara 2009
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */
#ifndef _cbuffer__h__included__
#define _cbuffer__h__included__

#include <stdlib.h>

#ifdef __cplusplus
extern "C" {
#endif

/*
 * Terminology:
 *	Segment:
 *		Memory-contigious range extending from get pointer, put
 *		pointer or start of circular buffer till used/free type
 *		changes.
 *	Current segment:
 *		Segment starting from get pointer or put pointer.
 *
 */

/* Primary circular buffer structure. Opaque type. */
struct cbuffer;

/*
 * Create new circular buffer using specified data area. The data area
 * is not copied and must remain until circular buffer is destroyed.
 *
 * Inputs:
 *	data		The data area to back the circular buffer.
 *	datasize	Size of backing data area.
 *
 * Outputs:
 *	return value	The newly created circular buffer, or NULL
 *			if out of memory.
 */
struct cbuffer *cbuffer_create(unsigned char *data, size_t datasize);


/*
 * Free circular buffer. Data area is not freed.
 *
 * Inputs:
 *	cbuf		The circular buffer to free.
 */
void cbuffer_destroy(struct cbuffer *cbuf);

/*
 * Return number of bytes used in buffer (how many bytes can be read without
 * writes).
 *
 * Inputs:
 *	cbuf		The circular buffer to interrogate.
 *
 * Outputs:
 *	return value	Number of bytes data in buffer.
 */
size_t cbuffer_used(struct cbuffer *cbuf);

/*
 * Return number of bytes free in buffer (how many bytes can be written
 * without reads).
 *
 * Inputs:
 *	cbuf		The circular buffer to interrogate.
 *
 * Outputs:
 *	return value	Number of bytes free in buffer.
 */
size_t cbuffer_free(struct cbuffer *cbuf);

/*
 * Peek specified number of bytes from buffer. The bytes are not removed.
 *
 * Inputs:
 *	cbuf		The circular buffer to peek.
 *	dest		Destination buffer to store the peeked data to.
 *	toread		Number of bytes to peek.
 *
 * Outputs:
 *	Return value	0 on success, -1 if buffer has insufficient
 *			amount of data.
 *
 */
int cbuffer_peek(struct cbuffer *cbuf, unsigned char *dest, size_t toread);

/*
 * Read specified number of bytes from buffer. The bytes read are
 * removed.
 *
 * Inputs:
 *	cbuf		The circular buffer to read.
 *	dest		Destination buffer to store the read data to.
 *	toread		Number of bytes to read.
 *
 * Outputs:
 *	Return value	0 on success, -1 if buffer has insufficient
 *			amount of data.
 */
int cbuffer_read(struct cbuffer *cbuf, unsigned char *dest, size_t toread);

/*
 * Write specified number of bytes to buffer.
 *
 * Inputs:
 *	cbuf		The circular buffer to write.
 *	src		Buffer to read the written data from.
 *	towrite		Number of bytes to write.
 *
 * Outputs:
 *	Return value	0 on success, -1 if buffer has insufficient
 *			free space.
 */
int cbuffer_write(struct cbuffer *cbuf, const unsigned char *src,
	size_t towrite);

/*
 * Move specified number of bytes from buffer to another. The data is
 * removed from source buffer.
 *
 * Inputs:
 *	dest		Destination circular buffer.
 *	src		Source cirrcular buffer.
 *	tomove		Number of bytes to move.
 *
 * Outputs:
 *	Return value	0 on success, -1 if insufficient source buffer
 *			data or insufficient destination buffer space.
 */
int cbuffer_move(struct cbuffer *dest, struct cbuffer *src, size_t tomove);

/*
 * Call read on file descriptor. The call tries to read as much as
 * possible in one go (up to entiere free space of destination
 * buffer).
 *
 * Inputs:
 *	cbuf		Circular buffer to store the data to.
 *	fd		File descriptor to read.
 *
 * Outputs:
 *	Return value	Number of bytes read (>0) on success, 0 if
 *			EOF on file descriptor or -1 if read failed.
 *	errno		Set by read() or readv() on failure.
 *			EAGAIN if there is no free space in circular buffer.
 */
ssize_t cbuffer_read_fd(struct cbuffer *cbuf, int fd);

/*
 * Call write on file descriptor. The call tries to write as much as
 * possible in one go (up to entiere used space of soruce
 * buffer).
 *
 * Inputs:
 *	cbuf		Circular buffer to read data from.
 *	fd		File descriptor to write.
 *
 * Outputs:
 *	Return value	Number of bytes written (>=0) on success,
 *			-1 if write failed.
 *	errno		Set by write() or writev() on failure.
 *			EAGAIN if there is no used space in circular buffer.
 */
ssize_t cbuffer_write_fd(struct cbuffer *cbuf, int fd);

/*
 * Fill buffer read segment for direct from memory read operation on
 * circular buffer. Any read operation on buffer invalidates segment.
 *
 * Inputs:
 *	cbuf		Circular buffer to read from.
 *
 * Outputs:
 *	base		Start address of segemnt is written here.
 *	length		Length of segment is written here.
 *
 * Notes:
 *	- Returned length of 0 is only possible if there is no used
 *	  space in buffer.
 *	- The entiere used space may not be returned in single segment.
 */
void cbuffer_fill_r_segment(struct cbuffer *cbuf, unsigned char **base,
	size_t *length);

/*
 * Commit read segment after direct read operation, marking data as
 * read. The read bytes are removed.
 *
 * Inputs:
 *	cbuf		Circular buffer read from.
 *	length		Length of segment to mark as read. Must
 *			be at most the length gotten from
 *			cbuffer_fill_r_segment.
 */
void cbuffer_commit_r_segment(struct cbuffer *cbuf, size_t length);

/*
 * Fill buffer write segment for direct to memory write operation on
 * circular buffer. Any write operation on buffer invalidates segment.
 *
 * Inputs:
 *	cbuf		Circular buffer to write to.
 *
 * Outputs:
 *	base		Start address of segment is written here.
 *	length		Length of segment is written here.
 *
 * Notes:
 *	- Returned length of 0 is only possible if there is no free
 *	  space in buffer.
 *	- The entiere free space may not be returned in single segment.
 */
void cbuffer_fill_w_segment(struct cbuffer *cbuf, unsigned char **base,
	size_t *length);

/*
 * Commit write segment after direct write operation, marking data as
 * written
 *
 * Inputs:
 *	cbuf		Circular buffer written to.
 *	length		Length of segment to mark as written. Must
 *			be at most the length gotten from
 *			cbuffer_fill_w_segment.
 */
void cbuffer_commit_w_segment(struct cbuffer *cbuf, size_t length);

/*
 * Clear all data in cbuffer, freeing any used space.
 *
 * Inputs:
 *	cbuf		Cirecular buffer to clear.
 */
void cbuffer_clear(struct cbuffer *cbuf);

/*
 * Read number of bytes from circular buffer smaller than limit.
 * The read bytes are removed.
 *
 * Inputs:
 *	cbuf		Circular buffer to read.
 *	dest		Destination to store the read data.
 *	limit		Maximum number of bytes to read.
 *
 * Outputs:
 *	Return value	Number of bytes read.
 */
size_t cbuffer_read_max(struct cbuffer *cbuf, unsigned char *dest,
	size_t limit);

/*
 * Write number of bytes to circular buffer smaller than limit.
 *
 * Inputs:
 *	cbuf		Circular buffer to write.
 *	dest		Source to read the wrritten data.
 *	limit		Maximum number of bytes to write.
 *
 * Outputs:
 *	Return value	Number of bytes written.
 */
size_t cbuffer_write_max(struct cbuffer *cbuf, const unsigned char *src,
	size_t limit);

/*
 * Move number of bytes from circular buffer to another smaller than limit.
 * The read bytes are removed from source circular buffer.
 *
 * Inputs:
 *	dest		Destination circular buffer.
 *	src		Source circular buffer.
 *	limit		Maximum number of bytes to move.
 *
 * Outputs:
 *	Return value	Number of bytes moved.
 */
size_t cbuffer_move_max(struct cbuffer *dest, struct cbuffer *src,
	size_t limit);

/*
 * Move as much bytes from circular buffer to another as possible.
 * The read bytes are removed from source circular buffer.
 *
 * Inputs:
 *	dest		Destination circular buffer.
 *	src		Source circular buffer.
 *
 * Outputs:
 *	Return value	Number of bytes moved.
 */
size_t cbuffer_move_nolimit(struct cbuffer *dest, struct cbuffer *src);


#ifdef __cplusplus
}
#endif

#endif
