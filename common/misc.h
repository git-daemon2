/*
 * Copyright (C) Ilari Liusvaara 2009
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */
#ifndef _misc__h__included__
#define _misc__h__included__

#ifdef __cplusplus
extern "C" {
#endif

/*
 * Forcibly close the file descriptor.
 *
 *Input:
 *	fd		The file descriptor.
 */
void force_close(int fd);

#ifdef __cplusplus
}
#endif

#endif
