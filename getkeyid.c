/*
 * Copyright (C) Ilari Liusvaara 2009-2010
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */
#include "certificate.h"
#include "home.h"
#include "ssh.h"
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <string.h>
#include <gnutls/openpgp.h>
#include <gcrypt.h>
#ifdef USE_COMPAT_H
#include "compat.h"
#else
#include "git-compat-util.h"
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static void do_help()
{
	printf("gits-get-key-name: Get name for keypair or hostkey.\n");
	printf("Command line options:\n");
	printf("--help\n");
	printf("\tThis help\n");
	printf("--ssh\n");
	printf("\tRead SSH keys\n");
	printf("<keyfile>\n");
	printf("\tRead key file <keyfile>. Name must contain at least\n");
	printf("\tone '/'\n");
	printf("<keyname>\n");
	printf("\tRead key named <keyname>. Name must not contain\n");
	printf("\t'/'\n");
	printf("\n");
	printf("Note: These key names are used in hostkey database and\n");
	printf("as user names seen by authorization program.\n");
	exit(0);
}

#define MAXKEY 65536
#define HASHBUFFERSIZE 28
#define HASHFUNCTION GCRY_MD_SHA224

void dump_blob(const char *name, const unsigned char *x, size_t i);

int do_ssh_key(const char *name)
{
	FILE* filp;
	int s;
	char filename[PATH_MAX + 1];
	unsigned char key[MAXKEY];
	size_t keysize;
	char *path;
	unsigned char *x;
	size_t i;
	unsigned char hbuffer[HASHBUFFERSIZE];

	if (strchr(name, '/'))
		s = snprintf(filename, PATH_MAX + 1, "%s", name);
	else
		s = snprintf(filename, PATH_MAX + 1, "~/.ssh/%s.pub",
			name);
	if (s < 0 || s > PATH_MAX)
		die("Insanely long keyname");

	path = expand_path(filename);

	/* Read the pubkey file. */
	filp = fopen(path, "r");
	if (!filp)
		die("Can't open '%s'", path);
	keysize = fread(key, 1, MAXKEY, filp);
	if (ferror(filp))
		die("Can't read '%s'", path);
	if (!feof(filp))
		die("Keyfile '%s' too large", path);
	fclose(filp);

	x = extract_key_from_file(key, keysize, &i);

	gcry_md_hash_buffer(HASHFUNCTION, hbuffer, x, i);

	printf("ssh-");
	for (s = 0; s < HASHBUFFERSIZE; s++)
		printf("%02x", hbuffer[s]);
	printf("\n");
	return 0;
}

/* Be ready in case some joker decides to use 1024 bit hash as fingerprint. */
#define KEYBUF 128

int main(int argc, char **argv)
{
	struct certificate certificate;
	gnutls_openpgp_crt_t cert;
	char filename[PATH_MAX + 1];
	char *filename2;
	unsigned char key[KEYBUF];
	int s;
	unsigned vout;
	size_t vout2;

	if (argc < 2 || argc > 3) {
		fprintf(stderr, "syntax: %s <keyname>\n", argv[0]);
		fprintf(stderr, "syntax: %s <keyfile>\n", argv[0]);
		fprintf(stderr, "syntax: %s --ssh <keyname>\n", argv[0]);
		fprintf(stderr, "syntax: %s --ssh <keyfile>\n", argv[0]);
		return 1;
	}

	if (!strcmp(argv[1], "--help"))
		do_help();

	s = gnutls_global_init();
	if (s < 0)
		die("Can't initialize GnuTLS: %s",
			gnutls_strerror(s));

	if (!strcmp(argv[1], "--ssh"))
		return do_ssh_key(argv[2]);

	if (strchr(argv[1], '/'))
		s = snprintf(filename, PATH_MAX + 1, "%s", argv[1]);
	else
		s = snprintf(filename, PATH_MAX + 1, "$XDG_CONFIG_HOME/.gits/keys/%s",
			argv[1]);
	if (s < 0 || s > PATH_MAX)
		die("Insanely long keyname");


	filename2 = expand_path(filename);
	certificate = parse_certificate(filename2, &s);
	if (s) {
		if (s == CERTERR_NOCERT)
			die("Can't find key %s", filename);
		else if (s == CERTERR_CANTREAD)
			die_errno("Can't read key");
		else
			die("Can't parse key: %s",
				cert_parse_strerr(s));
	}

	s = gnutls_openpgp_crt_init(&cert);
	if (s < 0)
		die("Can't allocate space for key: %s",
			gnutls_strerror(s));

	s = gnutls_openpgp_crt_import(cert, &certificate.public_key,
		GNUTLS_OPENPGP_FMT_RAW);
	if (s < 0)
		die("Bad key: %s", gnutls_strerror(s));

	s = gnutls_openpgp_crt_verify_self(cert, 0, &vout);
	if (s < 0)
		die("Bad key: %s", gnutls_strerror(s));
	if (vout)
		die("Bad key: Validation failed");

	vout2 = KEYBUF;
	s = gnutls_openpgp_crt_get_fingerprint(cert, key, &vout2);
	if (s < 0)
		die("Bad key: %s", gnutls_strerror(s));

	gnutls_openpgp_crt_deinit(cert);

	printf("openpgp-");
	for (s = 0; s < (int)vout2; s++)
		printf("%02x", key[s]);
	printf("\n");
	return 0;
}
