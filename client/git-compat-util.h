/*
 * Copyright (C) Ilari Liusvaara 2009-2010
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */
#ifndef _compat__h__included__
#define _compat__h__included__

#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>

void *xmalloc(size_t size);
void warning(const char *fmt, ...);
void error(const char *fmt, ...);
void die(const char *fmt, ...);
void die_errno(const char *fmt, ...);
int prefixcmp(const char *str, const char *prefix);
FILE *xfdopen(int fd, const char *mode);
void write_or_die(int fd, const void *buffer, size_t size);
char *xstrdup(const char *str);
char *xstrndup(const char *str, size_t n);

struct child_process
{
	const char **argv;
	int in;
	int out;
	int err;
	pid_t child_pid;
};

int start_command(struct child_process *child);
int finish_command(struct child_process *child);

#endif
