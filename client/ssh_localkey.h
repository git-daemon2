/*
 * Copyright (C) Ilari Liusvaara 2010
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */
#ifndef _ssh_localkey__h__include__
#define _ssh_localkey__h__include__

#include <stdlib.h>

unsigned char *sign_using_local_key(const char *keyname,
	const unsigned char *kblob, size_t kblob_size,
	const unsigned char *challenge, size_t challenge_size,
	size_t *signature_length, const char *type);

#endif
