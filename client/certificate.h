/*
 * Copyright (C) Ilari Liusvaara 2009
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */
#ifndef _certificate__h__included__
#define _certificate__h__included__

#include <gnutls/gnutls.h>

#define CERTERR_OK		0
#define CERTERR_NOCERT		-2
#define CERTERR_INVALID		-3
#define CERTERR_CANTREAD	-4
#define CERTERR_TOOBIG		-5

struct certificate
{
	gnutls_datum_t public_key;
	gnutls_datum_t private_key;
};

struct certificate parse_certificate(const char *name, int *errorcode);
const char *cert_parse_strerr(int errcode);

#endif
