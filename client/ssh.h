/*
 * Copyright (C) Ilari Liusvaara 2010
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */
#ifndef _ssh__h__included__
#define _ssh__h__included__

#include "user.h"

void do_ssh_preauth(const char *ssh_user);
void send_ssh_authentication(struct user *dispatcher, const char *ssh_user);
unsigned char *extract_key_from_file(const unsigned char *file,
	size_t file_len, size_t *key_len);

#endif
