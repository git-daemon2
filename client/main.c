/*
 * Copyright (C) Ilari Liusvaara 2009-2010
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */
#include "user.h"
#include "srp_askpass.h"
#include "keypairs.h"
#include "hostkey.h"
#include "connect.h"
#include "ssh.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
#include <poll.h>
#include <gnutls/gnutls.h>
#include <signal.h>
#include "git-compat-util.h"

volatile sig_atomic_t in_handler = 0;
volatile sig_atomic_t sigusr1_flag = 0;
struct user *debug_for = NULL;
int verbose = 0;
static int socket_fd;

static void sigusr1_handler(int x)
{
	x = 0;
	if (in_handler)
		sigusr1_flag = 1;
	else
		user_debug(debug_for, stderr);
}

struct parsed_addr
{
	char *protocol;		/* Protocol part */
	char *user;		/* User part, NULL if no user */
	char *host;		/* Hostname */
	char *uservid;		/* Unique server ID */
	char *port;		/* Port as string, NULL if no port */
	char *path;		/* Path part. */
	char *vhost_header;	/* vhost header to send */
	char *transport_proto;	/* Transport protocol. */
	char *address_space;	/* Address space to use. May be NULL */
};

void append_uniq_address(char *buffer, struct parsed_addr *_addr,
	int default_flag)
{
	char *ptr;
	ptr = strchr(_addr->host, '~');
	if (!ptr)
		ptr = _addr->host;
	else
		ptr++;
	if (strchr(ptr, ':'))
		strcat(buffer, "[");
	strcat(buffer, ptr);
	if (strchr(_addr->host, ':'))
		strcat(buffer, "]");
	if(!default_flag) {
		strcat(buffer, ":");
		strcat(buffer, _addr->port);
	}
}

struct parsed_addr parse_address(const char *addr)
{
	struct parsed_addr _addr;
	const char *proto_end;
	const char *path_start;
	const char *uhp_start;
	const char *uhp_delim;
	const char *orig_addr = addr;
	const char *proto_sep;
	size_t addrlen;
	int nondefault_port = 0;
	size_t vhost_len;

	_addr.transport_proto = xstrdup("tcp");
	_addr.address_space = NULL;

	addrlen = strlen(addr);

	proto_end = strchr(addr, ':');
	if (!proto_end)
		die("URL '%s': No ':' to end protocol.", orig_addr);

	_addr.protocol = xstrndup(addr, proto_end - addr);
	if (strncmp(proto_end, "://", 3))
		die("URL '%s': No '://' to end protocol.", orig_addr);

	uhp_start = proto_end + 3;

	/* Figure out the user if any. */
	uhp_delim = strpbrk(uhp_start, "@[:/");

	if (*uhp_delim == '@') {
		_addr.user = xstrndup(uhp_start,
			uhp_delim - uhp_start);
		uhp_start = uhp_delim + 1;
	} else {
		_addr.user = NULL;
	}

	/* Figure out host. */
	if (*uhp_start == '[') {
		uhp_delim = strpbrk(uhp_start, "]");
		if (uhp_delim) {
			_addr.host = xstrndup(uhp_start + 1,
				uhp_delim - uhp_start - 1);
			if (uhp_delim[1] != ':' && uhp_delim[1] != '/')
				die("URL '%s': Expected port or path after hostname",
					orig_addr);
			uhp_start = uhp_delim + 1;
		} else
			die("URL '%s': Hostname has '[' without matching ']'",
				orig_addr);
	} else {
		uhp_delim = strpbrk(uhp_start, "[:/");
		if (*uhp_delim == '[')
			die("URL '%s': Unexpected '['", orig_addr);
		_addr.host = xstrndup(uhp_start, uhp_delim - uhp_start);
		uhp_start = uhp_delim;
	}

	_addr.uservid = NULL;
	proto_sep = strchr(_addr.host, '@');
	if (proto_sep && proto_sep != _addr.host)
	{
		char *old_host;
		old_host = _addr.host;
		_addr.uservid = xstrndup(_addr.host, proto_sep - _addr.host);
		_addr.host = xstrdup(proto_sep + 1);
		free(old_host);
	}


	proto_sep = strchr(_addr.host, '~');
	if (proto_sep)
	{
		free(_addr.transport_proto);
		free(_addr.address_space);

		const char *ptr2;
		char *old_host;
		old_host = _addr.host;
		ptr2 = strchr(old_host, '/');
		if (ptr2 >= proto_sep)
			ptr2 = NULL;

		if (ptr2) {
			_addr.transport_proto = xstrndup(_addr.host,
				ptr2 - old_host);
			_addr.address_space = xstrndup(ptr2 + 1,
				proto_sep - ptr2 - 1);
		} else if (proto_sep == old_host + 4 &&
			!strncmp(old_host, "unix", 4)) {
			_addr.transport_proto = xstrdup("<N/A>");
			_addr.address_space = xstrdup("unix");
		} else {
			char suffix = '\0';
			if(proto_sep > old_host)
				suffix = proto_sep[-1];
			if(suffix == '4') {
				_addr.transport_proto = xstrndup(
					old_host, proto_sep - old_host - 1);
				_addr.address_space = xstrdup("ipv4");
			} else if(suffix == '6') {
				_addr.transport_proto = xstrndup(
					old_host, proto_sep - old_host - 1);
				_addr.address_space = xstrdup("ipv6");
			} else if(suffix == '_') {
				_addr.transport_proto = xstrndup(
					old_host, proto_sep - old_host - 1);
				_addr.address_space = NULL;
			} else {
				_addr.transport_proto = xstrndup(
					old_host, proto_sep - old_host);
				_addr.address_space = NULL;
			}
		}
		_addr.host = xstrdup(proto_sep + 1);
		free(old_host);
	}

	path_start = strchr(uhp_start, '/');
	if (!path_start)
		die("URL '%s': No '/' to start path", orig_addr);

	_addr.path = xstrndup(path_start, addrlen - (path_start - addr));

	if (*uhp_start == ':')
		_addr.port = xstrndup(uhp_start + 1,
			path_start - uhp_start - 1);
	else
		_addr.port = NULL;

	if (!*_addr.host)
		die("URL '%s': Empty hostname not allowed", orig_addr);

	if (strcmp(_addr.protocol, "gits") && strcmp(_addr.protocol, "tls") &&
		strcmp(_addr.protocol, "git")) {
		die("Unknown protocol %s://", _addr.protocol);
	}

	if (!strcmp(_addr.protocol, "git") && _addr.user) {
		die("git:// does not support users");
	}

	if (!strcmp(_addr.protocol, "git") && _addr.uservid) {
		die("git:// does not support unique server identitifier");
	}

	if (_addr.port) {
		nondefault_port = 1;
	} else if (!strcmp(_addr.protocol, "gits")) {
		_addr.port = xstrndup("git", 3);
	} else if (!strcmp(_addr.protocol, "git")) {
		_addr.port = xstrndup("git", 3);
	} else if (!strcmp(_addr.protocol, "tls")) {
		_addr.port = xstrndup("gits", 4);
	}

	/* 8 is for host=[]:\0 */
	vhost_len = 9 + strlen(_addr.host) + strlen(_addr.port);
	_addr.vhost_header = xmalloc(vhost_len);

	strcpy(_addr.vhost_header, "host=");
	append_uniq_address(_addr.vhost_header, &_addr, !nondefault_port);

	if (verbose) {
		fprintf(stderr, "Protocol:           %s\n", _addr.protocol);
		fprintf(stderr, "User:               %s\n", _addr.user ?
			_addr.user :  "<not set>");
		fprintf(stderr, "Host:               %s\n", _addr.host);
		fprintf(stderr, "Unique server ID:   %s\n", _addr.uservid ?
			_addr.uservid : "<any>");
		fprintf(stderr, "Port:               %s\n", _addr.port);
		fprintf(stderr, "Path:               %s\n", _addr.path);
		fprintf(stderr, "Vhost header:       %s\n",
			_addr.vhost_header);
		fprintf(stderr, "Transport protocol: %s\n",
			_addr.transport_proto);
		fprintf(stderr, "Address space:      %s\n",
			_addr.address_space ? _addr.address_space :
			"<unspecified>");
		fprintf(stderr, "\n");
	}

	return _addr;
}

#define MODE_ALLOW_EOF 0
#define MODE_HANDSHAKE 1
#define MAXFDS 128

static void disconnect_outgoing(struct user *user)
{
	if(user_get_red_out(user))
		user_set_red_io(user, -1, -1, -1);
	else
		user_set_red_io(user, -1, 1, -1);
	user_send_red_in_eof(user);
}

static void traffic_loop(struct user *user, int mode)
{
	fd_set rfds;
	fd_set wfds;
	struct pollfd polled[MAXFDS];
	int fdcount = 0;
	int failcode = 0;
	struct timeval deadline;
	int bound = 0;
	int r;
	FD_ZERO(&rfds);
	FD_ZERO(&wfds);
	user_add_to_sets(user, &bound, &rfds, &wfds, &deadline);
	if (bound == 0) {
		failcode = user_get_failure(user);
		if (failcode)
			goto failed;
		return;
	}
	for(r = 0; r < bound || r <= socket_fd; r++) {
		int added = 0;
		polled[fdcount].fd = r;
		polled[fdcount].events = 0;
		polled[fdcount].revents = 0;
		if(FD_ISSET(r, &rfds)) {
			polled[fdcount].events |= (POLLIN | POLLHUP);
			added = 1;
		}
		if(FD_ISSET(r, &wfds)) {
			polled[fdcount].events |= (POLLOUT | POLLHUP);
			added = 1;
		}
		if(r == socket_fd) {
			polled[fdcount].events |= POLLHUP;
			added = 1;
		}
		if(added)
			fdcount++;
	}

	r = poll(polled, fdcount, -1);
	if (r < 0 && errno != EINTR) {
		die_errno("poll() failed");
	} else if (r <= 0) {
		FD_ZERO(&rfds);
		FD_ZERO(&wfds);
	} else {
		FD_ZERO(&rfds);
		FD_ZERO(&wfds);
		for(r = 0; r < fdcount; r++) {
			if(polled[r].revents & POLLHUP) {
				/* Write stream hangup. Disconnect. */
				if(polled[r].fd == 0)
					FD_SET(0, &rfds);
				if(polled[r].fd == 1)
					FD_SET(1, &wfds);
				if(polled[r].fd > 1)
					disconnect_outgoing(user);
			}
			if(polled[r].revents & POLLIN)
				FD_SET(polled[r].fd, &rfds);
			if(polled[r].revents & POLLOUT)
				FD_SET(polled[r].fd, &wfds);
		}
	}
	in_handler = 1;
	user_service(user, &rfds, &wfds);
	in_handler = 0;
	if (sigusr1_flag) {
		sigusr1_flag = 0;
		user_debug(debug_for, stderr);
	}
	failcode = user_get_failure(user);
	if (failcode)
		goto failed;
	if (user_red_out_eofd(user) && !cbuffer_used(
		user_get_red_out_force(user))) {
		failcode = 1;
		goto failed;
	}

	return;
failed:
	if (failcode > 0 && mode == MODE_ALLOW_EOF)
		return;
	else if (failcode > 0) {
		error("Expected more data, got connection closed");
		fprintf(stderr, "Debug dump:\n");
		user_debug(user, stderr);
		die("Expected more data, got connection closed");
	} else if (failcode < 0) {
		const char *major;
		const char *minor;

		major = user_explain_failure(failcode);
		minor = user_get_error(user);

		if (minor)
			die("Connection lost: %s (%s)", major, minor);
		else
			die("Connection lost: %s", major);
	}
}

static int select_keypair(gnutls_certificate_credentials_t creds,
	const char *username, int must_succeed)
{
	int ret;
	ret = select_keypair_int(creds, username);
	if (ret < 0 && must_succeed)
		die("No keypair identity %s found", username);
	return ret;
}

static gnutls_session_t session;

static void preconfigure_tls(const char *username)
{
	int s;
	gnutls_certificate_credentials_t creds;
	int keypair_ok = 0;
#ifndef DISABLE_SRP
	const char *srp_password;
	gnutls_srp_client_credentials_t srp_cred;
#endif
	int kx[3];

	s = gnutls_global_init();
	if (s < 0)
		die("Can't initialize GnuTLS: %s", gnutls_strerror(s));

	s = gnutls_certificate_allocate_credentials(&creds);
	if (s < 0)
		die("Can't allocate cert creds: %s", gnutls_strerror(s));

	s = gnutls_init(&session, GNUTLS_CLIENT);
	if (s < 0)
		die("Can't allocate session: %s", gnutls_strerror(s));

#ifndef DISABLE_SRP
	s = gnutls_priority_set_direct (session, "NORMAL:+SRP-DSS:+SRP-RSA",
		NULL);
#else
	s = gnutls_priority_set_direct (session, "NORMAL", NULL);
#endif
	if (s < 0)
		die("Can't set priority: %s", gnutls_strerror(s));

	if (username) {
		if (!prefixcmp(username, "ssh-")) {
			do_ssh_preauth(username + 4);
			keypair_ok = 1;
			goto skip_ksel;
		}
		if (!prefixcmp(username, "key-")) {
			select_keypair(creds, username + 4, 1);
			keypair_ok = 1;
		} else
			keypair_ok = (select_keypair(creds, username, 0)
				>= 0);
	}

skip_ksel:
	s = gnutls_credentials_set (session, GNUTLS_CRD_CERTIFICATE, creds);
	if (s < 0)
		die("Can't set creds: %s", gnutls_strerror(s));

	if (keypair_ok)
		goto no_srp;
#ifndef DISABLE_SRP
	if (username && !prefixcmp(username, "srp-"))
		username = username + 4;
	if (!username || !*username)
		goto no_srp;

	s = gnutls_srp_allocate_client_credentials(&srp_cred);
	if (s < 0)
		die("Can't allocate SRP creds: %s", gnutls_strerror(s));

	s = 0;
	srp_password = get_srp_password(username);
	s = gnutls_srp_set_client_credentials(srp_cred, username, srp_password);
	if (s < 0)
		die("Can't set SRP creds: %s", gnutls_strerror(s));

	s = gnutls_credentials_set(session, GNUTLS_CRD_SRP, srp_cred);
	if (s < 0)
		die("Can't use SRP creds: %s", gnutls_strerror(s));

	/* GnuTLS doesn't seem to like to use SRP. Force it. */
	kx[0] = GNUTLS_KX_SRP_DSS;
	kx[1] = GNUTLS_KX_SRP_RSA;
	kx[2] = 0;
	s = gnutls_kx_set_priority(session, kx);
	if (s < 0)
		die("Can't force SRP: %s", gnutls_strerror(s));
#endif
	goto skip_force;
no_srp:
	kx[0] = GNUTLS_KX_DHE_DSS;
	kx[1] = GNUTLS_KX_DHE_RSA;
	kx[2] = 0;
	s = gnutls_kx_set_priority(session, kx);
	if (s < 0)
		die("Can't force Diffie-Hellman: %s", gnutls_strerror(s));
skip_force:
	;
}

static void configure_tls(struct user *user, const char *hostname)
{
	user_configure_tls(user, session);

	/* Wait for TLS connection to establish. */
	while (!user_get_tls(user))
		traffic_loop(user, MODE_HANDSHAKE);

	if(hostname)
		check_hostkey(session, hostname);
}

#define MAX_REQUEST 8192
const char *hexes = "0123456789abcdef";

static void do_request(const char *arg, struct parsed_addr *addr,
	int suppress_ok, int no_repo)
{
	int fd;
	struct user *dispatcher;
	struct cbuffer *inbuf;
	struct cbuffer *outbuf;
	const char *major;
	const char *minor;
	char reqbuf[MAX_REQUEST + 4];
	size_t reqsize;
	int do_tls = 0;

#ifdef SIGPIPE
	signal(SIGPIPE, SIG_IGN);
#endif

	preconfigure_tls(addr->user);

	fd = connect_host(addr->host, addr->port,
		addr->transport_proto, addr->address_space);

	/* Create dispatcher with no time limit. */
	debug_for = dispatcher = user_create(fd, 65535);
	if (!dispatcher)
		die("Can't create connection context");
	socket_fd = fd;
	user_clear_deadline(dispatcher);

	inbuf = user_get_red_in(dispatcher);
	outbuf = user_get_red_out(dispatcher);
	if (!strcmp(addr->protocol, "git")) {
		; /* Not protected. */
	} else if (!strcmp(addr->protocol, "tls")) {
		if (verbose)
			fprintf(stderr, "Configuring TLS...\n");
		configure_tls(dispatcher, addr->uservid);
		do_tls = 1;
	} else {
		if (verbose)
			fprintf(stderr, "Requesting TLS...\n");
		cbuffer_write(inbuf, (unsigned char*)"000cstarttls", 12);
		while (1) {
			char tmpbuf[9];
			int s;
			traffic_loop(dispatcher, MODE_HANDSHAKE);
			s = cbuffer_peek(outbuf, (unsigned char*)tmpbuf, 8);
			tmpbuf[8] = '\0';
			if (s >= 0 && !strcmp(tmpbuf, "proceed\n"))
				break;
			if (s >= 0 && !strcmp(tmpbuf, "notsupp\n"))
				die("Server does not support gits://");
			if (user_red_out_eofd(dispatcher))
				goto wait_eofd;
			if (user_get_failure(dispatcher))
				goto wait_failed;
		}
		if (verbose)
			fprintf(stderr, "Server ready for TLS. Configuring TLS...\n");
		configure_tls(dispatcher, addr->uservid);
		do_tls = 1;
	}

	if (do_tls) {
		if (verbose)
			fprintf(stderr, "Waiting for TLS link to establish...\n");

		while (!user_get_tls(dispatcher))
			traffic_loop(dispatcher, MODE_HANDSHAKE);

		if (verbose)
			fprintf(stderr, "Secure link established.\n");
	}

	if (addr->user && !prefixcmp(addr->user, "ssh-")) {
		if (verbose)
			fprintf(stderr, "Sending SSH auth request...\n");
		send_ssh_authentication(dispatcher, addr->user + 4);
		while (cbuffer_used(inbuf))
			traffic_loop(dispatcher, MODE_HANDSHAKE);
		if (verbose)
			fprintf(stderr, "Sent SSH auth request.\n");
	}

	if (!no_repo)
		reqsize = strlen(arg) + strlen(addr->path) +  3 +
			strlen(addr->vhost_header);
	else
		reqsize = strlen(arg);

	if (reqsize > MAX_REQUEST)
		die("Request too big to send");

	memcpy(reqbuf + 4, arg, strlen(arg));
	if (!no_repo) {
		reqbuf[strlen(arg) + 4] = ' ';
		memcpy(reqbuf + strlen(arg) + 5, addr->path, strlen(addr->path) + 1);
		memcpy(reqbuf + strlen(arg) + 6 + strlen(addr->path),
			addr->vhost_header, strlen(addr->vhost_header) + 1);
	}

	reqbuf[0] = hexes[((reqsize + 4) >> 12) & 0xF];
	reqbuf[1] = hexes[((reqsize + 4) >> 8) & 0xF];
	reqbuf[2] = hexes[((reqsize + 4) >> 4) & 0xF];
	reqbuf[3] = hexes[(reqsize + 4) & 0xF];

	if (verbose)
		fprintf(stderr, "Sending request...\n");
	cbuffer_write(inbuf, (unsigned char*)reqbuf, reqsize + 4);
	if (verbose)
		fprintf(stderr, "Request sent, waiting for reply...\n");
	while (!cbuffer_used(outbuf) && !suppress_ok)
		traffic_loop(dispatcher, MODE_HANDSHAKE);
	if (verbose)
		fprintf(stderr, "Server replied.\n");
	/* Ok, remote end has replied. */
	if(!suppress_ok)
		printf("\n");
	fflush(stdout);
	user_set_red_io(dispatcher, 0, 1, -1);

	while (!user_get_failure(dispatcher))
		traffic_loop(dispatcher, MODE_ALLOW_EOF);
	exit(0);

wait_failed:
	major = user_explain_failure(user_get_failure(dispatcher));
	minor = user_get_error(dispatcher);

	if (minor)
		die("Connection lost: %s (%s)", major, minor);
	else
		die("Connection lost: %s", major);
	exit(128);
wait_eofd:
	die("Expected response to starttls, server closed connection.");
	exit(128);
}

int main(int argc, char **argv)
{
	struct parsed_addr paddr;
	char buffer[8192];

	if (getenv("GITS_VERBOSE"))
		verbose = 1;

	if (argc < 3) {
		die("Need two arguments");
	}

	signal(SIGUSR1, sigusr1_handler);

	paddr = parse_address(argv[2]);

	if (!prefixcmp(argv[1], "--service=")) {
		do_request(argv[1] + 10, &paddr, 1, 0);
		return 0;
	}
	if (!prefixcmp(argv[1], "--nourl-service=")) {
		do_request(argv[1] + 16, &paddr, 1, 1);
		return 0;
	}

	while (1) {
		char *cmd;

		cmd = fgets(buffer, 8190, stdin);
		if (cmd[strlen(cmd) - 1] == '\n')
			cmd[strlen(cmd) - 1] = '\0';

		if (!strcmp(cmd, "capabilities")) {
			printf("*connect\n\n");
			fflush(stdout);
		} else if (!*cmd) {
			exit(0);
		} else if (!prefixcmp(cmd, "connect ")) {
			do_request(cmd + 8, &paddr, 0, 0);
			return 0;
		} else
			die("Unknown command %s", cmd);
	}
	return 0;
}
