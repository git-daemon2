/*
 * Copyright (C) Ilari Liusvaara 2009
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */
#ifndef _srp_askpass__h__included__
#define _srp_askpass__h__included__

/* Get password. Return is malloced */
char *get_password(const char *prompt);
char *get_srp_password(const char *username);
char *get_ssh_password(const char *keyname);

#endif
