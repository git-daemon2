/*
 * Copyright (C) Ilari Liusvaara 2010
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */
#ifndef _pem__h__included__
#define _pem__h__included__

#include <stdlib.h>

unsigned char *pem_decode(const unsigned char *file, size_t filesize,
	const char* keyname, size_t *keysize);

#endif
