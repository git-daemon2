/*
 * Copyright (C) Ilari Liusvaara 2009-2010
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */
#include "git-compat-util.h"
#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <errno.h>
#include <unistd.h>
#include <sys/wait.h>

void *xmalloc(size_t size)
{
	void *addr;
	addr = malloc(size);
	if (!addr) {
		fprintf(stderr, "fatal: Malloc failed, out of memory\n");
		exit(128);
	}
	return addr;
}


void warning(const char *fmt, ...)
{
	va_list args;
	fprintf(stderr, "warning: ");
	va_start(args, fmt);
	vfprintf(stderr, fmt, args);
	va_end(args);
	fprintf(stderr, "\n");
}

void error(const char *fmt, ...)
{
	va_list args;
	fprintf(stderr, "error: ");
	va_start(args, fmt);
	vfprintf(stderr, fmt, args);
	va_end(args);
	fprintf(stderr, "\n");
}

void die(const char *fmt, ...)
{
	va_list args;
	fprintf(stderr, "fatal: ");
	va_start(args, fmt);
	vfprintf(stderr, fmt, args);
	va_end(args);
	fprintf(stderr, "\n");
	exit(128);
}

void die_errno(const char *fmt, ...)
{
	int err = errno;
	va_list args;
	fprintf(stderr, "fatal: ");
	va_start(args, fmt);
	vfprintf(stderr, fmt, args);
	va_end(args);
	fprintf(stderr, ": %s\n", strerror(err));
	exit(128);
}

int prefixcmp(const char *str, const char *prefix)
{
	return strncmp(str, prefix, strlen(prefix));
}

FILE *xfdopen(int fd, const char *mode)
{
	FILE *r;
	r = fdopen(fd, mode);
	if (!r)
		die("Can't fdopen file descriptor");
	return r;
}

void write_or_die(int fd, const void *buffer, size_t size)
{
	const char *x = (const char*)buffer;
	ssize_t r;

	if (size == 0)
		return;

	r = write(fd, buffer, size);
	if (r < 0 && errno != EINTR && errno != EWOULDBLOCK)
		die("Write error: %s\n", strerror(errno));
	else if (r < 0)
		write_or_die(fd, buffer, size);
	else
		write_or_die(fd, x + r, size - r);
}


int start_command(struct child_process *child)
{
	int p;
	int pipes[4];

	if (pipe(pipes + 0) < 0)
		die_errno("Pipe failed");
	if (pipe(pipes + 2) < 0)
		die_errno("Pipe failed");

	p = fork();
	if (p < 0) {
		die_errno("Fork failed");
	} else if (p == 0) {
		close(pipes[1]);
		close(pipes[2]);

		if (dup2(pipes[0], 0) < 0)
			die_errno("dup2 failed");
		if (dup2(pipes[3], 1) < 0)
			die_errno("dup2 failed");

		execvp(child->argv[0], (char *const*)child->argv);
		die_errno("Can't run %s", child->argv[0]);
	} else if (p > 0) {
		close(pipes[0]);
		close(pipes[3]);
		child->child_pid = p;
		child->in = pipes[1];
		child->out = pipes[2];
	}
	return 0;
}

int finish_command(struct child_process *child)
{
	int status;

	errno = 0;
	while (waitpid(child->child_pid, &status, 0) < 0)
		if (errno == ECHILD)
			die("Waiting for what process?");
	if (WIFSIGNALED(status)) {
		fprintf(stderr, "Subprocess crashed with signal %i.\n",
			WTERMSIG(status));
		return WTERMSIG(status) - 128;
	} else if (WIFEXITED(status)) {
		if (WEXITSTATUS(status))
			fprintf(stderr, "Subprocess exited with code %i.\n",
				WEXITSTATUS(status));
		return WEXITSTATUS(status);
	} else
		die("Unknown subprocess abend: %i", status);
	return 0;
}

char *xstrdup(const char *str)
{
	char *ret;
	ret = xmalloc(strlen(str) + 1);
	strcpy(ret, str);
	return ret;
}

char *xstrndup(const char *str, size_t n)
{
	char *ret;
	ret = xmalloc(n + 1);
	strncpy(ret, str, n);
	ret[n] = 0;
	return ret;
}
