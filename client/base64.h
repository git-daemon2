/*
 * Copyright (C) Ilari Liusvaara 2010
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */
#ifndef _base64__h__included__
#define _base64__h__included__

#include <stdlib.h>
#include <stdint.h>

unsigned char *decode_base64_chunk(const unsigned char *chunk,
	size_t chunk_len, size_t *key_len);
const unsigned char *base64_blob_start(const unsigned char *blob,
	size_t *remaining);
const unsigned char *next_line(const unsigned char *blob,
	size_t *remaining);
void encode_uint32(unsigned char *ptr, uint32_t value);

#endif
