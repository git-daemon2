/*
 * Copyright (C) Ilari Liusvaara 2009-2010
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */
#include "srp_askpass.h"
#include "prompt.h"
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <stdio.h>
#include "git-compat-util.h"

#define PROMPTBUF 8192
#define CMDBUFSIZE 16384

/* Use GITS_ASKPASS to ask for password. */
static char *get_password_via_external(const char *prompt,
	const char *prog)
{
	static char buffer[PROMPTBUF + 1];
	static char cmdbuffer[CMDBUFSIZE + 1];
	char *ans;
	int escape = 0;
	int idx;
	int widx = 0;
	FILE *out;

	if (strchr(prompt, '\"'))
		die("Can't prompt for names containing '\"'");

	for (idx = 0; prog[idx]; idx++) {
		if (!escape && prog[idx] == '%')
			escape = 1;
		else if (escape && prog[idx] == 'p') {
			if (widx + strlen(prompt) + 2 >= CMDBUFSIZE)
				die("Command line too long");
			cmdbuffer[widx++] = '\"';
			strcpy(cmdbuffer + widx, prompt);
			widx += strlen(prompt);
			cmdbuffer[widx++] = '\"';
		} else {
			if (widx + 1 >= CMDBUFSIZE)
				die("Command line too long");
			cmdbuffer[widx++] = prog[idx];
			escape = 0;
		}
	}
	cmdbuffer[widx++] = '\0';

	out = popen(cmdbuffer, "r");
	if (!out)
		die_errno("Can't invoke $GITS_ASKPASS");

	if (!fgets(buffer, PROMPTBUF - 2, out)) {
		if (ferror(out))
			die("Can't read password");
	}

	if (strlen(buffer) > 0 && buffer[strlen(buffer) - 1] == '\n')
		buffer[strlen(buffer) - 1] = '\0';

	if (pclose(out))
		die("Authentication canceled");

	ans = xstrdup(buffer);
	return ans;
}

char *get_password(const char *prompt)
{
	if (getenv("GITS_ASKPASS"))
		return get_password_via_external(prompt,
			getenv("GITS_ASKPASS"));

	return prompt_string(prompt, 1);
}

char *get_srp_password(const char *username)
{
	static char buffer[PROMPTBUF + 1];
	int len;

	len = snprintf(buffer, PROMPTBUF + 1, "Enter SRP password for %s",
		username);
	if (len < 0 || len > PROMPTBUF)
		die("SRP Username is insanely long");

	return get_password(buffer);
}

char *get_ssh_password(const char *keyname)
{
	static char buffer[PROMPTBUF + 1];
	int len;

	len = snprintf(buffer, PROMPTBUF + 1, "Enter passphrase to unlock %s",
		keyname);
	if (len < 0 || len > PROMPTBUF)
		die("Key name is insanely long");

	return get_password(buffer);
}
