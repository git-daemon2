/*
 * Copyright (C) Ilari Liusvaara 2009-2010
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */
#ifndef _connect__h__included__
#define _connect__h__included__

#include <stdlib.h>

//Returns connected fd or dies.
int connect_host(const char *host, const char *port,
	const char *tproto, const char *addrspace);

#endif
