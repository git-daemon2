/*
 * Copyright (C) Ilari Liusvaara 2010
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */
#include "pem.h"
#include "pem_decrypt.h"
#include "base64.h"
#include <string.h>
#include "git-compat-util.h"

void dump_blob(const char *name, const unsigned char *blob, size_t bloblen);

static const unsigned char *strstr_n(const unsigned char *haystack,
	const unsigned char *needle, size_t haystack_size)
{
	size_t maxpos, i;
	size_t needle_size;
	needle_size = strlen((const char*)needle);
	if (haystack_size < needle_size)
		return NULL;
	maxpos = haystack_size - needle_size;
	for (i = 0; i < maxpos; i++)
		if (!strncmp((const char*)(haystack + i),
			(const char*)needle, needle_size))
			return haystack + i;
	return NULL;
}

static const unsigned char *strchrs_n(const unsigned char *str,
	const char *chars, size_t str_len)
{
	while (str_len > 0) {
		if (strchr(chars, (char)str[0]))
			return str;
		str++;
		str_len--;
	}
	return NULL;
}

const unsigned char *find_base64_start(const unsigned char *start,
	const unsigned char *end, char **dek_info)
{
	const unsigned char *startaddr;
	const unsigned char *dekline;
	size_t remaining = end - start;
	startaddr = base64_blob_start(start, &remaining);
	if (!startaddr) {
		error("Malformed PEM: No BASE64 section found");
		return NULL;
	}
	dekline = strstr_n(start, (const unsigned char*)"DEK-Info: ",
		startaddr - start);
	if (!dekline)
		*dek_info = NULL;
	else {
		const unsigned char *term;
		const unsigned char *dekstart = dekline + 10;
		while (dekstart < startaddr && (*dekstart == ' ' ||
			*dekstart == '\t'))
			dekstart++;

		term = strchrs_n(dekstart, "\r\n", startaddr - dekstart);
		if(!term) {
			error("Malformed PEM: No BASE64 section found");
			return NULL;
		}
		*dek_info = xstrndup((const char*)dekstart, term - dekstart);
	}
	return startaddr;
}

size_t decode_asn1_length_tag(const unsigned char **file, size_t *left)
{
	if (*left < 1)
		goto eof;

	if (**file < 0x80) {
		(*left)--;
		(*file)++;
		return (*file)[-1];
	} else if (**file == 0x80) {
		error("Malformed PEM: Bad length tag");
		*file = NULL;
		return 0;
	} else if (**file == 0x81) {
		if (*left < 2)
			goto eof;
		*left -= 2;
		(*file) += 2;
		return (*file)[-1];
	} else if (**file == 0x82) {
		if (*left < 3)
			goto eof;
		*left -= 3;
		(*file) += 3;
		return (((size_t)(*file)[-2]) << 8) |
			((size_t)(*file)[-1]);
	} else if (**file == 0x83) {
		if (*left < 4)
			goto eof;
		*left -= 4;
		(*file) += 4;
		return (((size_t)(*file)[-3]) << 16) |
			(((size_t)(*file)[-2]) << 8) |
			((size_t)(*file)[-1]);
	} else {
		error("Unreadable PEM: Size too large");
		*file = NULL;
		return 0;
	}
eof:
	error("Malformed PEM: Expected length tag, got EOF");
	*file = NULL;
	return 0;
}

size_t asn1_integers_size(const unsigned char *seq, size_t seq_len,
	int low, int high, size_t *k2size)
{
	int component = 0;
	size_t size = 0;
	for (component = 0; component < high; component++) {
		size_t clen = 0;
		if (seq_len < 1) {
			error("Malformed PEM: Expected tag, got EOF");
			return 0;
		}
		if (*seq != 0x02) {
			error("Malformed PEM: Expected INTEGER");
			return 0;
		}
		seq++;
		seq_len--;
		clen = decode_asn1_length_tag(&seq, &seq_len);
		if (!seq)
			return 0;
		if (seq_len < clen) {
			error("Malformed PEM: Truncated integer");
			return 0;
		}
		seq += clen;
		seq_len -= clen;
		if (component == 2 && k2size)
			*k2size = (4 + clen);
		if (component >= low) {
			size += (4 + clen);
		}
	}
	return size;
}

unsigned char *handle_asn1_sequence(const unsigned char *seq, size_t seq_len,
	size_t *keysize, int keytype)
{
	int low;
	int high;
	size_t kslow;
	size_t toffset = 0;
	size_t foffset = 0;
	int component = 0;
	size_t offset = 0;
	size_t k2size = 0;
	unsigned char *ret = NULL;

	if (keytype == 1) {
		low = 1;
		high = 4;
		kslow = 11;
	} else if (keytype == 2) {
		low = 1;
		high = 6;
		kslow = 11;
	} else {
		error("Unknown PEM key type");
		return NULL;
	}
	*keysize = kslow + asn1_integers_size(seq, seq_len, low, high,
		&k2size);
	if (*keysize <= kslow)
		return NULL;
	ret = xmalloc(*keysize);

	if (keytype == 1) {
		ret[0] = 0;
		ret[1] = 0;
		ret[2] = 0;
		ret[3] = 7;
		memcpy(ret + 4, "ssh-rsa", 7);
		offset = 11;
	} else if (keytype == 2) {
		ret[0] = 0;
		ret[1] = 0;
		ret[2] = 0;
		ret[3] = 7;
		memcpy(ret + 4, "ssh-dss", 7);
		offset = 11;
	}

	/*
	 * Hack warning: The e and n are in "wrong" order in PEM
	 * RSA keys, so swap those.
	 */
	for (component = 0; component < high; component++) {
		size_t clen = 0;
		seq++;
		seq_len--;
		clen = decode_asn1_length_tag(&seq, &seq_len);
		if(component >= low) {
			if (keytype == 1 && component == 1) {
				toffset = offset;
				foffset = offset + 4 + clen;
				offset += k2size;
			}
			ret[offset + 0] = (unsigned char)(clen >> 24);
			ret[offset + 1] = (unsigned char)(clen >> 16);
			ret[offset + 2] = (unsigned char)(clen >> 8);
			ret[offset + 3] = (unsigned char)(clen);
			memcpy(ret + offset + 4, seq, clen);
			offset += (4 + clen);
			if (keytype == 1 && component == 1)
				offset = toffset;
			if (keytype == 1 && component == 2)
				offset = foffset + k2size;
		}
		seq += clen;
		seq_len -= clen;
	}

	return ret;
}


#define RSA_START_MARKER "-----BEGIN RSA PRIVATE KEY-----"
#define DSA_START_MARKER "-----BEGIN DSA PRIVATE KEY-----"
#define START_MARKER_LEN 31
#define RSA_END_MARKER "-----END RSA PRIVATE KEY-----"
#define DSA_END_MARKER "-----END DSA PRIVATE KEY-----"

unsigned char *pem_decode(const unsigned char *file, size_t filesize,
	const char *keyname, size_t *keysize)
{
	int type = 0;
	const unsigned char *start;
	const unsigned char *end;
	unsigned char *binary;
	unsigned char *ret;
	const unsigned char *binary_c;
	size_t binary_c_size;
	size_t binary_size;
	size_t length;
	char *dek_info;

	while(filesize > 0 && (*file == '\n' || *file == '\r')) {
		filesize--;
		file++;
	}

	if (filesize < 64)
		return NULL;

	if (!strncmp((const char*)file, RSA_START_MARKER "\r",
		START_MARKER_LEN + 1)) {
		start = file + START_MARKER_LEN + 1;
		type = 1;
	} else if (!strncmp((const char*)file,
		DSA_START_MARKER "\r", START_MARKER_LEN + 1)) {
		start = file + START_MARKER_LEN + 1;
		type = 2;
	} else if (!strncmp((const char*)file,
		RSA_START_MARKER "\n", START_MARKER_LEN + 1)) {
		start = file + START_MARKER_LEN + 1;
		type = 1;
	} else if (!strncmp((const char*)file,
		DSA_START_MARKER "\n", START_MARKER_LEN + 1)) {
		start = file + START_MARKER_LEN + 1;
		type = 2;
	} else {
		return NULL;
	}

	if(type == 1) {
		end = strstr_n(file, (const unsigned char*)
			"\r" RSA_END_MARKER, filesize);
		if (end)
			goto ok;
		end = strstr_n(file, (const unsigned char*)
			"\n" RSA_END_MARKER, filesize);
		if (end)
			goto ok;
		error("Malformed PEM: No end marker (type 1)");
		return NULL;
	} else if(type == 2) {
		end = strstr_n(file, (const unsigned char*)
			"\r" DSA_END_MARKER, filesize);
		if (end)
			goto ok;
		end = strstr_n(file, (const unsigned char*)
			"\n" DSA_END_MARKER, filesize);
		if (end)
			goto ok;
		error("Malformed PEM: No end marker (type 2)");
		return NULL;
	} else {
		error("Unknown PEM key type");
		return NULL;
	}
ok:
	start = find_base64_start(start, end, &dek_info);
	binary = decode_base64_chunk(start, end - start, &binary_size);
	if (!binary) {
		free(dek_info);
		return NULL;
	}
	if (dek_info) {
		unsigned char *decrypted;
		size_t decrypted_len;
		decrypted = decrypt_key(binary, binary_size, dek_info,
			keyname, &decrypted_len);
		free(dek_info);
		free(binary);
		if (!decrypted)
			return NULL;
		binary = decrypted;
		binary_size = decrypted_len;
	}
	if (binary_size < 1)
		goto invalid_seq;
	if (binary[0] != 0x30) {
		error("Malformed PEM: First element must be SEQUENCE");
		goto error_decoded;
	}
	binary_c = binary + 1;
	binary_c_size = binary_size -1;
	length = decode_asn1_length_tag(&binary_c, &binary_c_size);
	if (!binary_c) {
		error("Malformed PEM: Bad length for SEQUENCE");
		goto error_decoded;
	}
	if (length < binary_c_size) {
		error("Malformed PEM: Garbage after end");
		goto error_decoded;
	} else if (length > binary_c_size) {
		error("Malformed PEM: SEQUENCE overflows file");
		goto error_decoded;
	}
	ret = handle_asn1_sequence(binary_c, binary_c_size, keysize, type);
	free(binary);
	return ret;
invalid_seq:
	error("Malformed PEM: No valid sequence");
error_decoded:
	free(binary);
	return NULL;
}
