/*
 * Copyright (C) Ilari Liusvaara 2009-2010
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */
#include "hostkey.h"
#include "home.h"
#include <stdio.h>
#include <limits.h>
#include <string.h>
#include <gnutls/openpgp.h>
#include "git-compat-util.h"

/* Be ready in case some joker decides to use 1024 bit hash as fingerprint. */
#define KEYBUF 129

void check_hostkey(gnutls_session_t session, const char *hostname)
{
	const gnutls_datum_t *certificate = NULL;
	unsigned int cert_size = 0;
	gnutls_openpgp_crt_t cert;
	int s;
	unsigned int vout;
	size_t vout2;
	unsigned char key[KEYBUF] = {0};
	char keydec[2 * KEYBUF + 1] = {0};

	certificate = gnutls_certificate_get_peers(session, &cert_size);
	if (!certificate)
		die("Server didn't send a hostkey");

	s = gnutls_openpgp_crt_init(&cert);
	if (s < 0)
		die("Can't allocate space for hostkey: %s",
			gnutls_strerror(s));

	s = gnutls_openpgp_crt_import(cert, certificate,
		GNUTLS_OPENPGP_FMT_RAW);
	if (s < 0)
		die("Server sent bad hostkey: %s", gnutls_strerror(s));

	/* Defend against subkey attack. */
	s = gnutls_openpgp_crt_get_subkey_count(cert);
	if (s != 0)
		die("Server sent bad hostkey: Subkeys are not allowed");

	s = gnutls_openpgp_crt_verify_self(cert, 0, &vout);
	if (s < 0)
		die("Server sent bad hostkey: %s", gnutls_strerror(s));
	if (vout)
		die("Server sent bad hostkey: Validation failed");

	vout2 = KEYBUF;
	s = gnutls_openpgp_crt_get_fingerprint(cert, key, &vout2);
	if (s < 0)
		die("Server sent bad hostkey: %s", gnutls_strerror(s));

	gnutls_openpgp_crt_deinit(cert);

	/*
	 * Be nice to users and strip this in case it gets
	 * retained from key id calculator.
	 */
	if (!strncmp(hostname, "openpgp-", 8))
		hostname += 8;


	for (s = 0; s < vout2; s++)
		sprintf(keydec + 2 * s, "%02x", (int)key[s]);
	if (strcmp(hostname, keydec))
		goto mismatch;
	return;
mismatch:
	die("HOST KEY MISMATCH FOR HOST ('%s' vs '%s')!", hostname, keydec);
}
