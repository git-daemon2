/*
 * Copyright (C) Ilari Liusvaara 2009
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */
#ifndef _hostkey__h__included__
#define _hostkey__h__included__

#include <gnutls/gnutls.h>

void check_hostkey(gnutls_session_t session, const char *hostname);

#endif
