/*
 * Copyright (C) Ilari Liusvaara 2009-2010
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */
#include "connect.h"
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#ifndef WIN32
#include <sys/un.h>
#endif
#include <netinet/in.h>
#include <netinet/ip.h>
#include "git-compat-util.h"

#ifndef UNIX_PATH_MAX
#define UNIX_PATH_MAX 108
#endif

#define BUFFERLEN 256

extern int verbose;

static int connect_unix(const char *path)
{
#ifndef WIN32
	struct sockaddr_un saddru;
	int fd, ret, plen;

	if (strlen(path) > UNIX_PATH_MAX - 1)
		die("Unix socket path too long");

	saddru.sun_family = AF_UNIX;
	strcpy(saddru.sun_path, path);
	if (*saddru.sun_path == '@')
		*saddru.sun_path = '\0';

	fd = socket(AF_UNIX, SOCK_STREAM, 0);
	if (fd < 0)
		die_errno("Can't create socket");
	if (*path == '@')
		plen = (int)((char*)saddru.sun_path - (char*)&saddru) +
			strlen(path);
	else
		plen = (int)sizeof(saddru);
	if (verbose) {
		fprintf(stderr, "Connecting to unix socket '%s'...",
			path);
		fflush(stderr);
	}
	ret = connect(fd, (struct sockaddr*)&saddru, plen);
	if (ret < 0) {
		if (verbose)
			fprintf(stderr, "%s\n", strerror(errno));
		die_errno("Can't connect to '%s'", path);
	}
	if (verbose)
		fprintf(stderr, "Success.\n");
	return fd;
#else
	die("Unix domain sockets not supported by this build");
#endif
}

static int connect_address(const char *host, const char *port,
	int protocol, struct sockaddr *addr, int size)
{
	char address[1024];
	char nport[256];
	int fd, ret;

	fd = socket(addr->sa_family, SOCK_STREAM, protocol);
	if (fd < 0) {
		error("Can't create socket: %s", strerror(errno));
		return -1;
	}

#ifndef NO_IPV6
	int r = getnameinfo(addr, size, address, 1024, nport, 256,
		NI_NUMERICSERV | NI_NUMERICHOST);
	if (r)
		strcpy(address, "<parse error>");
#else
	strcpy(address, inet_ntoa(((struct sockaddr_in*)addr)->sin_addr));
	sprintf(nport, "%u", (unsigned)ntohs(((struct sockaddr_in*)addr)->
		sin_port));
#endif
	if (verbose) {
		fprintf(stderr, "Connecting to address '%s', port %s...",
			address, nport);
		fflush(stderr);
	}
	ret = connect(fd, addr, size);
	if (ret < 0) {
		fprintf(stderr, "%s\n", strerror(errno));
		return -1;
	}
	if (verbose)
		fprintf(stderr, "Success\n");
	return fd;
}

int connect_gethostbyname(const char *_host,
	const char *port, const char *_protocol, const char *_family)
{
	int protocol = 0;
	int family = 0;
	struct protoent* proto;

	if (!_family)
		family = 0;
	else if (!strcmp(_family, "ipv4"))
		family = AF_INET;
#ifndef NO_IPV6
	else if (!strcmp(_family, "ipv6"))
		family = AF_INET6;
#endif
	else
		die("Unknown address type '%s'", _family);

	if (verbose) {
		fprintf(stderr, "Resolving protocol '%s'...", _protocol);
		fflush(stderr);
	}

	proto = getprotobyname(_protocol);
	if (!proto) {
		if (verbose)
			fprintf(stderr, "Failed\n");
		die("Unknown protocol '%s'", _protocol);
	}
	protocol = proto->p_proto;

	if (verbose)
		fprintf(stderr, "Protocol #%i\n", protocol);

#ifndef NO_IPV6
	struct addrinfo hints;
	struct addrinfo *returned;
	int fd, ret;

	memset(&hints, 0, sizeof(hints));
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_family = family;
	hints.ai_protocol = protocol;

	if (verbose) {
		fprintf(stderr, "Resolving host '%s', port '%s'...",
			_host, port);
		fflush(stderr);
	}

	ret = getaddrinfo(_host, port, &hints, &returned);
	if (ret) {
		if (verbose)
			fprintf(stderr, "%s\n", gai_strerror(ret));
		die("Can't resolve address: %s", gai_strerror(ret));
	}

	if (verbose) {
		fprintf(stderr, "Success\n");
	}

	while (returned) {
		fd = connect_address(_host, port, returned->ai_protocol,
			returned->ai_addr, returned->ai_addrlen);

		if (fd >= 0)
			goto out;
		returned = returned->ai_next;
	}

	die("Can't connect to host %s", _host);

out:
	freeaddrinfo(returned);
	return fd;
#else
	struct hostent *host;
	int fd;
	static struct sockaddr_in saddr4;
	int _port;
	unsigned long _port2;
	struct servent* serv;
	char* end;

	if (verbose) {
		fprintf(stderr, "Resolving service '%s/%s'...", port,
			_protocol);
		fflush(stderr);
	}

	_port2 = strtoul(port, &end, 10);
	serv = getservbyname(port, _protocol);
	if (!serv && (_port2 < 1 || _port2 > 65535 || *end)) {
		if (verbose)
			fprintf(stderr, "Not found\n");
		die("No such port or service '%s/%s'", port,
			_protocol);
	} else if (serv)
		_port = serv->s_port;
	else
		_port = (int)_port2;

	if (verbose)
		fprintf(stderr, "Port %u\n", (unsigned)_port);

	if (verbose) {
		fprintf(stderr, "Resolving host '%s'...", _host);
		fflush(stderr);
	}

	host = gethostbyname(_host);
	if (!host || !host->h_addr) {
		if (verbose)
			fprintf(stderr, "Failed\n");
		die("Can't find host");
	}

	if (verbose)
		fprintf(stderr, "Success\n");

	if (host->h_addrtype != AF_INET)
		die("Can't handle address type in result");

next_address:
	memset(&saddr4, 0, sizeof(saddr4));
	saddr4.sin_family = AF_INET;
	memcpy(&saddr4.sin_addr, host->h_addr_list[0], 4);
	saddr4.sin_port = htons(_port);
	fd = connect_address(_host, port, 0,
		(struct sockaddr*)&saddr4,
		sizeof(struct sockaddr_in));

	if (fd >= 0)
		goto out;

	host->h_addr_list++;
	if (host->h_addr_list[0])
		goto next_address;

	die("Can't connect to host %s", _host);
out:
	return fd;
#endif
}

int connect_host(const char *_host, const char *port, const char *tproto,
	const char *addrspace)
{
	char *hostcopy;

	if (_host[0] == '/' || (_host[0] == '@' && _host[1] == '/') ||
		(addrspace && !strcmp(addrspace, "unix")))
		return connect_unix(_host);

	hostcopy = xmalloc(strlen(_host) + 1);
	strcpy(hostcopy, _host);

	return connect_gethostbyname(hostcopy, port, tproto, addrspace);
}
