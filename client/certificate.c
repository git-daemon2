/*
 * Copyright (C) Ilari Liusvaara 2009-2010
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */
#include "certificate.h"
#include "cbuffer.h"
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <errno.h>
#include <signal.h>
#include "git-compat-util.h"
#ifndef BUILD_SELFSTANDING
#include "run-command.h"
#endif

#define CERT_MAX 65536

static long read_short(struct cbuffer *buffer)
{
	unsigned char x[2];
	if (cbuffer_read(buffer, x, 2) < 0)
		return -1;

	return ((long)x[0] << 8) | ((long)x[1]);
}


static int unseal_cert(struct cbuffer *sealed, struct cbuffer *unsealed,
	const char *unsealer)
{
	struct child_process child;
	char **argv;
	char *unsealer_copy;
	int splits = 0;
	int escape = 0;
	int ridx, widx, tidx;
	const char *i;

	signal(SIGPIPE, SIG_IGN);

	for (i = unsealer; *i; i++) {
		if (escape)
			escape = 0;
		else if (*i == '\\')
			escape = 1;
		else if (*i == ' ')
			splits++;
	}

	argv = xmalloc((splits + 2) * sizeof(char*));
	argv[splits + 1] = NULL;

	unsealer_copy = xstrdup(unsealer);
	argv[0] = unsealer_copy;

	ridx = 0;
	widx = 0;
	tidx = 1;
	escape = 0;
	while (unsealer_copy[ridx]) {
		if (escape) {
			escape = 0;
			unsealer_copy[widx++] = unsealer_copy[ridx++];
		} else if (unsealer_copy[ridx] == '\\') {
			ridx++;
			escape = 1;
		} else if (unsealer_copy[ridx] == ' ') {
			unsealer_copy[widx++] = '\0';
			argv[tidx++] = unsealer_copy + widx;
			ridx++;
		} else
			unsealer_copy[widx++] = unsealer_copy[ridx++];
	}
	unsealer_copy[widx] = '\0';

	memset(&child, 0, sizeof(child));
	child.argv = (const char**)argv;
	child.in = -1;
	child.out = -1;
	child.err = 0;
	if (start_command(&child))
		die("Running keypair unsealer command failed");

	while (1) {
		int bound;
		fd_set rf;
		fd_set wf;
		int r;

		FD_ZERO(&rf);
		FD_ZERO(&wf);
		FD_SET(child.out, &rf);
		if (cbuffer_used(sealed))
			FD_SET(child.in, &wf);
		else
			close(child.in);

		if (cbuffer_used(sealed))
			bound = ((child.out > child.in) ? child.out :
				child.in) + 1;
		else
			bound = child.out + 1;

		r = select(bound, &rf, &wf, NULL, NULL);
		if (r < 0 && r != EINTR)
			die_errno("Select failed");
		if (r < 0) {
			FD_ZERO(&rf);
			FD_ZERO(&wf);
			perror("select");
		}

		if (FD_ISSET(child.out, &rf)) {
			r = cbuffer_read_fd(unsealed, child.out);
			if (r < 0 && errno != EINTR && errno != EAGAIN)
				die_errno("Read from unsealer failed");
			if (r < 0 && errno == EAGAIN)
				if (!cbuffer_free(unsealed))
					die("Keypair too big");
			if (r < 0)
				perror("read");
			if (r == 0)
				break;
		}

		if (FD_ISSET(child.in, &wf)) {
			r = cbuffer_write_fd(sealed, child.in);
			if (r < 0 && errno == EPIPE)
				die("Unsealer exited unexpectedly");
			if (r < 0 && errno != EINTR && errno != EAGAIN)
				die_errno("Write to unsealer failed");
			if (r < 0)
				perror("write");
		}
	}

	if (finish_command(&child))
		die("Keypair unsealer command failed");

	return 0;
}


struct certificate parse_certificate(const char *name, int *errorcode)
{
	struct cbuffer *sealed = NULL;
	struct cbuffer *unsealed = NULL;
	unsigned char sealed_buf[CERT_MAX];
	unsigned char unsealed_buf[CERT_MAX];
	struct certificate cert;
	int fd;
	unsigned char head[10];
	long tmp;

	*errorcode = CERTERR_OK;
	cert.public_key.data = NULL;
	cert.public_key.size = 0;
	cert.private_key.data = NULL;
	cert.private_key.size = 0;

	sealed = cbuffer_create(sealed_buf, CERT_MAX);
	if (!sealed)
		die("Ran out of memory");

	unsealed = cbuffer_create(unsealed_buf, CERT_MAX);
	if (!unsealed)
		die("Ran out of memory");

	fd = open(name, O_RDONLY);
	if (fd < 0) {
		if (errno == ENOENT)
			*errorcode = CERTERR_NOCERT;
		else
			*errorcode = CERTERR_CANTREAD;
		goto out_unsealed;
	}

	while (1) {
		ssize_t r = cbuffer_read_fd(sealed, fd);
		if (r == 0)
			break;
		if (r < 0 && errno == EAGAIN) {
			if (!cbuffer_free(sealed)) {
				*errorcode = CERTERR_TOOBIG;
				goto out_close;
			}
		} else if (r < 0 && errno != EINTR) {
			*errorcode = CERTERR_CANTREAD;
			goto out_close;
		}
	}

	head[9] = 0;
	if (cbuffer_read(sealed, head, 9) < 0) {
		*errorcode = CERTERR_INVALID;
		goto out_close;
	}
	if (strcmp((char*)head, "GITSSCERT") &&
		strcmp((char*)head, "GITSUCERT")) {
		*errorcode = CERTERR_INVALID;
		goto out_close;
	}

	if (!strcmp((char*)head, "GITSSCERT")) {
		/* Sealed certificate. */
		char *unsealer;
		int s;
		tmp = read_short(sealed);
		if (tmp <= 0) {
			*errorcode = CERTERR_INVALID;
			goto out_close;
		}
		unsealer = xmalloc(tmp + 1);
		unsealer[tmp] = '\0';
		if (cbuffer_read(sealed, (unsigned char*)unsealer, tmp) < 0) {
			free(unsealer);
			*errorcode = CERTERR_INVALID;
			goto out_close;
		}
		s = unseal_cert(sealed, unsealed, unsealer);
		free(unsealer);
		if (s < 0) {
			*errorcode = s;
			goto out_close;
		}
	} else {
		/* Unsealed certificate. */
		cbuffer_move_nolimit(unsealed, sealed);
	}

	cert.private_key.data = NULL;
	cert.public_key.data = NULL;

	tmp = read_short(unsealed);
	if (tmp < 0) {
		*errorcode = CERTERR_INVALID;
		goto out_close;
	}
	cert.private_key.size = tmp;
	cert.private_key.data = (unsigned char*)gnutls_malloc(tmp);
	if (!cert.private_key.data)
		die("Ran out of memory");

	if (cbuffer_read(unsealed, cert.private_key.data,
		cert.private_key.size) < 0) {
		*errorcode = CERTERR_INVALID;
		goto out_private;
	}

	tmp = read_short(unsealed);
	if (tmp < 0) {
		*errorcode = CERTERR_INVALID;
		goto out_close;
	}
	cert.public_key.size = tmp;
	cert.public_key.data = (unsigned char*)gnutls_malloc(tmp);
	if (!cert.public_key.data)
		die("Ran out of memory");

	if (cbuffer_read(unsealed, cert.public_key.data,
		cert.public_key.size) < 0) {
		*errorcode = CERTERR_INVALID;
		goto out_public;
	}

	if (cbuffer_used(unsealed)) {
		*errorcode = CERTERR_INVALID;
		goto out_public;
	}

	goto out_close;

out_public:
	gnutls_free(cert.private_key.data);
out_private:
	gnutls_free(cert.private_key.data);
out_close:
	close(fd);
out_unsealed:
	cbuffer_destroy(unsealed);
	cbuffer_destroy(sealed);
	return cert;
}

const char *cert_parse_strerr(int errcode)
{
	switch(errcode) {
	case CERTERR_OK:
		return "Success";
	case CERTERR_NOCERT:
		return "No such keypair";
	case CERTERR_INVALID:
		return "Keypair file corrupt";
	case CERTERR_CANTREAD:
		return "Can't read keypair file";
	case CERTERR_TOOBIG:
		return "Keypair too big";
	}
	return "<Unknown error>";
}
