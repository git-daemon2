/*
 * Copyright (C) Ilari Liusvaara 2009
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */
#ifndef _keypairs__h__included__
#define _keypairs__h__included__

#include <gnutls/openpgp.h>

int select_keypair_int(gnutls_certificate_credentials_t creds,
	const char *username);

#endif
