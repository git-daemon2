/*
 * Copyright (C) Ilari Liusvaara 2009-2010
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */
#include "keypairs.h"
#include "home.h"
#include "certificate.h"
#include <stdio.h>
#include <limits.h>
#include <gnutls/openpgp.h>
#include <errno.h>
#include "git-compat-util.h"

int select_keypair_int(gnutls_certificate_credentials_t creds,
	const char *username)
{
	char keypath[PATH_MAX + 1];
	char *keypath2;
	struct certificate cert;
	int r;

	r = snprintf(keypath, PATH_MAX + 1, "$XDG_CONFIG_HOME/gits/keys/%s",
		username);
	if (r < 0 || r > PATH_MAX) {
		die("Username too long");
	}

	keypath2 = expand_path(keypath);
	cert = parse_certificate(keypath2, &r);
	if (r) {
		if (r == CERTERR_NOCERT)
			return -1;
		if (r == CERTERR_CANTREAD)
			die_errno("Can't read keypair");
		else
			die("Can't read keypair: %s",
				cert_parse_strerr(r));
	}

	r = gnutls_certificate_set_openpgp_keyring_mem(creds,
		cert.public_key.data, cert.public_key.size,
		GNUTLS_OPENPGP_FMT_RAW);
	if (r < 0)
		die("Can't load public key: %s", gnutls_strerror(r));

	r = gnutls_certificate_set_openpgp_key_mem(creds, &cert.public_key,
		&cert.private_key, GNUTLS_OPENPGP_FMT_RAW);
	if (r < 0)
		die("Can't load keypair: %s", gnutls_strerror(r));

	free(keypath2);
	return 0;
}
