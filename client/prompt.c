/*
 * Copyright (C) Ilari Liusvaara 2009-2010
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */
#include "prompt.h"
#include <termios.h>
#include <signal.h>
#include <unistd.h>
#include <stdio.h>
#include <fcntl.h>
#include <string.h>
#include "git-compat-util.h"

static int tmpfd;

static void sigint(int x);

void echo_off(int fd)
{
	struct termios t;

	if (tcgetattr(fd, &t) < 0)
		die_errno("Can't read terminal settings");

	t.c_lflag &= ~ECHO;

	tmpfd = fd;
	signal(SIGINT, sigint);

	if (tcsetattr(fd, TCSANOW, &t) < 0)
		die_errno("Can't write terminal settings");
}

void echo_on(int fd)
{
	struct termios t;

	if (tcgetattr(fd, &t) < 0)
		die_errno("Can't read terminal settings");

	t.c_lflag |= ECHO;

	if (tcsetattr(fd, TCSANOW, &t) < 0)
		die_errno("Can't write terminal settings");

	signal(SIGINT, SIG_DFL);
}

static void sigint(int x)
{
	echo_on(tmpfd);
	exit(1);
}

#define PROMPTBUF 8192

char *prompt_string(const char *prompt, int without_echo)
{
	char ansbuf[8192];
	char *ans;
	int fd;
	FILE* tty;

	fd = open("/dev/tty", O_RDWR);
	if (fd < 0)
		die_errno("Can't open /dev/tty for password prompt");

	tty = xfdopen(fd, "r+");

	fprintf(tty, "%s: ", prompt);
	fflush(tty);
	if (without_echo)
		echo_off(fd);

	if (!fgets(ansbuf, 8190, tty)) {
		if (without_echo)
			echo_on(fd);
		die("Can't read answer");
	}

	if (without_echo) {
		fprintf(tty, "\n");
		echo_on(fd);
	}

	if (*ansbuf && ansbuf[strlen(ansbuf) - 1] == '\n')
		ansbuf[strlen(ansbuf) - 1] = '\0';

	fclose(tty);

	ans = xstrdup(ansbuf);
	return ans;
}
