/*
 * Copyright (C) Ilari Liusvaara 2009
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */
#ifndef _prompt__h__included__
#define _prompt__h__included__

/* Turn terminal echo on specified fd off. */
void echo_off(int fd);
/* Turn terminal echo on specified fd on. */
void echo_on(int fd);
/* Prompt string from user and return mallocced copy of it. */
char *prompt_string(const char *prompt, int without_echo);

#endif
