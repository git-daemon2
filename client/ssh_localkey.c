/*
 * Copyright (C) Ilari Liusvaara 2010
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include "pem.h"
#include "base64.h"
#include "home.h"
#include <gcrypt.h>
#include "git-compat-util.h"

static int validate_key(const unsigned char *key, size_t keysize,
	size_t *offsets, size_t *lengths, int components)
{
	size_t offset = 0;
	int c;

	for(c = 0; c < components; c++) {
		size_t len = 0;
		if (offset + 4 < offset || offset + 4 > keysize)
			return -1;
		offsets[c] = offset + 4;
		len |= ((size_t)key[offset] << 24);
		len |= ((size_t)key[offset + 1] << 16);
		len |= ((size_t)key[offset + 2] << 8);
		len |= ((size_t)key[offset + 3]);
		lengths[c] = len;
		if (offset + 4 + len < offset + 4 ||
			offset + 4 + len > keysize)
			return -1;
		offset += (4 + len);
	}
	if (offset != keysize)
		return -1;
	return 0;
}

typedef void* voidptr;

static unsigned char *sign_rsa(const unsigned char *key, size_t keysize,
	const unsigned char *challenge, size_t challenge_size,
	size_t *signature_size)
{
	unsigned char *ret = NULL;
	size_t offsets[4];
	size_t lengths[4];
	gcry_mpi_t rsa_e;
	gcry_mpi_t rsa_n;
	gcry_mpi_t rsa_d;
	gcry_error_t err;
	gcry_sexp_t _key;
	gcry_sexp_t tosign;
	gcry_sexp_t signature;
	gcry_sexp_t sval;
	gcry_sexp_t rsas_s;
	const unsigned char *rsa_s;
	size_t rsa_s_len;
	int hashalgo = GCRY_MD_SHA256;
	const char *algo = "sha256";
	int hashlen = 32;
	unsigned char hashbuffer[128];
	voidptr params[3];
	if (validate_key(key, keysize, offsets, lengths, 4) < 0) {
		error("Bad RSA private key");
		return NULL;
	}
	err = gcry_mpi_scan(&rsa_e, GCRYMPI_FMT_STD, key + offsets[1],
		lengths[1], NULL);
	if (err) {
		error("Can't read RSA e: %s/%s", gcry_strsource(err),
			gcry_strerror(err));
		goto out;
	}
	err = gcry_mpi_scan(&rsa_n, GCRYMPI_FMT_STD, key + offsets[2],
		lengths[2], NULL);
	if (err) {
		error("Can't read RSA n: %s/%s", gcry_strsource(err),
			gcry_strerror(err));
		goto out_e;
	}
	err = gcry_mpi_scan(&rsa_d, GCRYMPI_FMT_STD, key + offsets[3],
		lengths[3], NULL);
	if (err) {
		error("Can't read RSA d: %s/%s", gcry_strsource(err),
			gcry_strerror(err));
		goto out_n;
	}
	params[0] = &rsa_n;
	params[1] = &rsa_e;
	params[2] = &rsa_d;
	err = gcry_sexp_build_array(&_key, NULL, "(private-key (rsa (n %m) (e %m) (d %m)))",
		params);
	if (err) {
		error("Can't build key: %s/%s", gcry_strsource(err),
			gcry_strerror(err));
		goto out_d;
	}

	gcry_md_hash_buffer(hashalgo, hashbuffer, challenge,
		challenge_size);

	err = gcry_sexp_build(&tosign, NULL, "(data (flags pkcs1) (hash %s %b))",
		algo, hashlen, hashbuffer);
	if (err) {
		error("Can't build data to sign: %s/%s", gcry_strsource(err),
			gcry_strerror(err));
		goto out_key;
	}

	err = gcry_pk_sign(&signature, tosign, _key);
	if (err) {
		error("Can't sign data: %s/%s", gcry_strsource(err),
			gcry_strerror(err));
		goto out_tosign;
	}

	sval = gcry_sexp_nth(signature, 1);
	if(!sval) {
		error("DSA signature output faulty");
		goto out_signature;
	}
	rsas_s = gcry_sexp_nth(sval, 1);
	if(!rsas_s) {
		error("RSA signature output faulty (no s)");
		goto out_sval;
	}
	rsa_s = (const unsigned char*)gcry_sexp_nth_data(rsas_s, 1, &rsa_s_len);
	if(!rsa_s) {
		error("RSA signature output faulty (no real s)");
		goto out_s;
	}

	*signature_size = 4 + rsa_s_len;
	ret = xmalloc(*signature_size);
	encode_uint32(ret, rsa_s_len);
	memcpy(ret + 4, rsa_s, rsa_s_len);

out_s:
	gcry_sexp_release(rsas_s);
out_sval:
	gcry_sexp_release(sval);
out_signature:
	gcry_sexp_release(signature);
out_tosign:
	gcry_sexp_release(tosign);
out_key:
	gcry_sexp_release(_key);
out_d:
	gcry_mpi_release(rsa_d);
out_n:
	gcry_mpi_release(rsa_n);
out_e:
	gcry_mpi_release(rsa_e);
out:
	return ret;
}

static unsigned char *sign_dsa(const unsigned char *key, size_t keysize,
	const unsigned char *challenge, size_t challenge_size,
	size_t *signature_size)
{
	unsigned char *ret = NULL;
	size_t offsets[6];
	size_t lengths[6];
	gcry_mpi_t dsa_p;
	gcry_mpi_t dsa_q;
	gcry_mpi_t dsa_g;
	gcry_mpi_t dsa_y;
	gcry_mpi_t dsa_x;
	gcry_mpi_t dsa_e;
	int hashalgo;
	const char *algo;
	int hashlen;
	unsigned char hashbuffer[128];
	voidptr params[5];
	gcry_sexp_t _key;
	gcry_sexp_t tosign;
	gcry_sexp_t signature;
	gcry_sexp_t sval;
	gcry_sexp_t dsas_r;
	gcry_sexp_t dsas_s;
	const unsigned char *dsa_r;
	size_t dsa_r_len;
	const unsigned char *dsa_s;
	size_t dsa_s_len;
	gcry_error_t err;
	int sbits;

	if (validate_key(key, keysize, offsets, lengths, 6) < 0) {
		error("Bad DSA private key");
		return NULL;
	}

	err = gcry_mpi_scan(&dsa_p, GCRYMPI_FMT_STD, key + offsets[1],
		lengths[1], NULL);
	if (err) {
		error("Can't read DSA p: %s/%s", gcry_strsource(err),
			gcry_strerror(err));
		goto out;
	}
	err = gcry_mpi_scan(&dsa_q, GCRYMPI_FMT_STD, key + offsets[2],
		lengths[2], NULL);
	if (err) {
		error("Can't read DSA q: %s/%s", gcry_strsource(err),
			gcry_strerror(err));
		goto out_p;
	}
	err = gcry_mpi_scan(&dsa_g, GCRYMPI_FMT_STD, key + offsets[3],
		lengths[3], NULL);
	if (err) {
		error("Can't read DSA g: %s/%s", gcry_strsource(err),
			gcry_strerror(err));
		goto out_q;
	}
	err = gcry_mpi_scan(&dsa_y, GCRYMPI_FMT_STD, key + offsets[4],
		lengths[4], NULL);
	if (err) {
		error("Can't read DSA y: %s/%s", gcry_strsource(err),
			gcry_strerror(err));
		goto out_g;
	}
	err = gcry_mpi_scan(&dsa_x, GCRYMPI_FMT_STD, key + offsets[5],
		lengths[5], NULL);
	if (err) {
		error("Can't read DSA x: %s/%s", gcry_strsource(err),
			gcry_strerror(err));
		goto out_y;
	}

	params[0] = &dsa_p;
	params[1] = &dsa_q;
	params[2] = &dsa_g;
	params[3] = &dsa_y;
	params[4] = &dsa_x;
	err = gcry_sexp_build_array(&_key, NULL, "(private-key (dsa (p %m) (q %m) (g %m) (y %m) (x %m)))",
		params);
	if (err) {
		error("Can't build key: %s/%s", gcry_strsource(err),
			gcry_strerror(err));
		goto out_x;
	}

	sbits = gcry_mpi_get_nbits(dsa_q);
	if(sbits <= 160) {
		algo = "sha1";
		hashalgo = GCRY_MD_SHA1;
		hashlen = 20;
	} else if(sbits <= 224) {
		algo = "sha224";
		hashalgo = GCRY_MD_SHA224;
		hashlen = 28;
	} else if(sbits <= 256) {
		algo = "sha256";
		hashalgo = GCRY_MD_SHA256;
		hashlen = 32;
	} else if(sbits <= 384) {
		algo = "sha384";
		hashalgo = GCRY_MD_SHA384;
		hashlen = 48;
	} else if(sbits <= 512) {
		algo = "sha512";
		hashalgo = GCRY_MD_SHA512;
		hashlen = 64;
	} else {
		error("DSA parameter q out of range (2^512 or larger)");
		goto out_key;
	}

	gcry_md_hash_buffer(hashalgo, hashbuffer, challenge,
		challenge_size);
	err = gcry_mpi_scan(&dsa_e, GCRYMPI_FMT_USG, hashbuffer,
		hashlen, NULL);
	if (err) {
		error("Can't read DSA e: %s/%s", gcry_strsource(err),
			gcry_strerror(err));
		goto out_key;
	}

	err = gcry_sexp_build(&tosign, NULL, "(%m)",
		dsa_e);
	if (err) {
		error("Can't build data to sign: %s/%s", gcry_strsource(err),
			gcry_strerror(err));
		goto out_e;
	}

	err = gcry_pk_sign(&signature, tosign, _key);
	if (err) {
		error("Can't sign data: %s/%s", gcry_strsource(err),
			gcry_strerror(err));
		goto out_tosign;
	}

	sval = gcry_sexp_nth(signature, 1);
	if(!sval) {
		error("DSA signature output faulty (no sval)");
		goto out_signature;
	}
	dsas_r = gcry_sexp_nth(sval, 1);
	if(!dsas_r) {
		error("DSA signature output faulty (no r)");
		goto out_sval;
	}
	dsa_r = (const unsigned char*)gcry_sexp_nth_data(dsas_r, 1, &dsa_r_len);
	if(!dsa_r) {
		error("DSA signature output faulty (no real r)");
		goto out_r;
	}
	dsas_s = gcry_sexp_nth(sval, 2);
	if(!dsas_s) {
		error("DSA signature output faulty (no s)");
		goto out_r;
	}
	dsa_s = (const unsigned char*)gcry_sexp_nth_data(dsas_s, 1, &dsa_s_len);
	if(!dsa_s) {
		error("DSA signature output faulty (no real s)");
		goto out_s;
	}

	*signature_size = 8 + dsa_r_len + dsa_s_len;
	ret = xmalloc(*signature_size);
	encode_uint32(ret, dsa_r_len);
	memcpy(ret + 4, dsa_r, dsa_r_len);
	encode_uint32(ret + 4 + dsa_r_len, dsa_s_len);
	memcpy(ret + 8 + dsa_r_len, dsa_s, dsa_s_len);

out_s:
	gcry_sexp_release(dsas_s);
out_r:
	gcry_sexp_release(dsas_r);
out_sval:
	gcry_sexp_release(sval);
out_signature:
	gcry_sexp_release(signature);
out_tosign:
	gcry_sexp_release(tosign);
out_e:
	gcry_mpi_release(dsa_e);
out_key:
	gcry_sexp_release(_key);
out_x:
	gcry_mpi_release(dsa_x);
out_y:
	gcry_mpi_release(dsa_y);
out_g:
	gcry_mpi_release(dsa_g);
out_q:
	gcry_mpi_release(dsa_q);
out_p:
	gcry_mpi_release(dsa_p);
out:
	return ret;
}

#define MAXPATH 4096
#define MAXKEY 65536

unsigned char *sign_using_local_key(const char *keyname,
	const unsigned char *kblob, size_t kblob_size,
	const unsigned char *challenge, size_t challenge_size,
	size_t *signature_length, const char *type)
{
	FILE* filp;
	unsigned char *ret = NULL;

	char *path;
	char pathtemplate[MAXPATH];
	unsigned char key[MAXKEY];
	size_t keysize;
	int readkey_flag = 0;

	unsigned char *x;
	size_t i;

	if (strlen(keyname) > MAXPATH - 32)
		die("Key name too long");
	sprintf(pathtemplate, "~/.ssh/%s", keyname);
	path = expand_path(pathtemplate);

	/* Read the privkey file. */
	filp = fopen(path, "r");
	if (!filp) {
		error("Can't open '%s'", path);
		goto out_readfile;
	}
	keysize = fread(key, 1, MAXKEY, filp);
	if (ferror(filp)) {
		error("Can't read '%s'", path);
		goto out_readfile_handle;
	}
	if (!feof(filp)) {
		error("Keyfile '%s' too large", path);
		goto out_readfile_handle;
	}
	readkey_flag = 1;
out_readfile_handle:
	fclose(filp);
out_readfile:
	free(path);
	if (!readkey_flag) {
		error("Can't read any private key named '%s'", keyname);
		return NULL;
	}

	x = pem_decode(key, keysize, keyname, &i);
	if (!x) {
		error("Can't decode key '%s'", keyname);
		return NULL;
	}
	if (kblob_size > i || memcmp(kblob, x, kblob_size)) {
		free(x);
		error("Pubic and private keys for '%s' are inconsistent",
			keyname);
		return NULL;
	}
	if (!strcmp(type, "ssh-rsa"))
		ret = sign_rsa(x, i, challenge, challenge_size,
			signature_length);
	else if (!strcmp(type, "ssh-dss"))
		ret = sign_dsa(x, i, challenge, challenge_size,
			signature_length);
	else
		error("Key '%s' is of unknown type", keyname);
	free(x);
	return ret;
}
