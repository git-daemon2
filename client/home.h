/*
 * Copyright (C) Ilari Liusvaara 2009-2010
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */
#ifndef _home__h__included__
#define _home__h__included__

#include <sys/types.h>

/*
 * Ensure that directory specified exists. If it does not, attempt
 * to create it.
 *
 *Input:
 *	path		The directory to ensure.
 *
 *Output:
 *	Return value	0 on success, -1 on failure.
 *
 *Notes:
 *	- Path starting with $HOME is special, it is
 *	  relative to user home directory.
 *	- Paths starting with $XDG_CONFIG_HOME and $XDG_DATA_HOME
 *	  are special. Those are interpretted as relative to XDG
 *	  base dir spec directories.
 */
int ensure_directory(const char *path);

/*
 * Open the specified regular file. Leading path components are
 * automatically created if file needs to be created.
 *
 *Input:
 *	path		The file to open.
 *	flags		Flags to pass to open.
 *	mode		Mode to pass to open.
 *
 *Output:
 *	Return value	File descriptor. -1 on failure.
 *
 *Notes:
 *	- Path starting with $HOME is special, it is
 *	  relative to user home directory.
 *	- Paths starting with $XDG_CONFIG_HOME and $XDG_DATA_HOME
 *	  are special. Those are interpretted as relative to XDG
 *	  base dir spec directories.
 */
int open_create_dirs(const char *path, int flags, mode_t mode);

/*
 * Expand the specified path.
 *
 *Input:
 *	path		The path to expand.
 *
 *Output:
 *	Return value	Mallocced copy of expanded path.
 *
 *Notes:
 *	- Path starting with $HOME is special, it is
 *	  relative to user home directory.
 *	- Paths starting with $XDG_CONFIG_HOME and $XDG_DATA_HOME
 *	  are special. Those are interpretted as relative to XDG
 *	  base dir spec directories.
 */
char *expand_path(const char *path);

#endif
