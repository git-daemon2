#include "pem_decrypt.h"
#include "srp_askpass.h"
#include <string.h>
#include <gcrypt.h>
#include "git-compat-util.h"

#define BUFFERSIZE 16384
#define IVSIZE 128

unsigned char hexdecode(char c)
{
	switch(c) {
	case '0':	return 0;
	case '1':	return 1;
	case '2':	return 2;
	case '3':	return 3;
	case '4':	return 4;
	case '5':	return 5;
	case '6':	return 6;
	case '7':	return 7;
	case '8':	return 8;
	case '9':	return 9;
	case 'a':	return 10;
	case 'b':	return 11;
	case 'c':	return 12;
	case 'd':	return 13;
	case 'e':	return 14;
	case 'f':	return 15;
	case 'A':	return 10;
	case 'B':	return 11;
	case 'C':	return 12;
	case 'D':	return 13;
	case 'E':	return 14;
	case 'F':	return 15;
	default:	die("Bad hexadecimal character");
	}
	return 0;
}

unsigned char hexpairdecode(const char* hexpair)
{
	return hexdecode(hexpair[0]) * 16 + hexdecode(hexpair[1]);
}

void decode_iv(unsigned char *iv, const char *dek_tail, size_t correct_len)
{
	int i;
	if (strchr(dek_tail, ' ') != dek_tail + 2 * correct_len &&
		strlen(dek_tail) != 2 * correct_len)
		die("IV field in key encryption wrong length");

	for (i = 0; i < correct_len; i++)
		iv[i] = hexpairdecode(dek_tail + 2 * i);
}

#define MD5LEN 16
#define SALTLEN 8

void derive_key(unsigned char *key, size_t keylen, const unsigned char *iv,
	const char *passphrase)
{
	size_t passphrase_len;
	int derived;
	unsigned char* md5 = NULL;
	unsigned char md5copy[MD5LEN];
	gcry_md_hd_t handle;
	int i;

	if (gcry_md_open(&handle, GCRY_MD_MD5, 0))
		die("Can't gcry_md_open(..., GCRY_MD_MD5, ...).");

	passphrase_len = strlen(passphrase);
	for (derived = 0; derived < keylen;) {
		gcry_md_reset(handle);

		if (md5)
			gcry_md_write(handle, md5copy, MD5LEN);
		gcry_md_write(handle, passphrase, passphrase_len);
		gcry_md_write(handle, iv, SALTLEN);

		md5 = gcry_md_read(handle, 0);
		memcpy(md5copy, md5, MD5LEN);
		for (i = 0; i < 16 && derived < keylen; i++) {
			key[derived++] = md5[i];
		}
	}
	gcry_md_close(handle);
}

unsigned char *decrypt_key(const unsigned char *cipher, size_t cipher_size,
	const char *dek_info, const char *keyname, size_t *decrypted_len)
{
	char *passphrase;
	static char *stored_passphrase = NULL;
	static char lastkeyname[BUFFERSIZE];
	unsigned char iv[IVSIZE];
	unsigned char key[IVSIZE];
	size_t ivlen;
	size_t keylen;
	int algo;
	int mode;
	size_t i;
	int cipher_inited = 0;
	gcry_cipher_hd_t cipherh;
	unsigned char *ret = NULL;

	if (strlen(keyname) + 1 > BUFFERSIZE)
		die("Keyname insanely long");

	if (stored_passphrase && !strcmp(keyname, lastkeyname)) {
		passphrase = stored_passphrase;
		goto reuse_passphrase;
	}

	passphrase = get_ssh_password(keyname);
	stored_passphrase = passphrase;
reuse_passphrase:
	if (!prefixcmp(dek_info, "DES-EDE3-CBC,")) {
		/* 3DES-CBC */
		ivlen = 8;
		keylen = 24;
		algo = GCRY_CIPHER_3DES;
		mode = GCRY_CIPHER_MODE_CBC;
	} else if (!prefixcmp(dek_info, "DES-CBC,")) {
		/* DES-CBC (INSECURE!!!) */
		ivlen = 8;
		keylen = 8;
		algo = GCRY_CIPHER_DES;
		mode = GCRY_CIPHER_MODE_CBC;
	} else if (!prefixcmp(dek_info, "AES-128-CBC,")) {
		/* AES-128-CBC */
		ivlen = 16;
		keylen = 16;
		algo = GCRY_CIPHER_AES128;
		mode = GCRY_CIPHER_MODE_CBC;
	} else if (!prefixcmp(dek_info, "AES-192-CBC,")) {
		/* AES-192-CBC */
		ivlen = 16;
		keylen = 24;
		algo = GCRY_CIPHER_AES192;
		mode = GCRY_CIPHER_MODE_CBC;
	} else if (!prefixcmp(dek_info, "AES-256-CBC,")) {
		/* AES-256-CBC */
		ivlen = 16;
		keylen = 32;
		algo = GCRY_CIPHER_AES256;
		mode = GCRY_CIPHER_MODE_CBC;
	} else {
		error("Key encryption type not supported (%s)", dek_info);
		goto error_exit;
	}
	decode_iv(iv, strchr(dek_info, ',') + 1, ivlen);
	derive_key(key, keylen, iv, passphrase);

	if (gcry_cipher_open(&cipherh, algo, mode, 0)) {
		error("gcry_cipher_open() failed (DEK=%s; unsupported algorithm?)", dek_info);
		goto error_exit;
	}
	cipher_inited = 1;

	if (gcry_cipher_setkey(cipherh, key, keylen)) {
		error("gcry_cipher_setkey() failed (DEK=%s)", dek_info);
		goto error_exit;
	}

	if (gcry_cipher_setiv(cipherh, iv, ivlen)) {
		error("gcry_cipher_setiv() failed (DEK=%s)", dek_info);
		goto error_exit;
	}

	ret = xmalloc(cipher_size);
	if (gcry_cipher_decrypt(cipherh, ret, cipher_size, cipher,
		cipher_size)) {
		error("gcry_cipher_decrypt() failed (DEK=%s, wrong passphrase?)", dek_info);
		goto error_exit;
	}

	*decrypted_len = cipher_size - ret[cipher_size - 1];
	for(i = *decrypted_len; i < cipher_size; i++) {
		if(ret[i] != ret[cipher_size - 1]) {
			error("Incorrect passphrase or key corrupt");
			goto error_exit;
		}
	}

	strcpy(lastkeyname, keyname);
	stored_passphrase = passphrase;
	gcry_cipher_close(cipherh);
	return ret;
error_exit:
	if (cipher_inited)
		gcry_cipher_close(cipherh);
	if (stored_passphrase)
		free(stored_passphrase);
	if (ret)
		free(ret);
	stored_passphrase = NULL;
	return NULL;
}
