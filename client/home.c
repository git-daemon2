/*
 * Copyright (C) Ilari Liusvaara 2009-2010
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */
#include "home.h"
#include <string.h>
#include <stdlib.h>
#include <pwd.h>
#include <unistd.h>
#include <sys/stat.h>
#include <errno.h>
#include <fcntl.h>
#include "git-compat-util.h"

static const char *get_home()
{
	static char *home = NULL;
	const char *tmpans;
#ifndef WIN32
	struct passwd *user;
#endif

	if (home)
		return home;

	if (getenv("HOME")) {
		tmpans = getenv("HOME");
		goto got_it;
	}

#ifndef WIN32
	user = getpwuid(getuid());
	if (user && user->pw_dir) {
		tmpans = user->pw_dir;
		goto got_it;
	}
#endif

	die("Can't obtain home directory of current user");
got_it:
	home = xstrdup(tmpans);
	return home;
}

static char *replace_variable(char *path, const char *value)
{
	char *slash;
	char *oldpath = path;

	slash = strchr(path, '/');
	if (slash) {
		size_t len;
		len = strlen(path) - (slash - path) +
			strlen(value);
		path = xmalloc(len + 1);
		if (value[strlen(value) - 1] == '/')
			sprintf(path, "%s%s", value, slash + 1);
		else
			sprintf(path, "%s%s", value, slash);
	} else {
		size_t len;
		len = strlen(value);
		path = xstrdup(value);
		if (path[len - 1] == '/')
			path[len - 1] = '\0';
	}
	free(oldpath);
	return path;
}

int check_component(const char *path, size_t len)
{
	struct stat s;
	int r;
	char *tmp;
	tmp = xstrndup(path, len);

	r = stat(tmp, &s);
	if (r < 0 && errno != ENOENT) {
		error("Creating '%s', unable to stat '%s': %s",
			path, tmp, strerror(errno));
		return -1;
	} else if (r < 0) {
		/* Attempt to create. */
		int r;
		r = mkdir(tmp, 0700);
		if (r >= 0)
			goto end;
		error("Creating '%s', unable to create '%s': %s",
			path, tmp, strerror(errno));
		return -1;
	} else if (S_ISDIR(s.st_mode)) {
		/* Exists and directory, OK. */
	} else {
		error("Creating '%s', '%s' exists but is not a directory",
			path, tmp);
		return -1;
	}
end:
	free(tmp);
	return 0;
}

static int ensure_directory_real(const char *path)
{
	size_t index;
	size_t last_index = 0;

	/* CWD always exists. */
	if (!*path)
		return 0;

	for (index = 0; path[index]; index++) {
		/* In middle of name? */
		if (path[index] != '/')
			continue;
		/* Root always exists. */
		if (index == 0)
			continue;
		if (check_component(path, index) < 0)
			return -1;
		last_index = index;
	}
	if (index > last_index + 1)
		if (check_component(path, index) < 0)
			return -1;
	return 0;
}

char *expand_path(const char *path)
{
	char *path2;
	path2 = xstrdup(path);

	/*
	 * Handle $XDG_CONFIG_HOME and $XDG_DATA_HOME first, as
	 * these may use $HOME.
	 */
	if (!strcmp(path2, "$XDG_CONFIG_HOME") ||
		!strncmp(path2, "$XDG_CONFIG_HOME/", 17)) {
		const char* var;

		var = getenv("XDG_CONFIG_HOME");
		if (!var || !*var)
			var = "$HOME/.config/";
		path2 = replace_variable(path2, var);
	}
	if (!strcmp(path2, "$XDG_DATA_HOME") ||
		!strncmp(path2, "$XDG_DATA_HOME/", 15)) {
		const char* var;

		var = getenv("XDG_DATA_HOME");
		if (!var || !*var)
			var = "$HOME/.local/share";
		path2 = replace_variable(path2, var);
	}
	if (!strcmp(path2, "$HOME") ||
		!strncmp(path2, "$HOME/", 6)) {
		const char* var;

		var = get_home();
		path2 = replace_variable(path2, var);
	}
	if (!strcmp(path2, "~") ||
		!strncmp(path2, "~/", 2)) {
		const char* var;

		var = get_home();
		path2 = replace_variable(path2, var);
	}
	return path2;
}

int ensure_directory(const char *path)
{
	char *path2;
	int r;

	path2 = expand_path(path);
	r = ensure_directory_real(path2);
	free(path2);
	return r;
}

int open_create_dirs(const char *path, int flags, mode_t mode)
{
	int r;
	struct stat s;
	char *path2;
	char *path3 = NULL;
	char *slash;

	path2 = expand_path(path);

	r = stat(path2, &s);
	if (r < 0 && errno != ENOENT) {
		error("Can't open/create '%s': stat failed: %s",
			path2, strerror(errno));
		return -1;
	} else if (r < 0 && (flags & O_CREAT) == 0) {
		errno = ENOENT;
		return -1;
	} else if (r >= 0 && !S_ISREG(s.st_mode)) {
		error("Can't open/create '%s': It isn't regular file", path2);
		errno = EEXIST;
		return -1;
	}

	slash = strrchr(path2, '/');
	if(slash) {
		path3 = xstrndup(path2, slash - path2);
		if (ensure_directory_real(path3) < 0)
			return -1;
	}

	r = open(path2, flags, mode);

	free(path2);
	free(path3);

	return r;
}
