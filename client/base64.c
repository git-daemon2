#include "base64.h"
#include "git-compat-util.h"

void encode_uint32(unsigned char *ptr, uint32_t value)
{
	ptr[0] = (unsigned char)(value >> 24);
	ptr[1] = (unsigned char)(value >> 16);
	ptr[2] = (unsigned char)(value >> 8);
	ptr[3] = (unsigned char)(value);
}

/* Base64 character value */
static int char_value(unsigned char ch)
{
	if (ch >= 'A' && ch <= 'Z')
		return ch - 'A';
	if (ch >= 'a' && ch <= 'z')
		return ch - 'a' + 26;
	if (ch >= '0' && ch <= '9')
		return ch - '0' + 52;
	if (ch == '+')
		return 62;
	if (ch == '/')
		return 63;
	return -1;
}

/* Decode Base64 chunk. */
unsigned char *decode_base64_chunk(const unsigned char *chunk,
	size_t chunk_len, size_t *key_len)
{
	unsigned char *ret;
	unsigned blockmod = 0;
	int buf[4];
	size_t i;

	ret = xmalloc((chunk_len + 3) / 4 * 3);
	*key_len = 0;

	for(i = 0; i < chunk_len; i++) {
		buf[blockmod] = char_value(chunk[i]);
		if (buf[blockmod] >= 0)
			blockmod++;
		else
			buf[blockmod] = 0;
		if(blockmod == 4) {
			int v = (buf[0] << 18) | (buf[1] << 12) |
				(buf[2] << 6) | buf[3];
			ret[(*key_len)++] = (unsigned char)(v >> 16);
			ret[(*key_len)++] = (unsigned char)(v >> 8);
			ret[(*key_len)++] = (unsigned char)(v);
			blockmod = 0;
			buf[0] = buf[1] = buf[2] = buf[3] = 0;
		}
	}
	if(blockmod > 0) {
		int v = (buf[0] << 18) | (buf[1] << 12) |
			(buf[2] << 6) | buf[3];
		if (blockmod > 1)
			ret[(*key_len)++] = (unsigned char)(v >> 16);
		if (blockmod > 2)
			ret[(*key_len)++] = (unsigned char)(v >> 8);
	}
	return ret;
}

/* Return address of next (nonempty) line, or NULL if none. */
const unsigned char *next_line(const unsigned char *blob,
	size_t *remaining)
{
	while (*blob != '\r' && *blob != '\n') {
		blob++;
		(*remaining)--;
		if (!*remaining)
			return NULL;
	}
	while (*blob == '\r' || *blob == '\n') {
		blob++;
		(*remaining)--;
		if (!*remaining)
			return NULL;
	}
	return blob;
}

#define STATUS_INIT 0
#define STATUS_HEADER 1
#define STATUS_CONTINUE 2
#define STATUS_EOL 3
#define STATUS_EOL_CONTINUE 4

/* Find start of base64 blob. */
const unsigned char *base64_blob_start(const unsigned char *blob,
	size_t *remaining)
{
	int status = STATUS_INIT;
	const unsigned char *line_start;
	size_t size_start;

	line_start = blob;
	size_start = *remaining;

	while(1) {
		switch(status) {
		case STATUS_INIT:
			if (!*remaining || *blob == '\r' || *blob == '\n') {
				/* Back off to start of line. */
				blob = line_start;
				*remaining = size_start;
				return blob;
			}
			if (*blob == ':')
				status = STATUS_HEADER;
			break;
		case STATUS_HEADER:
			if (!*remaining) {
				*remaining = 0;
				return NULL;
			}
			if (*blob == '\r' || *blob == '\n')
				status = STATUS_EOL;
			if (*blob == '\\')
				status = STATUS_CONTINUE;
			break;
		case STATUS_CONTINUE:
			if (!*remaining) {
				*remaining = 0;
				return NULL;
			}
			if (*blob == '\r' || *blob == '\n')
				status = STATUS_EOL_CONTINUE;
			else
				status = STATUS_HEADER;
			break;
		case STATUS_EOL:
			if (!*remaining) {
				*remaining = 0;
				return NULL;
			}
			if (*blob != '\r' && *blob != '\n') {
				/* Mark line start and back off by one. */
				line_start = blob;
				size_start = *remaining;
				blob--;
				(*remaining)++;
				status = STATUS_INIT;
			}
			break;
		case STATUS_EOL_CONTINUE:
			if (!*remaining) {
				*remaining = 0;
				return NULL;
			}
			if (*blob != '\r' && *blob != '\n') {
				/* Mark line start and back off by one. */
				line_start = blob;
				size_start = *remaining;
				blob--;
				(*remaining)++;
				status = STATUS_HEADER;
			}
			break;
		}
		blob++;
		(*remaining)--;
	}
}
