/*
 * Copyright (C) Ilari Liusvaara 2010
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */
#include "ssh.h"
#include "ssh_localkey.h"
#include "home.h"
#include "base64.h"
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <sys/un.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>
#include <gcrypt.h>
#include "git-compat-util.h"
#include <stdlib.h>

#define SSH2_AGENTC_SIGN_REQUEST 13
#define SSH_AGENT_FAILURE 5
#define SSH_AGENT_SUCCESS 6
#define SSH2_AGENT_SIGN_RESPONSE 14

#define MAX_REPLY_SIZE 1048576

/* Decode uint32. */
static unsigned long decode_uint32(const unsigned char *ptr)
{
	unsigned long v = 0;
	v |= ((unsigned long)ptr[0] << 24);
	v |= ((unsigned long)ptr[1] << 16);
	v |= ((unsigned long)ptr[2] << 8);
	v |= ((unsigned long)ptr[3]);
	return v;
}

/*
 * Extract actual signature from ssh-agent reply. The return value should be
 * freed. The reply has the component lengths present.
 */
static unsigned char *extract_actual_key(const unsigned char *keyreply,
	size_t keyreply_len, const char *expected_type, size_t *key_len)
{
	unsigned long rlen;

	if (keyreply_len < 1) {
		error("Malformed reply from SSH agent: No packet type present");
		return NULL;
	}
	if (keyreply[0] == SSH_AGENT_FAILURE) {
		error("SSH agent failed to sign the challenge");
		return NULL;
	}
	if (keyreply[0] != SSH2_AGENT_SIGN_RESPONSE) {
		error("SSH agent is confused: Returned packet type %u when type %u was expected",
			keyreply[0], SSH2_AGENT_SIGN_RESPONSE);
		return NULL;
	}
	if (keyreply_len < 5) {
		error("Malformed signature reply from SSH agent: Signature length incomplete");
		return NULL;
	}
	rlen = decode_uint32(keyreply + 1);

	if (keyreply_len < 5 + rlen) {
		error("Malformed signature reply from SSH agent: Signature incomplete");
		return NULL;
	}
	/* Strip packet header. */
	keyreply += 5;
	keyreply_len = rlen;

	if (keyreply_len < 4) {
		error("Malformed signature reply from SSH agent: Keytype header incomplete");
		return NULL;
	}
	rlen = decode_uint32(keyreply);
	if (keyreply_len < 4 + rlen) {
		error("Malformed signature reply from SSH agent: Keytype incomplete");
		return NULL;
	}
	if (rlen != strlen(expected_type) || memcmp(keyreply + 4,
		expected_type, rlen)) {
		error("SSH agent returned signature of wrong type: Expected '%s', got '%.*s'",
			expected_type, (int)rlen, keyreply + 4);
		return NULL;
	}
	/* Strip key type. */
	keyreply += 4 + rlen;
	keyreply_len -= 4 + rlen;

	if (keyreply_len < 4) {
		error("Malformed signature reply from SSH agent: Signature blob length incomplete");
		return NULL;
	}
	rlen = decode_uint32(keyreply);
	if (keyreply_len < 4 + rlen) {
		error("Malformed signature reply from SSH agent: Signature blob incomplete");
		return NULL;
	}

	/* Extract the signature blob. */
	keyreply += 4;
	keyreply_len = rlen;

	if (!strcmp(expected_type, "ssh-rsa")) {
		unsigned char *ret;

		ret = xmalloc(keyreply_len + 5);
		if (keyreply[0] > 128) {
			/* Zero-pad to avoid negative value. */
			encode_uint32(ret, keyreply_len + 1);
			ret[4] = 0;
			memcpy(ret + 5, keyreply, keyreply_len);
			*key_len = 5 + keyreply_len;
		} else {
			encode_uint32(ret, keyreply_len);
			memcpy(ret + 4, keyreply, keyreply_len);
			*key_len = 4 + keyreply_len;
		}
		return ret;
	} else if (!strcmp(expected_type, "ssh-dss")) {
		unsigned char *ret;
		unsigned offset = 0;

		ret = xmalloc(keyreply_len + 10);
		if (keyreply_len % 2) {
			error("Malformed DSA signature, length is odd.");
			free(ret);
			return NULL;
		}

		/* Encode r. */
		if (keyreply[0] > 128) {
			/* Zero-pad to avoid negative value. */
			encode_uint32(ret, keyreply_len / 2 + 1);
			ret[offset + 4] = 0;
			offset += 5;
		} else {
			encode_uint32(ret, keyreply_len / 2);
			offset += 4;
		}
		memcpy(ret + offset, keyreply, keyreply_len / 2);
		offset += keyreply_len / 2;

		/* Encode s. */
		if (keyreply[keyreply_len / 2] > 128) {
			/* Zero-pad to avoid negative value. */
			encode_uint32(ret + offset, keyreply_len / 2 + 1);
			ret[offset + 4] = 0;
			offset += 5;
		} else {
			encode_uint32(ret + offset, keyreply_len / 2);
			offset += 4;
		}
		memcpy(ret + offset, keyreply + keyreply_len / 2,
			keyreply_len / 2);
		offset += keyreply_len / 2;

		*key_len = offset;
		return ret;
	} else {
		error("Unknown signature type '%s'", expected_type);
		return NULL;
	}

	return NULL;
}


/* Extract SecSH public key. */
static unsigned char *extract_secsh_pubkey(const unsigned char *file,
	size_t file_len, size_t *key_len)
{
	const unsigned char *file2;
	size_t file_len2;

	/* Skip to begin line. */
	if (*file == '\r' || *file == '\n') {
		/* Skip the empty lines. */
		file = next_line(file, &file_len);
		if (!file) {
			error("Malformed SecSH public key (no header)");
			return NULL;
		}
	}
	file = next_line(file, &file_len);
	if (!file) {
		error("Malformed SecSH public key (no header)");
		return NULL;
	}

	file = base64_blob_start(file, &file_len);
	file2 = file;
	file_len2 = file_len;

	if (file_len2 == 29 && !memcmp(file2,
		"---- END SSH2 PUBLIC KEY ----", 29)) {
		error("Malformed SecSH public key (no encoded blob)");
		return NULL;
	}
	if (!memcmp(file2, "---- END SSH2 PUBLIC KEY ----\r", 30)) {
		error("Malformed SecSH public key (no encoded blob)");
		return NULL;
	}
	if (!memcmp(file2, "---- END SSH2 PUBLIC KEY ----\n", 30)) {
		error("Malformed SecSH public key (no encoded blob)");
		return NULL;
	}
	while(1) {
		file2 = next_line(file2, &file_len2);
		if (!file2) {
			error("Malformed SecSH public key (no trailer)");
			return NULL;
		}
		if (file_len2 == 29 && !memcmp(file2,
			"---- END SSH2 PUBLIC KEY ----", 29)) {
			file_len = file2 - file;
			break;
		}
		if (!memcmp(file2, "---- END SSH2 PUBLIC KEY ----\r", 30)) {
			file_len = file2 - file;
			break;
		}
		if (!memcmp(file, "---- END SSH2 PUBLIC KEY ----\n", 30)) {
			file_len = file2 - file;
			break;
		}
	}
	return decode_base64_chunk(file, file_len, key_len);
}

/* Is SecSH format key? */
static int is_secsh_pubkey(const unsigned char *file, size_t file_len)
{
	if (file_len < 32)
		return 0;
	if (!memcmp(file, "---- BEGIN SSH2 PUBLIC KEY ----\r", 32))
		return 1;
	if (!memcmp(file, "---- BEGIN SSH2 PUBLIC KEY ----\n", 32))
		return 1;
	file = next_line(file, &file_len);
	if (file_len < 32)
		return 0;
	if (!memcmp(file, "---- BEGIN SSH2 PUBLIC KEY ----\r", 32))
		return 1;
	if (!memcmp(file, "---- BEGIN SSH2 PUBLIC KEY ----\n", 32))
		return 1;
	return 0;
}

/* Extract OpenSSH public key. */
static unsigned char *extract_openssh_pubkey(const unsigned char *file,
	size_t file_len, size_t *key_len)
{
	const unsigned char *base;
	const unsigned char *sep2;
	size_t blob_size;
	base = (const unsigned char*)strchr((const char*)file, ' ');
	if (!base) {
		error("Malformed OpenSSH pubkey file");
		return NULL;
	}
	base++;

	sep2 = (const unsigned char*)strchr((const char*)base, ' ');
	if (!sep2)
		blob_size = strlen((const char*)base);
	else
		blob_size = sep2 - base;

	return decode_base64_chunk(base, blob_size, key_len);
}

/* Is OpenSSH format key? */
static int is_openssh_pubkey(const unsigned char *file, size_t file_len)
{
	if (file_len < 8)
		return 0;
	if (!memcmp(file, "ssh-rsa ", 8))
		return 1;
	if (!memcmp(file, "ssh-dss ", 8))
		return 1;
	return 0;
}

/*
 * Extract actual key blob from encoded key. The return value should be
 * freed. The reply has the component lengths present.
 */
unsigned char *extract_key_from_file(const unsigned char *file,
	size_t file_len, size_t *key_len)
{
	if (is_openssh_pubkey(file, file_len))
		return extract_openssh_pubkey(file, file_len, key_len);
	if (is_secsh_pubkey(file, file_len))
		return extract_secsh_pubkey(file, file_len, key_len);
	error("Unknown public key file format");
	return NULL;
}

void dump_blob(const char *name, const unsigned char *blob, size_t bloblen)
{

	size_t i;
	fprintf(stderr, "------- START %s (%u bytes) ------\n", name, (unsigned)bloblen);
	for(i = 0; i < bloblen; i++) {
		fprintf(stderr, "%02X ", blob[i]);
		if (i % 16 == 15)
			fprintf(stderr, "\n");
	}
	if (i % 16)
		fprintf(stderr, "\n");
	fprintf(stderr, "-------- END %s -------\n", name);

}

int write_to_agent(int fd, const unsigned char *ptr, size_t size)
{
	while (size > 0) {
		ssize_t r;
		r = write(fd, ptr, size);
		if (r > 0) {
			ptr += r;
			size -= r;
		} else if (r == 0) {
			error("Connection to ssh-agent unexpectedly lost");
			return -1;
		}
		else if (errno == EINTR || errno == EAGAIN)
			continue;
		else {
			error("Error writing to ssh-agent: %s",
				strerror(errno));
			return -1;
		}
	}
	return 0;
}

int read_in_agent(int fd, unsigned char *ptr, size_t size)
{
	while (size > 0) {
		ssize_t r;
		r = read(fd, ptr, size);
		if (r > 0) {
			ptr += r;
			size -= r;
		} else if (r == 0) {
			error("Connection to ssh-agent unexpectedly lost");
			return -1;
		}
		else if (errno == EINTR || errno == EAGAIN)
			continue;
		else {
			error("Error reading from ssh-agent: %s",
				strerror(errno));
			return -1;
		}
	}
	return 0;
}

int write_packet_to_agent(int fd, const unsigned char *payload, size_t len)
{
	unsigned char tmp[4];
	encode_uint32(tmp, len);
	if (write_to_agent(fd, tmp, 4) < 0)
		return -1;
	if (write_to_agent(fd, payload, len) < 0)
		return -1;
	return 0;
}

unsigned char *read_packet_from_agent(int fd, size_t *len)
{
	unsigned char *ret;
	unsigned char tmp[4];
	if (read_in_agent(fd, tmp, 4) < 0)
		return NULL;
	*len = decode_uint32(tmp);
	if (*len > MAX_REPLY_SIZE) {
		error("Reply from SSH agent too large (size %zu, maximum allowed %zu)",
			*len, (size_t)MAX_REPLY_SIZE);
		return NULL;
	}
	ret = xmalloc(*len);
	if (read_in_agent(fd, ret, *len) < 0) {
		free(ret);
		return NULL;
	}
	return ret;
}

static unsigned char *agent_do_io_cycle(int fd, const unsigned char *payload,
	size_t payload_len, size_t *reply_len)
{
	if (write_packet_to_agent(fd, payload, payload_len) < 0)
		return NULL;
	return read_packet_from_agent(fd, reply_len);
}

int ssh_agent_sock = -1;

int init_ssh_agent()
{
	const char *agentpath;
	struct sockaddr_un addr;

	if (ssh_agent_sock >= 0)
		return 0;
	if (ssh_agent_sock < -1)
		return -1;

	agentpath = getenv("SSH_AUTH_SOCK");
	if (!agentpath) {
		ssh_agent_sock = -2;
		return -1;
	}

	addr.sun_family = AF_UNIX;
	strcpy(addr.sun_path, agentpath);

	ssh_agent_sock = socket(AF_UNIX, SOCK_STREAM, 0);
	if (ssh_agent_sock < 0) {
		error("Can't create socket: %s",
			strerror(errno));
		ssh_agent_sock = -2;
		return -1;
	}

	if (connect(ssh_agent_sock, (struct sockaddr*)&addr, sizeof(addr)) < 0) {
		error("Can't connect to ssh-agent(%s): %s", agentpath,
		strerror(errno));
		close(ssh_agent_sock);
		ssh_agent_sock = -2;
		return -1;
	}
	return 0;
}

unsigned char *sign_using_ssh_agent(const char *keyname,
	const unsigned char *kblob, size_t kblob_size,
	const unsigned char *challenge, size_t challenge_size,
	size_t *signature_length, const char *type)
{
	unsigned char *request;
	unsigned char *reply;
	size_t reply_length;
	size_t request_length;
	unsigned char *signblocks = NULL;

	/* 43 = 1 (type) + 8 (2 lengths) + 4 (flags) */
	request = xmalloc(13 + kblob_size + challenge_size);
	request_length = 13 + kblob_size + challenge_size;

	request[0] = SSH2_AGENTC_SIGN_REQUEST;
	encode_uint32(request + 1, kblob_size);
	memcpy(request + 5, kblob, kblob_size);
	encode_uint32(request + 5 + kblob_size, challenge_size);
	memcpy(request + 9 + kblob_size, challenge, challenge_size);
	encode_uint32(request + 9 + kblob_size + challenge_size, 0);

	if (init_ssh_agent() < 0)
		goto out_request;

	reply = agent_do_io_cycle(ssh_agent_sock, request, request_length,
		&reply_length);
	if (!reply)
		goto out_request;

	signblocks = extract_actual_key(reply, reply_length, type,
		signature_length);
	/* extract_actual_key can fail, but we don't print any error for that. */

	free(reply);
out_request:
	free(request);
	return signblocks;
}

static const char *check_blob(const unsigned char* blob, size_t blob_size)
{
	size_t offset = 0;
	unsigned round = 0;
	unsigned rounds = 1;
	const char *type = NULL;

	for (round = 0; round < rounds; round++) {
		unsigned long len;
		if (offset + 4 < offset || blob_size < offset + 4) {
			error("Component length header incomplete (component=%u/%u, blob_size=%zu, offset=%zu",
				round + 1, rounds, blob_size, offset);
			return NULL;
		}
		len = decode_uint32(blob + offset);
		if (offset + len + 4 < offset + 4 || blob_size < offset + len + 4) {
			error("Component incomplete (component=%u/%u, blob_size=%zu, offset=%zu, len=%lu)",
				round + 1, rounds, blob_size, offset, len);
			return NULL;
		}
		if (round == 0 && len == 7 && !memcmp(blob + offset + 4,
			"ssh-rsa", 7)) {
			rounds = 3;
			type = "ssh-rsa";
		} else if (round == 0 && len == 7 && !memcmp(blob + offset + 4,
			"ssh-dss", 7)) {
			rounds = 5;
			type = "ssh-dss";
		} else if (round == 0) {
			error("Read key has unknown key type (neither ssh-rsa nor ssh-dss)");
			return NULL;
		}
		offset += len + 4;
	}
	if (offset != blob_size) {
		error("Garbage after end of key");
		return NULL;
	}
	return type;
}

#define MAXPATH 4096
#define MAXKEY 65536
#define DATABLOCK 32
#define HASHALGO GCRY_MD_SHA256
#define HASHLEN 32

void do_ssh_preauth(const char *ssh_user)
{
	send_ssh_authentication(NULL, ssh_user);
}

void send_ssh_authentication(struct user *dispatcher, const char *ssh_user)
{
	unsigned char *x = NULL;
	size_t i;
	FILE* filp;
	char *path;
	char pathtemplate[MAXPATH];
	unsigned char key[MAXKEY];
	size_t keysize;
	unsigned char block[DATABLOCK] = {0};

	unsigned char *signature = NULL;
	size_t signature_length;
	const char *type;

	if (strlen(ssh_user) > MAXPATH - 32)
		die("Key name too long");
	sprintf(pathtemplate, "~/.ssh/%s.pub", ssh_user);
	path = expand_path(pathtemplate);

	if (!x) {
		/* Read the pubkey file. */
		filp = fopen(path, "r");
		if (!filp) {
			error("Can't open '%s'", path);
			goto out_readfile;
		}
		keysize = fread(key, 1, MAXKEY, filp);
		if (ferror(filp)) {
			error("Can't read '%s'", path);
			goto out_readfile;
		}
		if (!feof(filp)) {
			error("Keyfile '%s' too large", path);
			goto out_readfile;
		}
		fclose(filp);

		x = extract_key_from_file(key, keysize, &i);
	}
out_readfile:
	if (!x)
		die("Can't read any public key named '%s'",
			ssh_user);

	type = check_blob(x, i);
	if (!type)
		die("Malformed public key for '%s'", ssh_user);

	if (dispatcher) {
		int s;
		gnutls_session_t session;
		unsigned char hash[HASHLEN];
		const gnutls_datum_t *cert;
		unsigned int lsize;
		session = user_get_tls(dispatcher);
		if (!session)
			die("SSH auth requires TLS");
		cert = gnutls_certificate_get_peers(session, &lsize);
		if (!cert)
			die("Unable to get server certificate (server didn't send one?)");

		gcry_md_hash_buffer(HASHALGO, hash, cert->data, cert->size);
		s = gnutls_prf(session, 14, "ssh-key-verify", 0, HASHLEN,
			(const char*)hash, DATABLOCK, (char*)block);
		if (s < 0)
			die("Can't compute challenge: %s",
				gnutls_strerror(s));
	}
	if (!signature)
		signature = sign_using_ssh_agent(ssh_user,
			x, i, block, DATABLOCK, &signature_length,
			type);
	if (!signature)
		signature = sign_using_local_key(ssh_user,
			x, i, block, DATABLOCK, &signature_length,
			type);
	if (!signature)
		die("Can't sign using key '%s'", ssh_user);

	if (dispatcher) {
		struct cbuffer* in;
		char tmp[5];
		in = user_get_red_in(dispatcher);
		sprintf(tmp, "%04x", (unsigned)(8 + i + signature_length));
		cbuffer_write(in, (const unsigned char*)tmp, 4);
		cbuffer_write(in, (const unsigned char*)"ssh ", 4);
		cbuffer_write(in, x, i);
		cbuffer_write(in, signature, signature_length);
	}

	free(signature);
	free(x);
}
