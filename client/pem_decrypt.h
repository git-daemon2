#ifndef _pem_decrypt__h__included__
#define _pem_decrypt__h__included__

#include <stdlib.h>

unsigned char *decrypt_key(const unsigned char *cipher, size_t cipher_size,
	const char *dek_info, const char *keyname, size_t *decrypted_len);

#endif
