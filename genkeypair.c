/*
 * Copyright (C) Ilari Liusvaara 2009-2010
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void write_cert(int server_mode);

static void do_help()
{
	printf("gits-generate-keypair: User keypair generator.\n");
	printf("Command line options:\n");
	printf("--help\n");
	printf("\tThis help\n");
	printf("\n");
	printf("Note: Keep generated keypair files private!\n");
	printf("Use gits-get-key-name to get public short\n");
	printf("representation of keypair for authorization.\n");
	printf("\n");
	printf("WARNING: Don't let keypairs to be tampered with!\n");
	printf("WARNING: Tampered keypairs may do very nasty things\n");
	printf("WARNING: if used.\n");
	exit(0);
}


int main(int argc, char **argv)
{
	if (argc > 1 && !strcmp(argv[1], "--help"))
		do_help();
	write_cert(0);
	return 0;
}
