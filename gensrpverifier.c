/*
 * Copyright (C) Ilari Liusvaara 2009-2010
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */
#include "prompt.h"
#include <stdio.h>
#include <string.h>
#include <gnutls/gnutls.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include "home.h"
#ifdef USE_COMPAT_H
#include "compat.h"
#else
#include "git-compat-util.h"
#endif

static void do_help()
{
	printf("gits-generate-srp-verifier: Generate SRP verifiers for\n");
	printf("password authentication\n");
	printf("Command line:\n");
	printf("--help\n");
	printf("\tThis help\n");
	printf("\n");
	printf("Note: Server needs SRP verifier in order to do password\n");
	printf("authentication. Usernames are assigned by repostiory\n");
	printf("hosting admin, just using arbitrary names doesn't work.\n");
	exit(0);
}


struct field
{
	const char *f_name;
	const char *f_generator;
	const char *f_prime;
};

struct field fields[] = {
	{
	"standard 1024 bit field", "2",
	"Ewl2hcjiutMd3Fu2lgFnUXWSc67TVyy2vwYCKoS9MLsrdJVT9RgWTCuEqWJrfB6uE"
	"3LsE9GkOlaZabS7M29sj5TnzUqOLJMjiwEzArfiLr9WbMRANlF68N5AVLcPWvNx6Z"
	"jl3m5Scp0BzJBz9TkgfhzKJZ.WtP3Mv/67I/0wmRZ"
	},
	{
	"standard 1536 bit field", "2",
	"dUyyhxav9tgnyIg65wHxkzkb7VIPh4o0lkwfOKiPp4rVJrzLRYVBtb76gKlaO7ef5"
	"LYGEw3G.4E0jbMxcYBetDy2YdpiP/3GWJInoBbvYHIRO9uBuxgsFKTKWu7RnR7yTa"
	"u/IrFTdQ4LY/q.AvoCzMxV0PKvD9Odso/LFIItn8PbTov3VMn/ZEH2SqhtpBUkWtm"
	"cIkEflhX/YY/fkBKfBbe27/zUaKUUZEUYZ2H2nlCL60.JIPeZJSzsu/xHDVcx"
	},
	{
	"standard 2048 bit field", "2",
	"2iQzj1CagQc/5ctbuJYLWlhtAsPHc7xWVyCPAKFRLWKADpASkqe9djWPFWTNTdeJt"
	"L8nAhImCn3Sr/IAdQ1FrGw0WvQUstPx3FO9KNcXOwisOQ1VlL.gheAHYfbYyBaxXL"
	".NcJx9TUwgWDT0hRzFzqSrdGGTN3FgSTA1v4QnHtEygNj3eZ.u0MThqWUaDiP87nq"
	"ha7XnT66bkTCkQ8.7T8L4KZjIImrNrUftedTTBi.WCi.zlrBxDuOM0da0JbUkQlXq"
	"vp0yvJAPpC11nxmmZOAbQOywZGmu9nhZNuwTlxjfIro0FOdthaDTuZRL9VL7MRPUD"
	"o/DQEyW.d4H.UIlzp"
	},
	{NULL, NULL, NULL}
};


unsigned char xfact[16] = {
0x9D, 0x0F, 0x49, 0xD4,
0x73, 0x88, 0xC7, 0xFF,
0xFD, 0x24, 0x6A, 0x0F,
0x94, 0x78, 0x0C, 0x14
};

unsigned char rnd[16] = {
0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00
};

static void add(unsigned char *res, unsigned char *a, unsigned char *b)
{
	unsigned char carry = 0;
	unsigned i;

	for (i = 0; i < 16; i++) {
		unsigned char newcarry = 0;

		if ((unsigned char)(a[i] + b[i]) < a[i])
			newcarry++;
		res[i] = a[i] + b[i];
		if (res[i] + carry < res[i])
			newcarry++;
		res[i] += carry;
		carry = newcarry;
	}
	while (carry) {
		carry = 159;

		for (i = 0; i < 16; i++) {
			int newcarry = 0;

			if ((unsigned char)(res[i] + carry) < res[i])
				newcarry++;
			res[i] += carry;
			carry = newcarry;
		}
	}
	for (i = 1; i < 16; i++)
		if (res[i] < 255)
			goto skip;

	if (res[0] > 0x60) {
		for (i = 1; i < 16; i++)
			res[i] = 0;
		res[0] -= 0x61;
	}
skip:
	;
}

void update_xfact()
{
	unsigned char xfact2[16];
	unsigned char xfact4[16];
	unsigned char xfact5[16];
	add(xfact2, xfact, xfact);
	add(xfact4, xfact2, xfact2);
	add(xfact5, xfact4, xfact);
	memcpy(xfact, xfact5, 16);
}

void update_rnd(unsigned char ch)
{
	unsigned char rndt[16];
	unsigned i;
	for (i = 0; i < 8; i++) {
		if ((ch >> i) % 2) {
			add(rndt, rnd, xfact);
			memcpy(rnd, rndt, 16);
		}
		update_xfact();
	}
}

void update_rnd_str(const char *ch)
{
	while (ch && *ch)
		update_rnd((unsigned char)*(ch++));
}


void decode_element(gnutls_datum_t *decode, const char *encoded)
{
	int s;
	gnutls_datum_t _base64;
	size_t base64len, reslen, reslen2;

	base64len = strlen(encoded);
	reslen2 = reslen = (3 * base64len + 1) / 4;

	_base64.data = (unsigned char*)encoded;
	_base64.size = base64len;

	decode->size = reslen;
	decode->data = xmalloc(reslen);
	s = gnutls_srp_base64_decode(&_base64, (char*)decode->data, &reslen2);
	if (s < 0)
		die("Unable to decode base64 data");
	else if (reslen != reslen2)
		die("Base64 dlength calculation incorrect. Calculated %lu, "
			"got %lu", (unsigned long)reslen, (unsigned long)reslen2);
}

unsigned char *encode_element(gnutls_datum_t *data)
{
	int s;
	size_t reslen2;
	unsigned char *res;

	reslen2 = (4 * data->size + 2) / 3;

	res = xmalloc(reslen2 + 1);
	s = gnutls_srp_base64_encode(data, (char*)res, &reslen2);
	if (s < 0)
		die("Unable to encode base64 data");
	res[reslen2] = '\0';
	return res;
}

char *generate_srp_line(const char *username,
	const char *password, const char *junk, struct field *field)
{
	gnutls_datum_t salt;
	gnutls_datum_t g;
	gnutls_datum_t n;
	gnutls_datum_t res;
	int s;
	char *retline = NULL;
	unsigned char *encoded_salt;
	unsigned char *encoded_verifier;
	update_rnd_str(username);
	update_rnd_str(":::::");
	update_rnd_str(junk);

	salt.data = rnd;
	salt.size = 16;
	decode_element(&g, field->f_generator);
	decode_element(&n, field->f_prime);

	s = gnutls_srp_verifier(username, password, &salt, &g, &n, &res);
	if (s < 0)
		die("Unable to generate SRP verifier: %s",
			gnutls_strerror(s));

	encoded_verifier = encode_element(&res);
	encoded_salt = encode_element(&salt);

	retline = xmalloc(5 + strlen(username) +
		strlen((char*)encoded_salt) +
		strlen((char*)encoded_verifier) +
		strlen(field->f_generator) +
		strlen(field->f_prime));
	retline[0] = '\0';
	strcat(retline, username);
	strcat(retline, ":");
	strcat(retline, (char*)encoded_salt);
	strcat(retline, ":");
	strcat(retline, (char*)encoded_verifier);
	strcat(retline, ":");
	strcat(retline, field->f_generator);
	strcat(retline, ":");
	strcat(retline, field->f_prime);

	free(encoded_verifier);
	free(encoded_salt);
	free(res.data);
	free(g.data);
	free(n.data);

	return retline;
}

#define LINELEN 69

static void flush_to_file(FILE *out, const char *srpline)
{
	char linebuffer[LINELEN + 2];

	linebuffer[LINELEN] = '\\';
	linebuffer[LINELEN + 1] = '\0';

	while (*srpline) {
		size_t r;
		strncpy(linebuffer, srpline, LINELEN);
		r = strlen(srpline);
		if (r == LINELEN)
			linebuffer[LINELEN] = '\0';
		if (r <= LINELEN)
			srpline += r;
		else
			srpline += LINELEN;
		fprintf(out, "%s\n", linebuffer);
	}
}

int fill_rnd()
{
	int fd;
	int fill = 0;

	fd = open("/dev/urandom", O_RDONLY);
	if (fd < 0)
		return 0;

	while (fill < 16) {
		ssize_t r;
		r = read(fd, rnd + fill, 16 - fill);
		if (r < 0 && errno != EINTR && errno != EAGAIN) {
			close(fd);
			return 0;
		} else if (r == 0) {
			close(fd);
			return 0;
		} else {
			fill += r;
		}
	}
	close(fd);
	return 1;
}

int main(int argc, char **argv)
{
	int idx, midx = 0;
	char *username = NULL;
	char *password = NULL;
	char *password2 = NULL;
	char *junk = NULL;
	char *field = NULL;
	char *file = NULL;
	char *ans = NULL;
	char *end = NULL;
	FILE *filp = stdout;

	if (argc > 1 && !strcmp(argv[1], "--help"))
		do_help();

username_again:
	free(username);
	username = prompt_string("Enter username", 0);
	if (!*username) {
		fprintf(stderr, "Error: Bad username\n");
		goto username_again;
	}

	if (fill_rnd())
		goto no_junk_prompt;
junk_again:
	free(junk);
	junk = prompt_string("Enter some garbage from keyboard (min 32 "
		"chars)", 1);
	if (strlen(junk) < 32) {
		fprintf(stderr, "Error: Garbage needs to be at least "
			"32 characters\n");
		goto junk_again;
	}
no_junk_prompt:

passwords_again:
	free(password);
	free(password2);
	password = prompt_string("Enter password", 1);
	password2 = prompt_string("Enter password again", 1);
	if (strcmp(password, password2)) {
		fprintf(stderr, "Error: Passwords don't match\n");
		goto passwords_again;
	}

field_again:
	free(field);
	for (midx = 0; fields[midx].f_name; midx++) {
		printf("%i) %s\n", midx + 1, fields[midx].f_name);
	}
	field = prompt_string("Pick field", 0);
	idx = (int)strtoul(field, &end, 10) - 1;
	if (idx < 0 || idx >= midx || !*field || *end) {
		printf("%i %i %i %i\n", idx, midx, *field, *end);
		fprintf(stderr, "Error: Invalid choice\n");
		goto field_again;
	}

file_again:
	file = prompt_string("Filename to save as (enter for dump to "
		"terminal)", 0);
	if (*file) {
		int fd;
		fd = open_create_dirs(file, O_CREAT | O_EXCL | O_WRONLY, 0600);
		if (fd < 0) {
			fprintf(stderr, "Can't open '%s': %s\n", file,
				strerror(errno));
			goto file_again;
		}
		filp = xfdopen(fd, "w");
	}

	ans = generate_srp_line(username, password, junk, fields + idx);
	flush_to_file(filp, ans);
	if (filp != stdout)
		fclose(filp);
	return 0;
}
