#
# Define NO_SRP if you don't want SRP support.
#
# Define NO_IPV6 if you don't want IPv6 support.
#
# Define NO_SO_PEERCRED if your OS does not support SO_PEERCRED
#
# Define SERVER_ONLY to only build the server.
#

ifdef SERVER_ONLY
all: git-daemon2 gits-get-key-id gits-generate-hostkey
else
all: git-daemon2 git-remote-gits gits-get-key-id gits-generate-srp-verifier gits-generate-hostkey gits-generate-keypair
endif

CFLAGS=-Wall -Werror -g
LDFLAGS=-rdynamic

SERVERFILES = server/main.o server/os_exception.o server/user.o server/cbuffer.o server/mainloop.o common/user.o common/cbuffer.o server/listensock.o server/srp-udb.o server/log.o server/certificate.o server/signals.o server/pkt-decoder.o server/userchange.o common/misc.o server/unix.o server/ssh-keypair.o

ifndef NO_SRP
SERVERFILES += server/srp-udb.o
else
CFLAGS += -DDISABLE_SRP
endif

ifndef NO_SO_PEERCRED
CFLAGS += -DUSE_SO_PEERCRED=1
endif

ifdef NO_IPV6
CFLAGS += -DNO_IPV6=1
SERVERFILES += server/inet-basic.o
else
SERVERFILES += server/inet-advanced.o
endif

CLIENT_SSH_COMPLEX = client/ssh.o client/ssh_localkey.o client/pem.o client/base64.o client/home.o client/pem_decrypt.o client/srp_askpass.o client/prompt.o common/user.o common/cbuffer.o common/misc.o

git-daemon2: $(SERVERFILES)
	g++ $(LDFLAGS) -o $@ $^ -lgnutls

git-remote-gits: client/main.o client/compat.o client/keypairs.o client/hostkey.o client/certificate.o client/connect.o $(CLIENT_SSH_COMPLEX)
	gcc $(LDFLAGS) -o $@ $^ -lgnutls

gits-get-key-id: getkeyid.o client/certificate.o client/compat.o $(CLIENT_SSH_COMPLEX)
	gcc $(LDFLAGS) -o $@ $^ -lgnutls

gits-generate-srp-verifier: gensrpverifier.o client/prompt.o client/compat.o client/home.o
	gcc $(LDFLAGS) -o $@ $^ -lgnutls

gits-generate-hostkey: genhostkey.o client/home.o common/mkcert.o common/cbuffer.o client/compat.o client/prompt.o
	gcc $(LDFLAGS) -o $@ $^ -lgnutls

gits-generate-keypair: genkeypair.o client/home.o common/mkcert.o common/cbuffer.o client/compat.o client/prompt.o
	gcc $(LDFLAGS) -o $@ $^ -lgnutls

%.o: %.cpp
	g++ $(CFLAGS) -c -o $@ $< -Icommon -Iserver

%.o: %.c
	gcc $(CFLAGS) -c -o $@ $< -Iclient -Icommon -DUSE_UNIX_SCATTER_GATHER_IO -DUSE_TRAP_PAGING -DBUILD_SELFSTANDING

clean:
	rm -f client/*.o common/*.o server/*.o *.o git-daemon2 git-remote-gits gits-generate-hostkey gits-generate-keypair gits-generate-srp-verifier gits-get-key-id gits-hostkey
